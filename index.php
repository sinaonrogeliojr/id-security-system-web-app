<?php
 session_start();
 if (isset($_SESSION['id'])) {
 	@header('location:client/');
 }
 else if (isset($_SESSION['pr_id'])) {
 	@header('location:pr_pane/');
 }
  else if (isset($_SESSION['teacher_id'])) {
 	@header('location:teacher/');
 }
 else if (isset($_SESSION['id_sec'])) {
 	@header('location:guard/');
 }
 include_once('config.php');
 ?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title>Ssja</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="icon/png" href="img/SSJA LOGO copy1.jpg">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<link rel="stylesheet" type="text/css" href="font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
</head>
<body>
<?php 
include_once("nav.php");
 ?>
 <script type="text/javascript">
 	function onclick_img(){
 		$("#img_logo").addClass('zoomOut');
 		setTimeout(function(){$("#img_logo").animate({'width':'160'}); $("#img_logo").removeClass('zoomOut');},500);
 	}
 </script>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<div class="text-center">
				<img src="img/isu.png" id="img_logo" style="position: sticky; top:0; position: -webkit-sticky;" onclick="onclick_img();" class="animated jackInTheBox" width="160">
			</div>
			<hr>
			<div class="form-group">
				<label>User Type</label>
				<select class="form-control" id="usertype" oninput="user_type();" onchange="user_type();">
					<option>Student</option>
					<option>System Provider</option>
					<option>Teacher</option>
					<option>Security</option>
					<option>Staff</option>
				</select>
			</div>

			<div id="load"></div>			
			<div id="forms">
				<div class=" form-group">
				<label id="uname_l">Identification Number</label>
				<input type="text" name="uname" id="uname" placeholder="Enter student id..." class="form-control ">
				</div>

				<div class=" form-group">
					<label id="pwd_l">Lastname</label>
					<input type="password" name="pwd" id="pwd" placeholder="Enter lastname..." class="form-control">
				</div>

				<div class="text-right">
					<button id="btn_log" class="btn btn-primary btn-block " onclick="login();">Login</button>
					
				</div>
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>

</body>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/sweetalert.min.js"></script>
<script src="js/login.js"></script>

<script>
  var input_uname = document.getElementById("uname");
  input_uname.addEventListener("keyup", function(event) {
      event.preventDefault();
      if (event.keyCode === 13) {
          document.getElementById("btn_log").click();
      }
  });

  var input = document.getElementById("pwd");
  input.addEventListener("keyup", function(event) {
      event.preventDefault();
      if (event.keyCode === 13) {
          document.getElementById("btn_log").click();
      }
  });
</script>

</html>