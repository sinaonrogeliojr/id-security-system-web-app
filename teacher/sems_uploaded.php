<?php
session_start();
if (!isset($_SESSION['teacher_id'])) {
	@header('location:../');	
}
 include_once('../config.php')
 ;?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['teacher_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="../css/showToast.css">
</head>
<script type="text/javascript">

	function load_grades(){
		
		$.ajax({
			type:"POST",
			url:"tbl_sems_uploaded.php",
			cache:false,
			beforeSend:function(){
			$("#tbl_grades").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#tbl_grades").html(data);
			}
		});
	}
	
</script>

<body onload="load_grades();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">

	<div class="row">

		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			
			<a href="index.php" class="btn btn-primary btn-block">Back</a>

			<!-- <h6 class=" w3-padding-top"><b><?php echo $_SESSION['teacher_fullname'] ?></b></h6>
			<h6 class=" w3-padding-top"><b><?php echo $_SESSION['teacher_grade'].' - '.$_SESSION['teacher_advisory'] ?></b></h6> -->
			<h5 class=""><i class="fa fa-list-alt w3-small"></i> SEMS UPLOADED</h5>
			<hr>

			<div id="tbl_grades"></div>

		</div>
		</div>

		<div class="col-sm-3"></div>

	</div>
</div>
</body>

<script src="../js/jquery.mins.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>


</html>