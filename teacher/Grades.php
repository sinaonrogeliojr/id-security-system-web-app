<?php
session_start();
if (!isset($_SESSION['teacher_id'])) {
	@header('location:../');	
}
include_once('../config.php');
$subject_code = $_GET['subject_code'];
$sql = mysqli_query($con, "SELECT * FROM tbl_subjects_uploaded WHERE subject_code <> '' and subject_code = '$subject_code'");
$row = mysqli_fetch_assoc($sql);
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['teacher_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<script type="text/javascript">

	function load_grades(){
		
		var sem_id = document.getElementById('sem_id');
		var subject_code = document.getElementById('subject_code');
		var txt_search = $('#txt_search').val();
		var mydata = 'sem_id=' + sem_id.value + '&subject_code=' + subject_code.value + '&txt_search=' + txt_search;
		$.ajax({
			type:"POST",
			url:"tbl_grades.php",
			data:mydata,
			cache:false,
			beforeSend:function(){
			$("#tbl_absent").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#tbl_absent").html(data);
			}
		});
	}
	
</script>

<body onload="load_grades();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">

	<div class="row">

		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>

			<form method="get" action="subjects_uploaded.php">
				<input href="subjects_uploaded.php" type="submit" class="btn btn-primary btn-block" value="Back">
				<input type="hidden" name="sem_id" id="sem_id" value="<?php echo $_GET['sem_id']; ?>">
			</form>
			
			<h5 class=" w3-padding-top"><b><?php echo $_SESSION['teacher_grade'].' - '.$_SESSION['teacher_advisory'] ?></b></h5>
			<input type="hidden" name="subject_code" id="subject_code" value="<?php echo $_GET['subject_code']; ?>">	

			<div class="w3-panel w3-blue"><b> List of Grades for <em><?php echo strtoupper($row['subject']) .' - '. strtoupper($row['description']);?></em></b>
				</div> 
			<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-search"></i></span>
						<input  type="text" name="txt_search" id="txt_search" oninput="load_grades()" class="form-control" placeholder="Search...">
					</div>
				</div> 

			<div id="tbl_absent"></div>

		</div>
		</div>

		<div class="col-sm-3"></div>

	</div>
</div>
</body>

<script src="../js/jquery.mins.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>

</html>