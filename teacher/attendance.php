<?php
session_start();
if (!isset($_SESSION['teacher_id'])) {
	@header('location:../');	
}
 include_once('../config.php')
 ;?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['teacher_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<script type="text/javascript">
	function src_load_now(){
	
		var src_attend = document.getElementById('src_attend');
		var data = 'mysrc=' + src_attend.value;
		if (src_attend.value == "")
		{	
			$('#loader').show('fast');
			//$("#load_data").show('fast');
			$('#page_load').show('fast');
			$("#load_data_search").html('');	
			$("#btn_remove").removeClass('w3-red animated slideInLeft');
		}
		else
		{
			$("#btn_remove").addClass('w3-red animated slideInLeft ');
			$.ajax({
				type:"POST",
				url:"fetch_attendance2.php",
				data:data,
				cache:false,
				success:function(data){
				//alert(data);
				if (data == 1 || data == 2) 
				{
					
					//load_now();
					$("#load_data_search").html('');	
					$("#page_load").html('<button class="btn btn-default btn btn-block">No records found...</button>');			
				}
				else
				{
					$('#loader').hide('fast');
					$('#page_load').hide('fast');
					$("#load_data_search").html(data);	
						
				}
					
					
				}
			});
		}
	}



	function load_now(){
		$(document).ready(function(){
		var limit = 4; 
		var start = 0;
		var action = 'inactive';
		function page_load(limit,start){
			$.ajax({
				type:"POST",
				url:"fetch_attendance.php",
				data:{limit:limit,start:start},
				cache:false,
				success:function(data){
					//alert(data);
					if (data == 1) 
					{
						$("#loader").html('<button class="btn btn-default btn btn-block">End of Records...</button>');
						action='active';
					}
					else
					{
						$("#load_data").append(data);
						$("#page_load").html('<button class="btn  btn-block btn-lg w3-small"><img style="margin-top:-9px;" src="../img/flat.gif" width="20"> Loading more records...</button>');
						action='inactive';
					}
				}
			});
		}

		if (action == 'inactive') 
		{
			action = 'active';
			page_load(limit,start);
		}
		$(window).scroll(function(){

			if ($(window).scrollTop() + $(window).height() > $("#load_data").height() && action == 'inactive') 
			{

				action = 'active';
				start = start + limit;
				setTimeout(function(){
				page_load(limit,start);
					//	alert(data);
				}, 1000);

					
			setTimeout(function(){
			
			},2000);
				
			}
			  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
			      $("#b-up").show('fast');
			    } else {
			       $("#b-up").fadeOut('fast');
			    }

		});

	});
	}
	function backtop(){
	document.documentElement.scrollTop = 0;
	}
</script>

<body onload="load_now();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">

		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<a href="index.php" class="btn btn-primary btn-block">Back</a>
			<h6 class=" w3-padding-top"><b><?php echo $_SESSION['teacher_fullname'] ?></b></h6>
			<h6 class=" w3-padding-top"><b><?php echo $_SESSION['teacher_grade'].' - '.$_SESSION['teacher_advisory'] ?></b></h6>	
			<br>
				<h5 class=""><i class="fa fa-list-alt"></i> Attendance</h5>

				<div class="input-group">
				    <span class="input-group-addon"><i class="fa fa-search"></i></span>
				   <input type="date" name="src_attend" onfocus="src_load_now();" oninput="src_load_now();" onchange="src_load_now();" oninput="src_load_now();" id="src_attend" placeholder="Search Attendance..." class="form-control">
				   <span class="input-group-addon btn w3-ripple" id="btn_remove" onclick="	$(this).removeClass('w3-red animated slideInLeft '); $('#src_attend').val(''); $('#src_attend').focus(); $('#load_data_src').html('');"><i class="fa fa-remove"></i></span>
				 </div>
			<hr>
				<!--<table class="table table-hover table-striped table-bordered table-condense w3-small" style="margin-bottom:2px;">
					<tr>
						<th>Day</th>
						<th>Time</th>
						<th>Date</th>
						<th class="text-right">AM/PM</th>
					</tr>
				</table>-->


					<div id="b-up" onclick="backtop(); load_now();" style="display: none; position:fixed; z-index: 10; right:15px; bottom:15px;"> <button class="btn w3-black w3-opacity btn-lg animated rubberBand"><i class="fa fa-arrow-up"></i></button></div>	
					<div id="load_data_src"></div>
					
					<div id="load_data_search"></div>
						<div id="loader">
							<div id="load_data"></div>
						</div>
					<br>
					<div id="page_load"></div>
					<br><br>


		</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>

</body>





<script src="../js/jquery.mins.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>

</html>