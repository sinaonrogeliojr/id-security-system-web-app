	<?php
session_start();
include_once("../config.php");
$grade = $_SESSION['teacher_grade'];
$section=$_SESSION['teacher_advisory'];

$sem_id = mysqli_real_escape_string($con, $_POST['sem_id']);
$subject_code = mysqli_real_escape_string($con, $_POST['subject_code']);
$teacher_id = $_SESSION['teacher_id'];
$txt_search = mysqli_real_escape_string($con, $_POST['txt_search']);
// $stmt = mysqli_query($con,"SELECT t1.*,CONCAT(t2.`lastname`, " ", t2.`firstname`, " ", t2.`mi`) AS student_name,t3.`image` FROM tbl_student_grades t1
// LEFT JOIN tbl_students t2 ON t1.`student_id` = t2.`student_id` 
// LEFT JOIN tbl_student_picture t3 ON t1.`student_id` = t3.`student_id` WHERE t1.`teacher_id` = '$teacher_id' and subject_code = '$subject_code' and sem = '$sem_id'
// ORDER BY t2.`lastname` ASC");

if ($txt_search == "" || $txt_search == null) {

$stmt = mysqli_query($con, "SELECT t1.*,t2.*,t3.*,t1.`grade` AS stud_grade,t4.* FROM tbl_student_grades t1 
	LEFT JOIN tbl_students t2 ON t1.`student_id` = t2.`student_id` 
	LEFT JOIN tbl_student_picture t3 ON t1.`student_id` = t3.`student_id`
	LEFT JOIN tbl_subjects_uploaded t4 ON t1.`subject_code` = t4.`subject_code`  
	WHERE t1.`teacher_id` = '$teacher_id' and t1.subject_code = '$subject_code' and t1.sem = '$sem_id' ORDER BY t2.`lastname` ASC");

if (mysqli_num_rows($stmt)>0) {
	while ($row = mysqli_fetch_assoc($stmt)) {
		$studid = $row['student_id'];
	
		$popups = '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="190" class="img-thumbnail"/><hr><p class="text-center">'.$row['lastname'].', '.$row['firstname'].' '.$row['mi'].'</p><p class="text-center">'.$row['grade'].', '.$row['section'].'</p>';
		$data_title = '<b> Grade For '. $row['description'] .' <b/>';
		if($row['stud_grade'] >3.01){ ?>

			<script type="text/javascript">
				$('[data-toggle="popover"]').popover(); 
			</script>
			<div class="well w3-card" data-toggle="popover" data-title="<?php echo $data_title; ?>" data-placement="top" data-trigger="hover" data-content='<?php echo $popups; ?>' data-html="true">
				<span class="">
					<i class="fa fa-thumbs-down w3-text-red w3-large"></i> 
					<b>Failed <b class="w3-right"><?php echo $row['stud_grade']; ?></b></b> 
				</span>
				<h5 class="w3-medium">
					<?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
					if ($str != null) {
						echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
					}
					else
					{
						echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
					}
				?>
				</h5>
			</div>

		<?php 
		}else{ ?>

			<script type="text/javascript">
				$('[data-toggle="popover"]').popover(); 
			</script>
			<div class="well w3-card" data-toggle="popover" data-title="<?php echo $data_title; ?>" data-placement="top" data-trigger="hover" data-content='<?php echo $popups; ?>' data-html="true">
				<span class=""><i class="fa fa-thumbs-up w3-text-green w3-large"></i> 
					<b>Passed
						<b class="w3-right">
						<?php echo $row['stud_grade']; ?>
						</b>
					</b> 
				</span>
				<h5 class="w3-medium">
					<?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
					if ($str != null) {
						echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						
					}
					else
					{
						echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
					}
				?>
				</h5>
			</div>

		<?php 
		}

		?>

<?php		
	}
}
else
{
	?>
				<div class="well">No data found...</div>
				<?php
}

}else{

$stmt = mysqli_query($con, "SELECT t1.*,t2.*,t3.*,t1.`grade` AS stud_grade,t4.* FROM tbl_student_grades t1 
	LEFT JOIN tbl_students t2 ON t1.`student_id` = t2.`student_id` 
	LEFT JOIN tbl_student_picture t3 ON t1.`student_id` = t3.`student_id`
	LEFT JOIN tbl_subjects_uploaded t4 ON t1.`subject_code` = t4.`subject_code`  
	WHERE t1.`teacher_id` = '$teacher_id' and t1.subject_code = '$subject_code' and t1.sem = '$sem_id' and concat(t2.`lastname`,t2.`firstname`,t2.`mi`) like '%$txt_search%' ORDER BY t2.`lastname` ASC");

if (mysqli_num_rows($stmt)>0) {
	while ($row = mysqli_fetch_assoc($stmt)) {
		$studid = $row['student_id'];
	
		$popups = '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="190" class="img-thumbnail"/><hr><p class="text-center">'.$row['lastname'].', '.$row['firstname'].' '.$row['mi'].'</p><p class="text-center">'.$row['grade'].', '.$row['section'].'</p>';
		$data_title = '<b> Grade For '. $row['description'] .' <b/>';
		if($row['stud_grade'] > 3.01){ ?>


			<script type="text/javascript">
				$('[data-toggle="popover"]').popover(); 
			</script>
			<div class="well w3-card" data-toggle="popover" data-title="<?php echo $data_title; ?>" data-placement="top" data-trigger="hover" data-content='<?php echo $popups; ?>' data-html="true">
				<span class=""><i class="fa fa-thumbs-down w3-text-red w3-large"></i> <b>Failed <b class="w3-right"><?php echo $row['stud_grade']; ?></b></b> </span>
			<h5 class="w3-small">
				<?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
				if ($str != null) {
					echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
				}
				else
				{
					echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
				}
			?>
			</h5>
			</div>

		<?php 
		}else{ ?>

			<script type="text/javascript">
				$('[data-toggle="popover"]').popover(); 
			</script>
			<div class="well w3-card" data-toggle="popover" data-title="<?php echo $row['stud_grade'] ?>" data-placement="top" data-trigger="hover" data-content='<?php echo $popups; ?>' data-html="true">
				<span class=""><i class="fa fa-thumbs-up w3-text-green w3-large"></i> <b>Passed<b class="w3-right"><?php echo $row['stud_grade']; ?></b></b> </span>
			<h5 class="w3-small">
				<?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
				if ($str != null) {
					echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
				}
				else
				{
					echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
				}
			?>
			</h5>
			</div>

		<?php 
		}

		?>		
				
<?php		
	}
}
else
{
	?>
				<div class="well">No data found...</div>
				<?php
}

}




			

?>