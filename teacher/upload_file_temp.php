<?php
session_start();    // Increase 'timeout' time<br />
  
  if(!empty($_FILES["file"]["type"])){
        $fileName = time().'_'.$_FILES['file']['name'];
        $valid_extensions = array("pdf", "docx", "doc","jpeg", "jpg", "png","xls","xlsx","pptx","ppt","txt","rtf");
        $temporary = explode(".", $_FILES["file"]["name"]);
        $file_extension = end($temporary);
        if((($_FILES["file"]["type"] == "application/pdf") || ($_FILES["file"]["type"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") || ($_FILES["file"]["type"] == "application/msword") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "application/vnd.ms-excel") || ($_FILES["file"]["type"] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") || ($_FILES["file"]["type"] == "application/vnd.openxmlformats-officedocument.presentationml.presentation") || ($_FILES["file"]["type"] == "application/vnd.ms-powerpoint") || ($_FILES["file"]["type"] == "text/plain")) && ($_FILES["file"]["size"] < 20000000) && in_array($file_extension, $valid_extensions)){
            $sourcePath = $_FILES['file']['tmp_name'];
            $targetPath = "UploadedDocuments/".$fileName;
            if(move_uploaded_file($sourcePath,$targetPath)){
                $uploadedFile = "UploadedDocuments/".$fileName; ?>

                <?php 
                 //echo  $_FILES["file"]["type"];

                 if($_FILES["file"]["type"] == "image/jpeg" || $_FILES["file"]["type"] == "image/jpg" || $_FILES["file"]["type"] == "image/png"){ ?>

                  <a href="<?php echo $uploadedFile; ?>" target="_blank"><img src="<?php echo $uploadedFile; ?>" class="img img-resonsive img_size img-thumbnail"></a> 
                  <br>
                  <br>
                  <span class="btn btn-danger btn-block" onclick="del_temp_file('<?php echo $uploadedFile; ?>')">Remove</span>
                  <input type="hidden" id="file_name" name="file_name" value="<?php echo $uploadedFile; ?>">
                  <hr>
                <?php
                 }else{ ?>

                  <a href="<?php echo $uploadedFile; ?>" target="_blank" class="w3-hover-transparent"><img src="../img/doc.jpg" class="img img-resonsive img_size img-thumbnail"></a>
                  <br>
                  <br>
                  <h3 class="text-center"><?php echo $_FILES['file']['name']; ?></h3>
                  <span class="btn btn-danger btn-block" onclick="del_temp_file('<?php echo $uploadedFile; ?>')">Remove</span>
                  <input type="hidden" id="file_name" name="file_name" value="<?php echo $uploadedFile; ?>">
                  <hr>

                <?php 
                 }

                ?>

                <?php 
            }
        }
    }
?>

