<?php
session_start();
if (!isset($_SESSION['teacher_id'])) {
	@header('location:../');	
}
 include_once('../config.php');?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['teacher_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="../css/showToast.css">
</head>

<script type="text/javascript">
	function display_announce(){
		var start = document.getElementById('start');
		var limit = document.getElementById('limit');

		var mydata = 'start=' + start.value + '&limit=' + limit.value;
		$.ajax({
			type:"POST",
			url:"display_my_announcement.php",
			data:mydata,
			cache:false,
			success:function(data){
				$("#load_announce").html(data);
			}
		});
	}
</script>

<script type="text/javascript">
	function clear_update(){
		var a = $("#el").val();
		$("#"+a+"").show('fast');
		$("#post_ann").val('');
		$("#cat").val('');
		$("#user_id").val('');
		$("#btn_cupdate").hide('fast');
		$("#btn_ann").text('Post');
		$('#preview_file').html('');
		$('#f_name').val('');
	}
</script>
<script type="text/javascript">
	function post_new_announce(){
		var a = $("#el").val();
		// $("#"+a+"").show('fast');
		var post_ann = document.getElementById('post_ann');
		var cat = document.getElementById('cat');
		var user_id = document.getElementById('user_id');
		var f_name = document.getElementById('f_name');
		if (post_ann.value == "") 
		{
			post_ann.focus();
		}
		else
		{
			var mydata = 'post_ann=' + post_ann.value + '&cat=' + cat.value + '&user_id=' + user_id.value + '&f_name=' + f_name.value;
			// alert(mydata);
			$.ajax({
			type:"POST",
			url:"post_now.php",
			data:mydata,
			cache:false,
			beforeSend:function(){
				if (user_id.value == "")
				{
					$("#load_pend").prepend('<div class="panel panel-info w3-card-2 w3-animate-zoom w3-opacity"><div class="panel-heading">Uploading <button class="close"><i class="fa fa-chevron-down "></i></button></div><div class="panel-body"><p>'+post_ann.value+'</p></div><div class="panel-footer"><div class="text-right w3-text-dark-grey"><small><img src="../img/flat.gif" width="15" /></small></div></div></div>');
				}
				else
				{
					$("#"+a+"").html('<div class="panel panel-info w3-card-2 w3-animate-zoom w3-opacity"><div class="panel-heading">Uploading <button class="close"><i class="fa fa-chevron-down "></i></button></div><div class="panel-body"><p>'+post_ann.value+'</p></div><div class="panel-footer"><div class="text-right w3-text-dark-grey"><small><img src="../img/flat.gif" width="15" /></small></div></div></div>');
				}
				document.getElementById('btn_ann').disabled = true;
			},
			success:function(data){
				// alert(data);
				document.getElementById('btn_ann').disabled = false;
				if (data == 1) 
				{
				setTimeout(function(){
					showToast.show('Announcement has been posted!',2000);
				display_announce();
				$("#load_pend").html('');
				},2000);
				$("#post_ann").val('');
				$("#cat").val('');
				$("#f_name").val('');
				$('#preview_file').html('');
				}
				else if (data == 2) 
				{
				setTimeout(function(){
					showToast.show('Announcement has been updated!',2000);
				display_announce();
				$("#load_pend").html('');

				},2000);
					clear_update();
				}
			}		
			});
		}
	}
</script>
<script type="text/javascript">
	function add_category(){
	var category = document.getElementById('category');

	if (category.value == "") 
	{
		category.focus();
	}
	else
	{
		var mydata = 'category=' + category.value;
		$.ajax({
			type:"POST",
			url:"add_category.php",
			data:mydata,
			cache:false,
			success:function(data){
				if (data == 1) 
				{
					showToast.show("Category has been save !",1000);
					$("#category").val('');
					show_cat();
					$("#add_cat").modal('hide');
				}
				else
				{
					alert(data);
				}
			}
		});
	}
	}


	function show_cat(){
		$.ajax({
			type:"POST",
			url:"display_cat.php",
			data:"",
			cache:false,
			success:function(data){
				$("#display_mycat").html(data);
			}
		});
		// $("#display_mycat").html(data);
	}
</script>

<body onload="display_announce(); show_cat();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">

		<div class="">
			<input type="hidden" name="el" id="el">
			<input type="hidden" name="user_id" id="user_id">
			<input type="hidden" name="start" id="start" value="0">
			<input type="hidden" name="limit" id="limit" value="10">
			<a href="index.php" class="btn btn-primary btn-block">Home</a>
			<h4>Announcement</h4>
			<hr>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon">Choose Category</span>
					<div id="display_mycat"></div>
					<span class="input-group-addon w3-green w3-ripple" data-toggle="modal" data-target="#add_cat"><i class=" fa fa-plus"></i></span>
				</div>
			</div>
			<div class="form-group">
				<textarea class="form-control" id="post_ann" name="post_ann" placeholder="Write announcement..." style="resize: none; height:100px;	"></textarea>
			</div>
			<div class="form-group">
		        <input type="file" class="form-control" id="file" name="file" style="display: none;"/>
		        <button type="button" class="btn btn-info btn-small" id="btn-select" onclick="$('#file').click();"><span class="fa fa-file"></span> Add Attachment</button>
		        <button type="button" class="btn btn-danger btn-small" id="btn_remove_file" style="display: none;"><span class="fa fa-file"></span> Remove Attachment</button>
		    </div>
		    <input type="hidden" id="f_name" name="f_name">
			<div class="text-center" id="preview_file"></div>
			<button class="btn btn-success btn-block" id="btn_ann" onclick="post_new_announce();">Post</button>
			<button style="display: none;" class="btn btn-danger btn-block" onclick="clear_update();" id="btn_cupdate">Cancel</button>
			<hr>
		</div>
		<div id="load_pend"></div>
		<div id="load_announce"></div>
		<!-- <div class="well">No Records found...</div> -->
		</div>

		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>
<script src="../js/showToast.js"></script>
<style type="text/css">
		.img_size{
			width: 250px;
			height: 250px;
			object-fit: cover;
		}
		.img_size_150{
			width: 150px;
			height: 150px;
			object-fit: cover;
		}
	</style>
</html>

<?php include_once('modals.php'); ?>


<div class="modal fade" role="dialog" id="add_cat">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Add Category <button class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Category</span>
						<input type="text" name="category" id="category" class="form-control" placeholder="Enter category...">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button onclick="add_category();" class="btn btn-primary btn-block">Save</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	//file type validation
	$(document).ready(function(){
		$("#file").change(function() {
	        var file = this.files[0];
	        var imagefile = file.type;
	        var form_data = new FormData();
	        //alert(imagefile);
	        $('#doc_type').val(imagefile.trim(""));
	        var match= ["application/pdf","application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword","image/jpeg","image/png","image/jpg", "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.ms-powerpoint", "text/plain"];
	        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3]) || (imagefile==match[4]) || (imagefile==match[5]) || (imagefile==match[6]) || (imagefile==match[7]) || (imagefile==match[8]) || (imagefile==match[9]) || (imagefile==match[10]))){
	            alert('INVALID FILE!');
	            $("#file").val('');
	            return false;
	        }else{


				form_data.append("file", document.getElementById('file').files[0]);

	        	$.ajax({
			      url:"upload_file_temp.php",
			      method:"POST",
			      data: form_data,
			      contentType: false,
			      cache: false,
			      processData: false,
			      beforeSend:function(data){
			        $('#preview_file').html("<label class='text-success col-sm-12'> Uploading File... <span class='fa fa-spinner fa-spin'></span></label>");      
			      },   
			      success:function(data)
			      {
			       if(data == 404){

					swal("Warning", "Song Already Exists.", "warning");
					$('#preview_file').html("<label class='text-danger col-sm-12'>Music Uploading Failed... <span class='fa fa-spinner fa-spin'></span></label>");  

			       }else{

			          $('#preview_file').html(data);
			          var file_name = $('#file_name').val();
			          $('#f_name').val(file_name);
			       }
			      
			      }
			      });


	        }
	    });
	});
	    
</script>