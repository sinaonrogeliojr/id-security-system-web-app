<?php
session_start();
if (!isset($_SESSION['teacher_id'])) {
	@header('location:../');	
}
 include_once('../config.php');?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['teacher_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<script type="text/javascript">
	function visit_student(id){
		var tid = id;
		var info = 'tid=' + tid;
		$.ajax({
			type:"POST",
			url:"get_session_from_student.php",
			data:info,
			cache:false,
			success:function(data){
				//alert(data);
				if (data == 1) 
				{
					window.location="../client";
				}
				else
				{
					alert(data);
				}
			}
		});
	}
</script>
<script type="text/javascript">

	function get_up_pics(){
		$.ajax({
			type:"POST",
			url:"new_pic.php",
			data:"",
			cache:false,
			success:function(data){
				$("#my_pictures").html(data);
			}
		});
	}

		function get_up_list(){
		$(document).ready(function(){
			var start = 0;
			var limit = 5;
			var fidle= 'inactive';
			function get_append(start,limit){
				$.ajax({
				type:"POST",
				url:"my_student.php",
				data:{start:start, limit,limit},
				cache:false,

				success:function(data){
					$("#loader_first").html('');
					// alert(data);
					if (data == 1) 
					{
							$("#mystudent").append('<div class="well">End of records</div>');
							fidle='active';
							$("#loader").html('');
					}
					else
					{
							
							$("#mystudent").append(data);
							$("#loader").html('<div class="well">Load more data..</div>');
							fidle='inactive';
					}
				}
			});
			}
			if (fidle == 'inactive') 
			{
				fidle = 'active';
				get_append(start,limit);
			}

			$(window).scroll(function(){
					if ($(window).scrollTop() + $(window).height() > $("#mystudent").height() && fidle == 'inactive')
					{
					fidle = 'active';
					start = start + limit;
				//	alert(start);
					setTimeout(function(){
					get_append(start,limit);
					});
					}
				});
			});
	}

function load_first(){
	$("#loader_first").html('<center><img src="../img/flat.gif" width="50"></center>');
	setTimeout(function(){

		get_up_list();
	},1000);
}


	function get_up_list_src(id){
		var myid = id;
		var	me = document.getElementById('src');
		if (me.value == "") 
		{
			$("#mystudent").show('fast');
			$("#loader").show('fast');	
			$("#src_mystudent").hide('fast');
			$("#rem_text").hide('fast');
		}
		else
		{
		$("#rem_text").show('fast');
		var myinfo = 'myid=' + myid;
		$.ajax({
			type:"POST",
			url:"my_student.php",
			data:myinfo,
			cache:false,
			beforeSend:function(){
				$("#mystudent").hide('fast');
				$("#loader").hide('fast');	
				$("#src_mystudent").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				if (data == 1) 
				{
					$("#src_mystudent").html('<div class="well">No records found...</div>');
				}
				else
				{
					$("#src_mystudent").show('fast');
					$("#src_mystudent").html(data);
				}
				
			}
		});
		}
		
	}
		function load_pics(){
		$.ajax({
			type:"POST",
			url:"new_pic.php",
			data:"",
			cache:false,
			beforeSend:function(){
				$("#my_pictures").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#my_pictures").html(data);
			}
		});
	}</script>

<script type="text/javascript">
	setInterval(function(){get_up_pics();},2000);
</script>
<body  onload="load_pics(); load_first();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<div class="text-center">
				<a href="index.php" class="btn btn-primary btn-block">Back to Home</a>
				<br>
			<div id="my_pictures"></div>
			</div>
			<h3 class="w3-medium w3-padding-top text-center w3-text-dark-grey"><b><?php echo $_SESSION['teacher_fullname'] ?></b></h3>
			<h3 class="w3-medium w3-padding-top text-center w3-text-dark-grey"><b><?php echo $grade = $_SESSION['teacher_grade'].' - '. $section=$_SESSION['teacher_advisory'] ?></b></h3>
			<hr>
		
			<div class="">
				<div class="form-group">
					<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-search"></i></span>
					<input type="text" name="src" id="src" onkeyup="get_up_list_src(this.value);"  onblur="get_up_list_src(this.value);"  onfocus="get_up_list_src(this.value);" class="form-control" placeholder="Search...">
					<span class="input-group-addon"><i class="fa fa-remove" style="display: none;" id="rem_text" onclick="$('#src').val(''); $('#src').focus();"></i></span>
				</div>
				</div>
				<br>
				<div id="loader_first"></div>
				<div id="src_mystudent"></div>
				<div id="mystudent"></div>
				<div id="loader"></div>
			</div>
		</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>

</html>

<?php include_once('modals.php'); ?>