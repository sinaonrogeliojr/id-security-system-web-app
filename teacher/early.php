<?php
session_start();
if (!isset($_SESSION['teacher_id'])) {
	@header('location:../');	
}
 include_once('../config.php')
 ;?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['teacher_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<script type="text/javascript">
	function get_late(){
		var mydate = document.getElementById('src_date');
		var myinfo = 'mydate=' + mydate.value;
		$.ajax({
			type:"POST",
			url:"tbl_early.php",
			data:myinfo,
			cache:false,
			beforeSend:function(){
			$("#tbl_absent").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				//console.log(data);
				if (data == "	") 
				{
					$("#tbl_absent").html('<div class="well">No data found...</div>')
				}
				else
				{
					$("#tbl_absent").html(data);
				}
				
			}
		});
	}
</script>
<script type="text/javascript">
	function go_btn(){
		var mydate = document.getElementById('src_date');
		var param = document.getElementById('param');

		if (mydate.value == "" || mydate.value == param.value) 
		{
			$("#clear_date").hide('fast');
			$("#remover").hide('fast');
			$("#label").show('fast');
			get_late();
		}
		else
		{
		$("#clear_date").show('fast');
		$("#remover").show('fast');
			$("#label").hide('fast');
		
	}
}
</script>

<script type="text/javascript">
	function load_absent(){
			var mydate = document.getElementById('src_date');
		var param = document.getElementById('param');
		var myinfo = 'mydate=' + mydate.value;
		// alert(myinfo);
		$.ajax({
			type:"POST",
			url:"tbl_early.php",
			data:myinfo,
			cache:false,
			beforeSend:function(){
			$("#tbl_absent").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				console.log("executing data...");
			$("#tbl_absent").html(data);
			}
		});
	}

	function clear_filter(){
		var mydate = document.getElementById('param');
			$("#clear_date").hide('fast');
			$("#remover").hide('fast');
			$("#label").show('fast');
			$("#src_date").val(mydate.value);
			get_late();
	}
</script>
<body onload="get_late();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<a href="index.php" class="btn btn-primary btn-block">Back</a>
			<h5 class=" w3-padding-top"><b><?php echo $_SESSION['teacher_grade'].' - '.$_SESSION['teacher_advisory'] ?></b></h5>	
			<br>
				<h5 class=""><i class="fa fa-twitter w3-small"></i> EARLY BIRDS</h5>
			<hr>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-calendar" id="label"></i> <i id="remover" style="display: none;" onclick="clear_filter();" class="fa fa-remove"></i></span>
					<input type="hidden" name="param" id="param" value="<?php echo date('Y-m-d') ?>">
					<input type="date" name="src_date" oninput="go_btn();"  id="src_date" class="form-control input-lg" value="<?php echo date('Y-m-d') ?>">
					<span class="input-group-addon w3-blue"><i onclick="load_absent();" id="clear_date" style="display: none;"> <b>Search</b> </i></span>
				</div>
			</div>

			<div id="tbl_absent"></div>

		</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>

</body>





<script src="../js/jquery.mins.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>

</html>