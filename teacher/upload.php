<?php
session_start();
if (!isset($_SESSION['teacher_id'])) {
	@header('location:../');	
}
include_once('../config.php');

$teacher_id = $_SESSION['teacher_id'];

 ?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['teacher_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<body>
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">

		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<a href="index.php" class="btn btn-primary btn-block">Back</a>
			<h6 class=" w3-padding-top"><b><?php echo $_SESSION['teacher_fullname'] ?></b></h6>
			<h6 class=" w3-padding-top"><b><?php echo $_SESSION['teacher_grade'].' - '.$_SESSION['teacher_advisory'] ?></b></h6>	
			<br>
				<h5 class=""><i class="fa fa-upload"></i> Upload Grades</h5>
				<br>
				<div class="form">
					<form method="post" id="export_excel">  
                     <label>Select Excel</label>  
                     <input type="file" name="excel_file" id="excel_file" />  
                	</form> 
                	<span id="loader" style="display: none;"><span class="fa fa-spin fa-spinner"></span> Uploading... </span>
                	<br />  
                <br />  
                <div id="result">  
                </div>  
				</div>
			<hr>
		</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>

</body>



<script src="../js/jquery.mins.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>

<script>  
 $(document).ready(function(){  
      $('#excel_file').change(function(){  
           $('#export_excel').submit();  
      });  
      $('#export_excel').on('submit', function(event){  
           event.preventDefault();  
           $.ajax({  
                url:"export.php",  
                method:"POST",  
                data:new FormData(this),  
                contentType:false,  
                processData:false,  
                beforeSend:function(){
                	$("#loader").css("display","block");
                },
                success:function(data){  
                     $('#result').html(data);  
                     $('#excel_file').val('');
                     $("#loader").css("display","none");  
                }  
           });  
      });  
 });  
 </script>  

</html>