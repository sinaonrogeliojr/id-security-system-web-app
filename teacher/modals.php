<div class="modal fade" role="dialog" id="logout_me">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Logout</div>
			<div class="modal-body">
				<h5>Do you want to logout your account ?</h5>
				<hr>
				<a href="logout.php" class="btn btn-block btn-primary">Yes</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-default">No</a>
			</div>
		</div>
	</div>
</div>
<!--separator -->
<div class="modal fade" role="dialog" id="show_mypic">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Profile Picture</div>
			<div class="modal-body text-center">
				<?php 
				echo '<img src="data:image/jpeg;base64,'.base64_encode($_SESSION['teacher_pict']).'" class="img-thumbnail animated jackInTheBox" width="220"/>';
			 ?>
			 <hr>
			 <h5><?php echo $_SESSION['teacher_fullname'] ?></h5>
			 <h5><?php echo $_SESSION['teacher_grade'] ?></h5>
			</div>

			<form  method="post"  enctype="multipart/form-data">
				<input type="file" name="image" id="image"  style="display: none;'">
				<div style="margin:20px 20px 5px 20px;">
					<input type="submit" id="btn_sub" name="btn_sub" style="display: none;" value="Update Picture" type="submit" class="btn btn-success btn-block">
				</div>
			</form>
			<div style="margin:0px 20px 20px 20px;">
				<button id="change_btn" class="btn btn-block btn-primary" onclick="$('#image').click();">Change Picture</button>
				<button onclick="close_change_pic();" data-dismiss="modal" class="btn btn-block btn-danger">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- UPLOAD SCHEDULE -->
<div class="modal fade" role="dialog" id="upload_schedule">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" id="m-header">UPLOAD SCHEDULE</div>
			<div class="modal-body text-center">
				<div class="form">
					<form method="post" id="export_excel">  
	                     <input type="file" name="excel_file" id="excel_file" style="display: none;"/>
	                     <button type="button" class="btn btn-default btn-small btn-block" id="btn-select" onclick="$('#excel_file').click();"><span class="fa fa-file"></span> Select File</button>  
                	</form>
                	<label id="loader"></label>    
	                <div id="result">  
	                </div>  
				</div>
			</div>
			
			<div style="margin:0px 20px 20px 20px;">
				<button data-dismiss="modal" class="btn btn-block btn-danger">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- UPLOAD SCHEDULE -->

<?php 
	if (isset($_POST['btn_sub'])) {
		include_once("../config.php");
		$myid = $_SESSION['teacher_id'];
		$imageName=mysqli_real_escape_string($con,$_FILES["image"]["name"]);
		$imageData=mysqli_real_escape_string($con,file_get_contents($_FILES["image"]["tmp_name"]));
		$imageType=mysqli_real_escape_string($con,$_FILES["image"]["type"]);
		$date=Date("l F d, Y h:i:sa");
		?>
		<?php
		if(substr($imageType,0,5) == "image")
		{	

			$update = mysqli_query($con,"UPDATE tbl_teacher_picture set image='$imageData' where teacher_id='$myid'");	
			if ($update) {

			$stmt = mysqli_query($con,"SELECT a.*,b.* from tbl_teachers a left join tbl_teacher_picture b on a.teacher_id = b.teacher_id where b.teacher_id='$myid'");
			$injec = mysqli_fetch_assoc($stmt);
			$_SESSION['teacher_pict'] = $injec['image'];
			?>
			<script type="text/javascript">
				alert("Profile Picture has been updated !");
				window.location="index.php";
			</script>
			<?php
		}
		}	
		else{
			?>
			<script type="text/javascript">
				alert("Sorry Invalid image format or file is too large ! ");
			</script>
			<?php
		}
	}
 ?>

<script type="text/javascript">
	function close_change_pic(){
		$("#btn_sub").hide('fast');
		$("#change_btn").show('fast');
		$("image").val('');
	}
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#image").on('change',function(){
			var mypicfile = document.getElementById('image');
			var info = mypicfile.value;
			if (mypicfile.value == "") 
			{
				$("#btn_sub").hide('fast');
				$("#change_btn").show('fast');
			}
			else
			{
				$("#btn_sub").show('fast');
				$("#change_btn").hide('fast');
				//alert(info);
			}
		});
	});


	$(document).ready(function(){  

      $('#excel_file').change(function(){  
           $('#export_excel').submit();  
      });  

      $('#export_excel').on('submit', function(event){  
           event.preventDefault();  
           $.ajax({  
                url:"export.php",  
                method:"POST",  
                data:new FormData(this),  
                contentType:false,  
                processData:false,  
                beforeSend:function(){
                	$('#loader').html('Please Wait Uploading');
                },
                success:function(data){  
                     $('#result').html(data);  
                     $('#excel_file').val('');
                     $('#loader').css("display","none");
                     showToast.show("Schedule Successfully Saved",1000);
                     $('#loader').html('');
                     setTimeout(function()
                     	{  $('#upload_schedule').modal('hide'); 
                     	load_grades()
                     },2000);
                }  
           });  
      });  
 });
</script>