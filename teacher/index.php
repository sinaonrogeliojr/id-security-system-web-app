<?php
session_start();
if (!isset($_SESSION['teacher_id'])) {
	@header('location:../');
}
 include_once('../config.php');?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['teacher_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<script type="text/javascript">
	function get_up_pics(){
		$.ajax({
			type:"POST",
			url:"new_pic.php",
			data:"",
			cache:false,
			success:function(data){
				$("#my_pictures").html(data);
			}
		});
	}
		function load_pics(){
		$.ajax({
			type:"POST",
			url:"new_pic.php",
			data:"",
			cache:false,
			beforeSend:function(){
				$("#my_pictures").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#my_pictures").html(data);
			}
		});
	}


	function check_notif(){
		$.ajax({
			type:"POST",
			url:"checK_notif.php",
			data:"",
			cache:false,
			success:function(data){
				//alert(data);
				// if (data == 1 || data == 2)
				// {
				// 	$("#new_notif").hide('fast');
				// }
				// else if(data == 0)
				// {
				// 	$("#new_notif").show('fast');
				// }

				if (data == 0) 
				{
					$("#new_notif").hide('fast');
				}
				else
				{
					$("#new_notif").show('fast');
					$("#new_notif").text(data);
				}
				
			}
		});
	}
</script>

<script type="text/javascript">
	setInterval(function(){get_up_pics(); check_notif();},2000);
</script>
<body onload="load_pics(); check_notif();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<div class="text-center">
			<div id="my_pictures"></div>
			</div>
			<h3 class="w3-medium w3-padding-top text-center w3-text-dark-grey"><b><?php echo $_SESSION['teacher_fullname'] ?></b></h3>
			<h3 class="w3-medium w3-padding-top text-center w3-text-dark-grey"><b><?php echo $_SESSION['teacher_grade'].' - '.$_SESSION['teacher_advisory'] ?></b></h3>
			<hr>
			<div class="">
			
				<?php 
				$str = $_SESSION['teacher_advisory'];
				$strcount = strlen($str);
					$section = $str;
				 ?>
				<div class="btn-group btn-group-justified btn-block">
				<a href="myinfo.php" class="btn btn-block w3-border-right btn-lg w3-blue w3-hover-shadow w3-small" ><i class="fa fa-user"></i> MY INFO</a>
				<a href="announce_room.php" class="btn btn-block btn-lg w3-blue w3-hover-shadow w3-small w3-border-left"> <i class="fa fa-home"></i> UPLOAD ANNOUNCEMENT</a>
				
				</div>

				<div class="btn-block"><h4 class="text-left w3-small" style="color:blue; font-weight: bolder;">ANNOUNCEMENT</h4></div>
				<div class="btn-group btn-group-justified btn-block">
					<a href="announce.php" class="btn btn-block btn-lg w3-blue w3-hover-shadow w3-small w3-border-right" style="text-align: center;"><span class="badge w3-red" id="new_notif" style="display: none;"><b> <i class="fa fa-envelope"></i> </b></span> <i class="fa fa-flag"></i> SCHOOL </a>
					<a href="calendar_event.php" class="btn btn-block w3-border-right btn-lg w3-blue w3-hover-shadow w3-small" ><i class="fa fa-calendar"></i> CALENDAR EVENT</a>
				
				<a href="documents.php" class="btn w3-border-left btn-block btn-lg w3-blue w3-hover-shadow w3-small"><i class="fa fa-list-alt"></i> DOCUMENTS</a>
				 
				</div>
				
				
				<div class="btn-block"><h4 class="text-left w3-small" style="color:blue; font-weight: bolder;">STUDENT'S INFORMATION</h4></div>
				<div class="btn-group btn-group-justified btn-block">
					<a href="advisory.php" class="btn btn-block w3-border-right btn-lg w3-blue w3-hover-shadow w3-small" ><i class="fa fa-home"></i> <?php echo $_SESSION['teacher_grade'].' - '.$section?></a>
					<a href="attendance.php" class="btn w3-border-left btn-block btn-lg w3-blue w3-hover-shadow w3-small"><i class="fa fa-list-alt"></i> STUDENT ATTENDANCE</a>
					<a href="sems_uploaded.php" class="btn w3-border-left btn-block btn-lg w3-blue w3-hover-shadow w3-small"><i class="fa fa-circle"></i> GRADES</a>
				</div>
				
				<div class="btn-group btn-group-justified btn-block">
					<a href="early.php" class="btn btn-block btn-lg w3-blue w3-hover-shadow w3-small  w3-border-right"><i class="fa fa-twitter"></i> EARLY BIRDS</a>

				<a href="late_absent.php" class="btn btn-block btn-lg w3-blue w3-hover-shadow w3-small  w3-border-left  w3-border-right"><i class="fa fa-remove"></i> ABSENT</a>

				<a href="late.php" class="btn btn-block btn-lg w3-blue w3-hover-shadow w3-small  w3-border-left"><i class="fa fa-warning"></i> LATE</a>

				</div>
				<div class="btn-group btn-group-justified btn-block">
				
				
				</div>
				<?php 
				if (isset($_SESSION['pr_id'])) {
					?>
						<a href="../pr_pane/break_teach.php" class="btn btn-block btn-lg w3-red w3-hover-shadow w3-small" style="text-align:left;"><i class="fa fa-arrow-left"></i> HOME</a>
					<?php
				}
				else
				{
					?>
						<a href="#logout" data-toggle="modal"  data-target="#logout_me" class="btn btn-block btn-lg w3-red w3-hover-shadow w3-small" style="text-align:left;"><i class="fa fa-arrow-left"></i> LOGOUT</a>
					<?php
				}

				 ?>
			<br><br>
			</div>
			
		</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="teacher.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>

</html>

<?php include_once('modals.php'); ?>