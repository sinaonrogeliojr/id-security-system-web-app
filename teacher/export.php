<?php  

session_start();
if (!isset($_SESSION['teacher_id'])) {
  @header('location:../');  
}
include_once('../config.php');
$teacher_id = $_SESSION['teacher_id'];
 //export.php  
 if(!empty($_FILES["excel_file"]))  
 {  
      $file_array = explode(".", $_FILES["excel_file"]["name"]);  
      if($file_array[1] == "xlsx")  
      {  
           include("PHPExcel/IOFactory.php");  
           $output = '';  
           $output .= "  
           <label class='text-success'>Data Inserted</label>  
                <table class='table table-bordered'>  
                     <tr>  
                          <th>Student ID</th>  
                          <th>Grade</th>  
                          <th>Sem</th>  
                          <th>Subject Code</th>   
                     </tr>  
                     ";  

           $object = PHPExcel_IOFactory::load($_FILES["excel_file"]["tmp_name"]);  
           $object2 = PHPExcel_IOFactory::load($_FILES["excel_file"]["tmp_name"]);  

           foreach($object->getWorksheetIterator() as $worksheet)  
           {  

                $highestRow = $worksheet->getHighestRow();

                $subject_code = $worksheet->getCell('C6')->getValue();
                $subject = $worksheet->getCell('C8')->getValue();
                $desc = $worksheet->getCell('C9')->getValue();
                $sem = $worksheet->getCell('C3')->getValue();
                $sem_year = $worksheet->getCell('C4')->getValue();
                //echo 1;
                // $sql = mysqli_query($con, "INSERT INTO tbl_subjects_uploaded(teacher_id,sem,subject_code) VALUES ('$teacher_id','$sem','$subject_code')");
                //     if($sql){
                //       echo "insert";
                //     }
                insert_subject($con,$teacher_id,$sem,$subject_code,$desc,$subject,$sem_year);
               
                for($row=12; $row<=$highestRow; $row++)  
                {  

                     $no = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(0, $row)->getValue()); 
                     $student_id = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(1, $row)->getValue());  
                     $grade = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(3, $row)->getValue());  
         
                      if($no === ""){

                      }else{

                        $chk_data = mysqli_query($con, "SELECT * FROM tbl_student_grades where student_id = '$student_id' and subject_code = '$subject_code' and sem = '$sem'");

                        if(mysqli_num_rows($chk_data) > 0){
                          //update
                          //echo 'update';
                          $query = "  
                         UPDATE tbl_student_grades set grade = '".$grade."' where student_id = '".$student_id."' and subject_code = '".$subject_code."' and sem = '".$sem_year."' and sem_year = '".$sem_year."'";  
                        }else{
                          //echo 'insert';
                          //insert
                          $query = "  
                         INSERT INTO tbl_student_grades(student_id, grade,sem,subject_code,teacher_id)
                         VALUES ('".$student_id."', '".$grade."', '". $sem_year ."', '". $subject_code ."', '".$teacher_id."')  
                         ";  
                        }

                         mysqli_query($con, $query);  
                         $output .= '  
                         <tr>  
                              <td>'.$student_id.'</td>  
                              <td>'.$grade.'</td>  
                              <td>'.$sem_year.'</td>  
                              <td>'.$subject_code.'</td>  
                         </tr>  
                         '; 

                      }
   
                }  
           }  

           $output .= '</table>';  
           echo $output;  

      }  
      else  
      {  
           echo '<label class="text-danger">Invalid File</label>';  
      }  
 }  


function insert_subject($con,$teacher_id,$sem,$subject_code,$desc,$subject,$sem_year){

  $chk_subject = mysqli_query($con, "SELECT * from tbl_subjects_uploaded where teacher_id = '$teacher_id' and sem = '$sem' and subject_code = '$subject_code'");

  if(mysqli_num_rows($chk_subject) > 0){
    $usql = mysqli_query($con, "UPDATE tbl_subjects_uploaded set teacher_id = '$teacher_id',sem = '$sem', subject_code = '$subject_code', subject = '$subject', description = '$desc',sem_year = '$sem_year' WHERE teacher_id = '$teacher_id' and sem = '$sem' and subject_code = '$subject_code'");

    if($usql){
      //echo "update";
    }

  }else{
    $sql = mysqli_query($con, "INSERT INTO tbl_subjects_uploaded(teacher_id,sem,subject_code,subject,description,sem_year) VALUES ('$teacher_id','$sem','$subject_code','$subject','$desc','$sem_year') ");
    if($sql){
      //echo "insert";
    }
  }

}

 ?>  