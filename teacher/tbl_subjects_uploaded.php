<?php
session_start();
include_once("../config.php");

$teacher_id = $_SESSION['teacher_id'];
$sem_id = mysqli_real_escape_string($con, $_POST['sem_id']);
$stmt = mysqli_query($con,"SELECT * from tbl_subjects_uploaded where teacher_id='$teacher_id' and sem_year = '$sem_id' order by subject_code desc");

if (mysqli_num_rows($stmt)>0) {
	while ($row = mysqli_fetch_assoc($stmt)) { ?>
		
		<div class="well w3-card-2 w3-hover-teal">
							
			<form method="GET" action="Grades.php">  
			<p class="w3-small">
				<?php echo $row['subject_code'] ?> - <?php  echo $row['subject']; ?>  <br>  
				<span class="w3-xlarge"><?php echo $row['description']; ?></span>
			</p>	
			<input type="text" style="display: none" id="sem_id" name="sem_id" value="<?php echo $row['sem_year']; ?>" autocomplete="off"/>
            <input type="text" style="display: none" id="subject_code" name="subject_code" value="<?php echo $row['subject_code']; ?>" autocomplete="off"/>
            <button  class="close" id="btn_view_grades" style="margin-top: -46px;" type="submit"><i class="fa fa-chevron-right"></i></button>

			</form>
		
		</div>
		<hr/>
<?php		
	}
}
else
{
?>
	<div class="well">No data found...</div>
<?php
}

?>