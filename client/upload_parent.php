<script type="text/javascript">
	function get_parent_pic(img,name,id){
		document.getElementById("my_current_pic").src = img;
		$("#my_name").html(name);
		$("#pid").val(id);
		$("#edit_pic_parent").modal('show');
	}
</script>

<div class="modal fade" role="dialog" id="edit_pic_parent">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">Edit Parent Picture <button class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<center><img src="" id="my_current_pic" class="img-thumbnail animated jackInTheBox" width="220"/></center>
				<hr>
				<center><div id="my_name"></div></center>
				<form  method="post"  enctype="multipart/form-data">
					<input type="hidden" name="pid" id="pid">
					<input type="file" name="images" id="images"  style="display: none;'">
						<div style="margin:20px 20px 5px 20px;">
					<input type="submit" id="btn_sub_parent" name="btn_sub_parent" style="display: none;" value="Update Picture" type="submit" class="btn btn-success btn-block">
				</div>
				</form>
				<div style="margin:0px 20px 20px 20px;">
					<button id="change_btn_parent" class="btn btn-block btn-primary" onclick="$('#images').click();">Change Picture</button>
					<button onclick="close_change_pic();" data-dismiss="modal" class="btn btn-block btn-danger">Close</button>
				</div>
			</div>
			<div class="modal-footer">
				
			</div>
		</div>
	</div>
</div>

<?php 
	if (isset($_POST['btn_sub_parent'])) {
		//session_start();
		include_once("../config.php");
		$p_id = $_POST['pid'];
		$imagesName=mysqli_real_escape_string($con,$_FILES["images"]["name"]);
		$imagesData=mysqli_real_escape_string($con,file_get_contents($_FILES["images"]["tmp_name"]));
		$imagesType=mysqli_real_escape_string($con,$_FILES["images"]["type"]);
		$date=Date("l F d, Y h:i:sa");
		?>
		<?php
		if(substr($imagesType,0,5) == "image")
		{	

			$update = mysqli_query($con,"UPDATE tbl_parent_picture set image='$imagesData' where parent_id='$p_id'");	
			if ($update) {
			?>
			<script type="text/javascript">
				alert("Profile Picture has been updated !");
				//window.location="guardian.php";
			</script>
			<?php
		}
		}	
		else{
			?>
			<script type="text/javascript">
				alert("Sorry Invalid images format or file is too large ! ");
			</script>
			<?php
		}
	}
 ?>



<script type="text/javascript">
	function close_change_pic(){
		$("#btn_sub_parent").hide('fast');
		$("#change_btn_parent").show('fast');
		$("images").val('');
	}
</script>
