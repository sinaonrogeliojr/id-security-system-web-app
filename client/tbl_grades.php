	<?php
session_start();
include_once("../config.php");

$grade = $_SESSION['grade'];
$section=$_SESSION['section'];
$student_id = $_SESSION['id'];
$sem_id = mysqli_real_escape_string($con, $_POST['sem_id']);
// $stmt = mysqli_query($con,"SELECT t1.*,CONCAT(t2.`lastname`, " ", t2.`firstname`, " ", t2.`mi`) AS student_name,t3.`image` FROM tbl_student_grades t1
// LEFT JOIN tbl_students t2 ON t1.`student_id` = t2.`student_id` 
// LEFT JOIN tbl_student_picture t3 ON t1.`student_id` = t3.`student_id` WHERE t1.`teacher_id` = '$teacher_id' and subject_code = '$subject_code' and sem = '$sem_id'
// ORDER BY t2.`lastname` ASC");

$stmt = mysqli_query($con, "SELECT t1.*,t2.*,t3.*,t1.`grade` AS stud_grade,t4.* FROM tbl_student_grades t1 
	LEFT JOIN tbl_students t2 ON t1.`student_id` = t2.`student_id` 
	LEFT JOIN tbl_student_picture t3 ON t1.`student_id` = t3.`student_id`
	LEFT JOIN tbl_subjects_uploaded t4 ON t1.`subject_code` = t4.`subject_code`
	WHERE t1.`student_id` = '$student_id' and t4.`sem_year` = '$sem_id'");

if (mysqli_num_rows($stmt)>0) {
	while ($row = mysqli_fetch_assoc($stmt)) {
		$studid = $row['student_id'];
	
		$popups = '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="190" class="img-thumbnail"/>';
		
		if($row['stud_grade'] > 3.01){ ?>

			<script type="text/javascript">
				$('[data-toggle="popover"]').popover(); 
			</script>
			<div class="well w3-card-2 w3-hover-red" data-html="true">
				<span class=""><i class="fa fa-thumbs-down w3-text-black w3-large"></i> <b>Failed<b class="w3-right"><?php echo $row['subject_code']; ?> - <?php echo $row['subject']; ?></b></b> </span>
			<h5 class="w3-small">
				<?php echo '<h3> '.$row['description'].'  - <b> '.$row['stud_grade'].' </b> </h3> ' ?> <?php $str = $row['mi'];  
			?>
			</h5>
			</div>

		<?php 
		}else{ ?>

			<script type="text/javascript">
				$('[data-toggle="popover"]').popover(); 
			</script>
			<div class="well w3-card-2 w3-hover-teal" data-html="true">
				<span class=""><i class="fa fa-thumbs-up w3-text-black w3-large"></i> <b>Passed<b class="w3-right"><?php echo $row['subject_code']; ?> - <?php echo $row['subject']; ?></b></b> </span>
			<h5 class="w3-small">
				<?php echo '<h3> '.$row['description'].'  - <b> '.$row['stud_grade'].' </b> </h3> ' ?> <?php $str = $row['mi'];  
			?>
			</h5>
			</div>

		<?php 
		}

		?>
	
<?php		
	}
}
else
{
	?>
		<div class="well">No data found...</div>
		<?php
}






			

?>