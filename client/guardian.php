<?php
session_start();
if (!isset($_SESSION['id'])) {
	@header('location:../');	
}
 include_once('../config.php')
 ;?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<script type="text/javascript">
	function show_guardians(){
		$.ajax({
			type:"POST",
			url:"show_guardians.php",
			data:"",
			cache:false,
			beforeSend:function(){
				$("#guardians").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#guardians").html(data);
			}
		});
	}</script>
<body class="w3-light-grey" onload="show_guardians();  load_change(); load_change_parent(); ">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<a href="index.php" class="btn btn-primary btn-block">Back to Home</a>
		<hr>
		<div id="guardians"></div>
		</div>

		<div class="col-sm-3"></div>
	</div>
</div>
<?php include_once("upload_guardian.php"); 	include_once("upload_parent.php");?>


<div class="modal fade" role="dialog" id="info_edit_pg">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Edit info <button class="close" data-dismiss="modal" onclick="clear_pg();">&times;</button></div>
			<div class="modal-body">
				<input type="hidden" name="stat" id="stat">
				<input type="hidden" name="pgid" id="pgid">
				<div class="form-group">
					<input type="text" name="pgln" id="pgln" style="text-transform: uppercase;" class="form-control" placeholder="Lastname..." />
				</div>
				<div class="form-group">
					<input type="text" name="pgfn" id="pgfn" style="text-transform: uppercase;" class="form-control" placeholder="Firstname..." />
				</div>
				<div class="form-group">
					<input type="text" name="pgmn" id="pgmn" style="text-transform: uppercase;" class="form-control" placeholder="Middlename..." />
				</div>

				<div class="form-group">
					<select class="form-control" id="pggender" name="pggender">
						<option selected="" disabled="">Choose your gender</option>
						<option>MALE</option>
						<option>FEMALE</option>
					</select>
				</div>

				<div class="form-group">
					<textarea name="pgaddress" style="text-transform: uppercase;" id="pgaddress" class="form-control" placeholder="Address..."></textarea>
				</div>

				<div class="form-group">
					<input type="text" name="pgoccupation" style="text-transform: uppercase;" id="pgoccupation" class="form-control" placeholder="Occupation..." />
				</div>

				<div class="form-group">
					<input type="text" name="pgcontact" maxlength="10" onkeypress="return num_only(event);" id="pgcontact" class="form-control" placeholder="Contact number..." />
				</div>

				<div class="form-group">
					<input type="text" name="pgemail" id="pgemail" class="form-control" placeholder="E-mail..." />
				</div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-success btn-block" id="btn_up_pg" onclick="update_pg_set();">Save Changes</button>
			</div>
		</div>
	</div></div>
<script type="text/javascript">
function load_change_parent(){
		$(document).ready(function(){
		$("#images").on('change',function(){
			var mypicfile = document.getElementById('images');
			var info = mypicfile.value;
			if (mypicfile.value == "") 
			{
				$("#btn_sub_parent").hide('fast');
				$("#change_btn_parent").show('fast');
			}
			else
			{
				$("#btn_sub_parent").show('fast');
				$("#change_btn_parent").hide('fast');
				//alert(info);
			}
		});
	});
}
</script>
<script type="text/javascript">
function load_change(){
		$(document).ready(function(){
		$("#image").on('change',function(){
			var mypicfile = document.getElementById('image');
			var info = mypicfile.value;
			if (mypicfile.value == "") 
			{
				$("#btn_sub").hide('fast');
				$("#change_btn").show('fast');
			}
			else
			{
				$("#btn_sub").show('fast');
				$("#change_btn").hide('fast');
				//alert(info);
			}
		});
	});
}
</script>
<script type="text/javascript">
		function get_info_pg(who,id,fn,mn,ln,addr,cont,email,gender,occup){
		$("#stat").val(who);
		$("#pgid").val(id);
		$("#pgfn").val(fn);
		$("#pgmn").val(mn);
		$("#pgln").val(ln);
		$("#pggender").val(gender);
		$("#pgaddress").val(addr);
		$("#pgoccupation").val(occup);
		$("#pgcontact").val(cont);
		$("#pgemail").val(email);
		$("#info_edit_pg").modal('show');
	}
</script>
</body>
	<script src="../js/jquery.min.js"></script>
	<script src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/sweetalert.min.js"></script>
	<script src="../js/function.js"></script>
		<script src="../js/client.js"></script>
</html>