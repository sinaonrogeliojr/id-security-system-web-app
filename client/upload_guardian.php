<script type="text/javascript">
	function get_guardian_pic(img,name,id){
		document.getElementById("my_current_pics").src = img;
		$("#my_name").html(name);
		$("#gid").val(id);
		$("#edit_pic_guardian").modal('show');
	}
</script>

<div class="modal fade" role="dialog" id="edit_pic_guardian">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">Edit Guardian Picture <button class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
				<center><img src="" id="my_current_pics" class="img-thumbnail animated jackInTheBox" width="220"/></center>
				<hr>
				<center><div id="my_name"></div></center>
				<form  method="post"  enctype="multipart/form-data">
					<input type="hidden" name="gid" id="gid">
					<input type="file" name="image" id="image"  style="display: none;'">
						<div style="margin:20px 20px 5px 20px;">
					<input type="submit" id="btn_sub" name="btn_sub" style="display: none;" value="Update Picture" type="submit" class="btn btn-success btn-block">
				</div>
				</form>
				<div style="margin:0px 20px 20px 20px;">
					<button id="change_btn" class="btn btn-block btn-primary" onclick="$('#image').click();">Change Picture</button>
					<button onclick="close_change_pic();" data-dismiss="modal" class="btn btn-block btn-danger">Close</button>
				</div>
			</div>
			<div class="modal-footer">
				
			</div>
		</div>
	</div>
</div>

<?php 
	if (isset($_POST['btn_sub'])) {
		//session_start();
		include_once("../config.php");
		$guardian_id = $_POST['gid'];
		$imageName=mysqli_real_escape_string($con,$_FILES["image"]["name"]);
		$imageData=mysqli_real_escape_string($con,file_get_contents($_FILES["image"]["tmp_name"]));
		$imageType=mysqli_real_escape_string($con,$_FILES["image"]["type"]);
		$date=Date("l F d, Y h:i:sa");
		?>
		<?php
		if(substr($imageType,0,5) == "image")
		{	

			$update = mysqli_query($con,"UPDATE tbl_guardian_picture set image='$imageData' where guardians_id='$guardian_id'");	
			if ($update) {
			?>
			<script type="text/javascript">
				alert("Profile Picture has been updated !");
				window.location="guardian.php";
			</script>
			<?php
		}
		}	
		else{
			?>
			<script type="text/javascript">
				alert("Sorry Invalid image format or file is too large ! ");
			</script>
			<?php
		}
	}
 ?>



<script type="text/javascript">
	function close_change_pic(){
		$("#btn_sub").hide('fast');
		$("#change_btn").show('fast');
		$("image").val('');
	}
</script>
