<?php
session_start();
if (!isset($_SESSION['id'])) {
	@header('location:../');	
}
 include_once('../config.php');

$section = $_SESSION['section'];
$sql = mysqli_query($con, "SELECT t2.`grade`, t1.`section` FROM tbl_section t1
LEFT JOIN tbl_grade t2 ON t1.`grade_id` = t2.`ID` where t1.`section` = '$section'");

$row = mysqli_fetch_assoc($sql);

?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>

<script type="text/javascript">

//function show_sched_src(src){
	// 	section = $('#section').val();
	// 	if (src == "") 
	// 	{
	// 		show_schedules();
	// 	}
	// 	else
	// 	{
	// 	var search = src;
	// 	var mydata = 'search=' + search + '&section=' + section;
	// 	$.ajax({
	// 		type:"POST",
	// 		url:"tbl_schedules.php",
	// 		data:mydata,
	// 		cache:false,
	// 		beforeSend:function(){
	// 			$("#schedules").html('<center><img src="../img/flat.gif" width="50"></center>');
	// 		},
	// 		success:function(data){
	// 			$("#schedules").html(data);
	// 		}

	// 	});
	// 	}
	// }

	function show_schedules(){
		var start,limit,section;
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		section = $('#section').val();
		var day_ = $('#day_').val();
		var mydata = 'start=' + start.value + '&limit=' + limit.value + '&section=' + section + '&day_=' + day_ ;
		$.ajax({
			type:"POST",
			url:"tbl_schedules.php",
			data:mydata,
			cache:false,
			success:function(data){
					$("#schedules").html(data);
					$("#btn_gl").text('load more');
				
			}

		});
	}

	function display_schedules(){
		var start,limit,section;
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		section = $('#section').val();
		var day_ = $('#day_').val();
		var mydata = 'start=' + start.value + '&limit=' + limit.value + '&section=' + section + '&day_=' + day_ ;
		$.ajax({
			type:"POST",
			url:"tbl_schedules.php",
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#schedules").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#schedules").html(data);
			}

		});
	}

	function goBack() {
	  window.history.back();
	}

	function todays_day(){

		var today_d = $('#day_today').val();

		//$('#day_').val(today_d);

	}

</script>

<input type="hidden" name="start" id="start" value="0">
<input type="hidden" name="limit" id="limit" value="10">
<input type="hidden" id="section" value="<?php echo $_SESSION['section']; ?>">

<body class="w3-light-grey"  onload="display_schedules(); todays_day();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<div class="">
				<div class="btn-group btn-group-justified btn-block">
					<a href="index.php" class="btn btn-primary btn-block"><span class="fa fa-home"></span> Home</a>
					<a onclick="goBack();" class="btn btn-primary btn-block"><span class="fa fa-arrow-left"></span> Back</a>
				</div>
				<input type="hidden" id="day_today" value="<?php echo date('l'); ?>">
				<div class="w3-panel w3-red">SCHEDULE FOR <b><em><?php echo $row['grade'] . " - " . $row['section']; ?></em></b>
				</div> 
				<div class="form-group">
				  <label for="day_">Select Day: </label>
				  <select class="form-control" id="day_"  oninput="display_schedules();" onchange="display_schedules();">
				  	<option value="ALL">All</option>
				    <option value="Monday">Monday</option>
				    <option value="Tuesday">Tuesday</option>
				    <option value="Wednesday">Wednesday</option>
				    <option value="Thursday">Thursday</option>
				    <option value="Friday">Friday</option>
				    <option value="Saturday">Saturday</option>
				  </select>
				</div>
				<div id="schedules"></div>
			</div>
			
		</div>

		</div>

		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>

<?php include_once('modals.php'); ?>

</html>

<script type="text/javascript">
	function view_modal_sched(){
		$('#m-header').html('Upload Schedule');
		$('#upload_schedule').modal('show');
	}
</script>
