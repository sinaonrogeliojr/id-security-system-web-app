<?php
session_start();
if (!isset($_SESSION['id'])) {
	@header('location:../');	
}
 include_once('../config.php');?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="../css/showToast.css">
</head>

<?php 
function drop_year(){
$year = 2015; 
while($year <= 2099) {
   ?>
   <option><?php echo $year ?></option>
   <?php
    $year++;
}
}
 ?>
<style type="text/css">
	/* The switch - the box around the slider */
	.switch {
	  position: relative;
	  display: inline-block;
	  width: 60px;
	  height: 34px;
	}

	/* Hide default HTML checkbox */
	.switch input {display:none;}

	/* The slider */
	.slider {
	  position: absolute;
	  cursor: pointer;
	  top: 0;
	  left: 0;
	  right: 0;
	  bottom: 0;
	  background-color: #ccc;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	.slider:before {
	  position: absolute;
	  content: "";
	  height: 26px;
	  width: 26px;
	  left: 4px;
	  bottom: 4px;
	  background-color: white;
	  -webkit-transition: .4s;
	  transition: .4s;
	}

	input:checked + .slider {
	  background-color: #2196F3;
	}

	input:focus + .slider {
	  box-shadow: 0 0 1px #2196F3;
	}

	input:checked + .slider:before {
	  -webkit-transform: translateX(26px);
	  -ms-transform: translateX(26px);
	  transform: translateX(26px);
	}

	/* Rounded sliders */
	.slider.round {
	  border-radius: 34px;
	}

	.slider.round:before {
	  border-radius: 50%;
	}
</style>
<body onload="able_toggle('is_heads'); $('#year').val('<?php echo date('Y') ?>'); able_toggle('is_shows'); show_events();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">

		<div class="">
			<a href="index.php" class="btn btn-primary btn-block">Home</a>
			<h4><span class="fa fa-calendar-week"> </span> Calendar Events</h4>
		
			<input type="hidden" name="aid" id="aid">
			<div class="row">
				<div class="col-sm-12">
					<!-- <hr> -->
					<table class="table">
						<tr>
							<td style="width:50%;">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<select class="form-control" id="year" oninput="show_events();" name="year">
											<?php echo drop_year(); ?>
										</select>
									</div>
								</div>
							</td>
							<td style="width:50%;">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-search"></i></span>
										<input type="text" name="src" id="src" oninput="show_events();" class="form-control" placeholder="Search">
									</div>
								</div>
							</td>
						</tr>
					</table>
					<div id="events"></div>
					<br>
					<br>
					<br>
				</div>
			</div>
		</div>


		<script type="text/javascript">

			function show_events(){
				var years = $('#year').val();
				var src = $('#src').val();

				var mydata = 'year=' + years + '&src=' + src;
				$.ajax({
					type:"POST",
					url:"show_event.php",
					data:mydata,
					cache:false,
					success:function(data){
						$("#events").html(data);
					}
				});
			}
			
			function hide_show(id,status){
				var mydata = 'id=' + id + '&status=' + status;
				$.ajax({
					type:"POST",
					url:"hide_unhide.php",
					data:mydata,
					cache:false,
					success:function(data){
						// alert(data);
						if (data == 1) 
						{
						showToast.show('Record has been showed!',2000);

						show_events();
						}
						else if (data == 2) 
						{
						show_events();
						showToast.show('Record has been hide!',2000);

						}
						else
						{
						alert(data);
						}
					}
				});
			}

			function save_event_range(){
				if ($("#from_range").val() == "" || $("#from_range").val() == null) {
					$("#from_range").focus();

				}
				else if ($("#to_range").val() == "" || $("#to_range").val() == null) {
					$("#to_range").focus();
				}
				else
				{
					var mydata = 'eid=' + $("#eid").val() +'&from_range=' + $("#from_range").val() +'&to_range=' + $("#to_range").val();
					$.ajax({
						type:"POST",
						url:"add_range.php",
						data:mydata,
						cache:false,
						success:function(data){
							// alert(data);
							if (data == 1) {
							showToast.show('Date Range has been saved!',2000);
							show_events();

							}
							else if (data == 2) {
							showToast.show('Date Range has been saved!',2000);
							show_events();

							}
							else 
							{
								alert(data);
							}
						}
					});
				}
			
			}

			function add_range(id,date_from,to){
				$("#eid").val(id);
				$("#from_range").val(date_from);
				$("#to_range").val(to);
				$("#date_range").modal('show');
			} 

			function cancel_events(){
				$("#adate").val('');
				$("#msg").val('');
				$("#aid").val('');
			}


			function edit_announce(date,is_header,is_visibility,msg,id){
			// alert(date+' '+is_header+' '+is_visibility+' '+msg);
			$("#adate").val(date);
			$("#is_header").val(is_header);
			$("#is_visibility").val(is_visibility);
			$("#msg").val(msg);
			$("#aid").val(id);
			// $("#is_head").click();
			// $("#is_show").click();
			
			
			}

			function able_toggle(index){
				// alert(index);
				switch(index) {
				  case 'is_heads':
				   if ($("#is_header").val() == 0) 
				   {
				   $("#is_header").val(1);
				   $("#switch_status").text('Header');	
				   $("#is_head").attr('checked',true);
				   }
				   else
				   {
				   $("#is_header").val(0);
				   $("#switch_status").text('Subitem');
					$("#is_head").attr('checked',false);
				   }
				  break;
				  
				  case 'is_shows':
				   if ($("#is_visibility").val() == 0) 
				   {
				   $("#is_visibility").val(1);
				   $("#visible").text('Visible');
				   $("#is_show").attr('checked',true);

				   }
				   else
				   {

				   $("#is_visibility").val(0);
				   $("#visible").text('Invisible');
				   $("#is_show").attr('checked',false);
				   }
				  break;
				  
				  default:
				    alert("Error in case " + index);
				}
			}

			

			function save_events(){
				var msg = $("#msg");
				var adate = $("#adate");
				var aid = $("#aid").val();
				var is_visibility = $("#is_visibility").val();
				var is_header = $("#is_header").val();

				if (adate.val() == "") 
				{
					adate.focus();
				}
				else if (msg.val() == "") 
				{
					msg.focus();
				}
				else
				{
					var mydata = 'msg=' + msg.val() + '&id=' + aid + '&visible=' + is_visibility + '&header=' + is_header + '&adate=' + adate.val();
					// alert(mydata);
					$.ajax({
						type:"POST",
						url:"save_event.php",
						data:mydata,
						cache:false,
						success:function(data){
						// alert(data);
						if (data == 1) 
						{
						showToast.show('Announcement has been saved!',2000);
						show_events();
						msg.val('');
						// adate.val('');
						$("#aid").val('');
						}
						else if (data == 2) 
						{
						showToast.show('Announcement has been saved!',2000);
						show_events();
						// adate.val('');
						msg.val('');
						$("#aid").val('');

						}
						else
						{
							alert(data);
						}
						}
					});
				}
			}


			function delete_course(id){

		    swal({
		      title: "Are you sure?",
		      text: "Do you want to delete this record ?",
		      type: "warning",
		      showCancelButton: true,
		      confirmButtonColor: "#DD6B55",
		      confirmButtonText: "Yes",
		      closeOnConfirm: false
		    },
		    function(){
		    	var mydata = 'id=' + id;
		   	$.ajax({
		   		type:"POST",
		   		url:"delete_event.php",
		   		data:mydata,
		   		cache:false,
		   		success:function(data){
		   			if (data == 1) 
		   			{
					showToast.show('Announcement has been deleted!',2000);

					show_events();
		   			}
		   			else
		   			{
		   				alert(data);
		   			}
		   		}
		   	});
		    });
		  }
		</script>
		<!-- <div class="well">No Records found...</div> -->
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>
<script src="../js/showToast.js"></script>

</html>
<?php include_once('modals.php'); ?>