<?php 
session_start();
include_once("../config.php");
$start = $_POST['start'];
$limit = $_POST['limit'];


$get_pr = mysqli_query($con,"SELECT * from tbl_staff where position='PRINCIPAL'");
$prid = mysqli_fetch_assoc($get_pr);
$pr = $prid['staff_id'];

$sql = mysqli_query($con,"SELECT * from tbl_announcement where userid='$pr' order by id desc limit ".$_POST['start'].", ".$_POST['limit']." ");

$sqls = mysqli_query($con,"SELECT count(id) from tbl_announcement where userid='$pr'");
$get_max = mysqli_fetch_assoc($sqls);

if (mysqli_num_rows($sql)>0) {
	while ($row = mysqli_fetch_assoc($sql)) {
		$date = date_create($row['date_post']);
		$mdate = date_format($date,'M d Y, h:i a');
		if ($row['type'] == null) {
			?>
			<div class="panel panel-info w3-card-2" id="<?php echo $row['id'] ?>">
				<div class="panel-heading"> <span class="w3-round-large w3-blue" style="padding:4px;">Others</span> Concern: <?php echo $row['audience'] ?>

				</div>
				<div class="panel-body">
						<p><b>From office of the principal</b></p>
					<hr>
					<p class="w3-text-dark-grey" style="word-break:break-all;"><?php echo $row['msg']; ?></p>
					<p></p>
				</div>
				<div class="panel-footer">
					<div class="text-right w3-text-dark-grey"><small><?php echo $mdate ?></small></div>
				</div>
			</div>
			<?php
		}
		else
		{
			?>
			<div class="panel panel-info w3-card-2" id="<?php echo $row['id'] ?>">
				<div class="panel-heading"> <span class="w3-round-large w3-blue" style="padding:4px;"><?php echo $row['type']; ?></span> Concern: <?php echo $row['audience'] ?>
				</div>
				<div class="panel-body">
						<p><b>From office of the principal</b></p>
					<hr>
					<p class="w3-text-dark-grey" style="word-break:break-all;"><?php echo $row['msg']; ?></p>
					<p></p>
				</div>
				<div class="panel-footer">
					<div class="text-right w3-text-dark-grey"><small><?php echo $mdate ?></small></div>
				</div>
			</div>
			<?php
		}
	}

	if ($limit >= $get_max['count(id)']) {
	?>
	<button class="btn btn-block w3-light-grey  btn-lg">End of records..</button>
	<br>
	<?php
	}
	else
	{
	?>
	<button class="btn btn-block btn-info w3-card-2 btn-lg" onclick="load_more();">Load more</button>
	<br>
	<?php
	}
}
else
{
	?>
	<div class="well">No data found...</div>
	<?php
}
?>


<script type="text/javascript">
	function load_more(){
	var start = document.getElementById('start');
	var limit = document.getElementById('limit');
	var b;
	b = Number(limit.value) + 10; 
	$("#limit").val(b);
	setTimeout(function(){
			display_announce();
	},1000);
	}
</script>
