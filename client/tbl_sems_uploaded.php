<?php
session_start();
include_once("../config.php");

// $stmt = mysqli_query($con,"SELECT t1.*,CONCAT(t2.`lastname`, " ", t2.`firstname`, " ", t2.`mi`) AS student_name,t3.`image` FROM tbl_student_grades t1
// LEFT JOIN tbl_students t2 ON t1.`student_id` = t2.`student_id` 
// LEFT JOIN tbl_student_picture t3 ON t1.`student_id` = t3.`student_id` WHERE t1.`teacher_id` = '$teacher_id' and subject_code = '$subject_code' and sem = '$sem_id'
// ORDER BY t2.`lastname` ASC");



$stmt = mysqli_query($con,"SELECT sem,school_year FROM tbl_sem WHERE is_active = 1");

if (mysqli_num_rows($stmt)>0) {
	while ($row = mysqli_fetch_assoc($stmt)) { ?>
		
		<div class="well w3-card-2 w3-hover-teal">
							
			<form method="get" action="grades.php">  
				<p class="w3-small"><span class="w3-xlarge"><?php echo $row['sem'] ?> <?php echo $row['school_year']; ?></span></p>	
				<input type="text" style="display: none" id="sem_id" name="sem_id" value="<?php echo $row['sem'] ?> <?php echo $row['school_year']; ?>" autocomplete="off"/>
	            <button  class="close" id="btn_view_grades" style="margin-top: -35px;" type="submit"><i class="fa fa-chevron-right"></i></button>
			</form>
		
		</div>

		<hr/>
<?php		
	}
}
else
{
?>
	<div class="well">No data found...</div>
<?php
}

?>