<?php 
include("../config.php");

$year = mysqli_real_escape_string($con,$_POST['year']);
$src = mysqli_real_escape_string($con,$_POST['src']);

function convert_date($date){
	$str = strtotime($date);
	return date('Y-m-d',$str);
}

function year_month($date){
	$str = strtotime($date);
	return date('Y-m',$str);
}



function is_visible($index,$id){
	if ($index == 1) {
		return '<button title="hide" class="btn btn-secondary btn-sm" onclick="hide_show(\''.$id.'\',\''.$index.'\');"><i class="fa fa-eye-slash"></i></button>';
	}
	else if ($index == 0) {
		return '<button title="show" class="btn btn-primary btn-sm" onclick="hide_show(\''.$id.'\',\''.$index.'\');"><i class="fa fa-eye"></i></button>';

	}
}

		
function is_display($index){
	if ($index == 1) {
		return 0;
	}
	else if ($index == 0) {
		return 1;
	}
}


function i_header($index){
	if ($index == 'SUBITEM') {
		return 0;
	}
	else if ($index == 'HEADER') {
		return 1;
	}
}

$concat ='';


if ($src!="") {
	$concat ="and concat(aparticular,groupheader) like '%$src%'";
}
else
{
	$concat ='';
}



$sql = mysqli_query($con,"SELECT *,DATE_FORMAT(acategory,'%Y%m') as aa from tbl_calendar where groupheader='HEADER' and DATE_FORMAT(acategory,'%Y')='$year' ".$concat."  group by DATE_FORMAT(acategory,'%Y%m') asc order by DATE_FORMAT(acategory,'%Y%m') asc");

if (mysqli_num_rows($sql)>0) {
	while ($row = mysqli_fetch_assoc($sql)) {
		$dates = year_month($row['acategory']);
	?>
	<div class="h5 text-primary w3-hover-opacity" onclick="edit_announce('<?php echo $row['acategory'] ?>','<?php echo i_header($row['groupheader']) ?>','<?php echo $row['avisible'] ?>','<?php echo $row['aparticular'] ?>','<?php echo $row['atransid'] ?>');" style="cursor: pointer;"><b><?php echo $row['aparticular'] ?></b></div>
	<?php
	$query = mysqli_query($con,"SELECT * from tbl_calendar where DATE_FORMAT(acategory,'%Y-%m')='$dates' and DATE_FORMAT(acategory,'%Y')='$year' and groupheader='SUBITEM' ".$concat."");
	?>
	<table class="table table-bordered table-hover table-condence" style="cursor: pointer;">
	<?php
	while ($rows = mysqli_fetch_assoc($query)) {
		?>
		<tr>
			<td ondblclick="add_range('<?php echo $rows['atransid'] ?>','<?php echo $rows['acategory'] ?>','');" class="w3-small"  style="width:25%;"><?php echo $rows['adate'] ?> </td>
			<td ondblclick="add_range('<?php echo $rows['atransid'] ?>','<?php echo $rows['acategory'] ?>','');"  class="w3-small" style="width:65%;"><?php echo $rows['aparticular'] ?>
				
			</td>
		</tr>
		<?php
	}
	?>
	</table>
	<?php
	}
	}
	else
	{
	$querys = mysqli_query($con,"SELECT * from tbl_calendar where DATE_FORMAT(acategory,'%Y')='$year' and groupheader='SUBITEM' ".$concat."");
	if (mysqli_num_rows($querys)) {
		?>
	<table class="table table-bordered table-hover table-condence" style="cursor: pointer;">
	<?php

	
	while ($rowss = mysqli_fetch_assoc($querys)) {
		?>
		<tr>
			<td ondblclick="add_range('<?php echo $rowss['atransid'] ?>','<?php echo $rowss['acategory'] ?>','');" class="w3-small"  style="width:25%;"><?php echo $rowss['adate'] ?> </td>
			<td ondblclick="add_range('<?php echo $rowss['atransid'] ?>','<?php echo $rowss['acategory'] ?>','');"  class="w3-small" style="width:65%;"><?php echo $rowss['aparticular'] ?></td>
			
		</tr>
		<?php
	}
	?>
	</table>
	<?php
	}
	else
	{
		?>
		<div class="alert alert-info">No record found!</div>
		<?php
	}
	}

?>