<?php
session_start();
if (!isset($_SESSION['id'])) {
	@header('location:../');	
}
 include_once('../config.php')
 ;?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['id'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<script type="text/javascript">

	function load_grades(){
		var sem_id = $('#sem_id').val();
		var mydata = 'sem_id=' + sem_id
		$.ajax({
			type:"POST",
			url:"tbl_grades.php",
			data:mydata	,
			cache:false,
			beforeSend:function(){
			$("#tbl_grades").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#tbl_grades").html(data);
			}
		});
	}
	
</script>

<body onload="load_grades();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">

	<div class="row">

		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>

			<input type="hidden" id="sem_id" name="sem_id" value="<?php echo $_GET['sem_id']; ?>">
			<a href="sems_uploaded.php" class="btn btn-primary btn-block">Back</a>
			<h5 class=" w3-padding-top"><b><?php echo $_SESSION['grade'].' - '.$_SESSION['section'] ?></b></h5>
			<br>
				<h5 class=""><i class="fa fa-list-alt w3-small"></i> LIST OF GRADES</h5>
			<hr>

			<!-- <div class="form-group">
				<div class="form-group">
				  <label for="sel1">Select list:</label>
				  <select class="form-control" id="sel1">
				    <option>1</option>
				    <option>2</option>
				    <option>3</option>
				    <option>4</option>
				  </select>
				</div>

			</div> --> 

			<div id="tbl_grades"></div>

		</div>
		</div>

		<div class="col-sm-3"></div>

	</div>
</div>
</body>

<script src="../js/jquery.mins.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>
</html>