<?php 
session_start();
include_once("../config.php");
$mgrade = $_SESSION['grade'];
$msection = $_SESSION['section'];


$get_teach = mysqli_query($con,"SELECT * from tbl_teachers where advisory='$msection' and grade='$mgrade'");

$get_id = mysqli_fetch_assoc($get_teach);
$myid = $get_id['teacher_id'];

echo $myteacher = '<h4>'.$get_id['lastname'].', '.$get_id['firstname'].' '.$get_id['mi'].'</h4>';
	echo '<b>'.$mgrade.' '.$msection.'</b><hr>';
$start = $_POST['start'];
$limit = $_POST['limit'];

$sql = mysqli_query($con,"SELECT * from tbl_announcement where userid='$myid' order by id desc limit ".$_POST['start'].", ".$_POST['limit']." ");

$sqls = mysqli_query($con,"SELECT count(id) from tbl_announcement where userid='$myid'");
$get_max = mysqli_fetch_assoc($sqls);

if (mysqli_num_rows($sql)>0) {
	while ($row = mysqli_fetch_assoc($sql)) {
		$date = date_create($row['date_post']);
		$mdate = date_format($date,'M d Y, h:i a');
		if ($row['type'] == null) {
			?>
			<div class="panel panel-info w3-card-2" id="<?php echo $row['id'] ?>">
				<div class="panel-heading">Others

				</div>
				<div class="panel-body">
					<p style="word-break:break-all;"><?php echo $row['msg']; ?></p>
					<a style="text-decoration:  underline; color: blue;" title="File Attachment... Click to View" href="../teacher/<?php echo $row['file_attached']; ?>" target="_blank"><?php echo substr($row['file_attached'], 18); ?></a>
				</div>
				<div class="panel-footer">
					<div class="text-right w3-text-dark-grey"><small>Posted on <?php echo $mdate ?></small></div>
				</div>
			</div>
			<?php
		}
		else
		{
			?>
			<div class="panel panel-info w3-card-2" id="<?php echo $row['id'] ?>">
				<div class="panel-heading">
					<?php echo $row['type']; ?> <button class="btn btn-sm btn-primary pull-right" onclick="reply_info('<?php echo $row['post_id']; ?>')"><span class="fa fa-message"></span> Reply</button>
				</div>
				<div class="panel-body">
					<p style="word-break:break-all;"><?php echo $row['msg']; ?></p>
					<a style="text-decoration:  underline; color: blue;" title="File Attachment... Click to View" href="../teacher/<?php echo $row['file_attached']; ?>" target="_blank"><?php echo substr($row['file_attached'], 18); ?></a>
				</div>
				<div class="panel-footer">
					<div class="text-right w3-text-dark-grey"><small>Posted on <?php echo $mdate ?></small></div>
				</div>
			</div>
			<?php
		}
	}

	if ($limit >= $get_max['count(id)']) {
	?>
	<button class="btn btn-block w3-light-grey  btn-lg">End of records..</button>
	<br>
	<?php
	}
	else
	{
	?>
	<button class="btn btn-block btn-info w3-card-2 btn-lg" onclick="load_more();">Load more</button>
	<br>
	<?php
	}
}
else
{
	?>
	<div class="well">No data found...</div>
	<?php
}
?>

<script type="text/javascript">
	function load_more(){
	var start = document.getElementById('start');
	var limit = document.getElementById('limit');
	var b;
	b = Number(limit.value) + 10; 
	$("#limit").val(b);
	setTimeout(function(){
			display_announce();
	},1000);
	}

	function reply_info(post_id) {
		$('#post_id').val(post_id);
		$('#reply_announcement').modal('show');
	}

</script>
