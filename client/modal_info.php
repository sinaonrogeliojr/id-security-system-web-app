<div class="modal fade" role="dialog" id="info_show_mypic">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Profile Picture</div>
			<div class="modal-body text-center">
				<?php 
				echo '<img src="data:image/jpeg;base64,'.base64_encode($_SESSION['mypic']).'" class="img-thumbnail animated jackInTheBox" width="220"/>';
			 ?>
			 <hr>
			 <h5><?php echo $_SESSION['fullname'] ?></h5>
			 <h5><?php echo $_SESSION['grade'].' - '.$_SESSION['section'] ?></h5>
			</div>
			
			<form  method="post"  enctype="multipart/form-data">
				<input type="file" name="image" id="image"  style="display: none;'">
				<div style="margin:20px 20px 5px 20px;">
					<input type="submit" id="btn_sub_info" name="btn_sub_info" style="display: none;" value="Update Picture" type="submit" class="btn btn-success btn-block">
				</div>
			</form>
			<div style="margin:0px 20px 20px 20px;">
				<button id="change_btn_info" class="btn btn-block btn-primary" onclick="$('#image').click();">Change Picture</button>
				<button onclick="close_change_pic_info();" data-dismiss="modal" class="btn btn-block btn-danger">Close</button>
			</div>
		</div>
	</div>
</div>

<!--separator -->
<?php 
	if (isset($_POST['btn_sub_info'])) {
		include_once("../config.php");
		$myid = $_SESSION['id'];
		$imageName=mysqli_real_escape_string($con,$_FILES["image"]["name"]);
		$imageData=mysqli_real_escape_string($con,file_get_contents($_FILES["image"]["tmp_name"]));
		$imageType=mysqli_real_escape_string($con,$_FILES["image"]["type"]);
		$date=Date("l F d, Y h:i:sa");
		?>
		<?php
		if(substr($imageType,0,5) == "image")
		{	

			$update = mysqli_query($con,"UPDATE tbl_student_picture set image='$imageData' where student_id='$myid'");	
			if ($update) {

			$stmt = mysqli_query($con,"SELECT a.*,b.* from tbl_students a left join tbl_student_picture b on a.student_id = b.student_id where b.student_id='$myid'");
			$injec = mysqli_fetch_assoc($stmt);
			$_SESSION['mypic'] = $injec['image'];
			?>
			<script type="text/javascript">
				alert("Profile Picture has been updated !");
				window.location="student_info.php";
			</script>
			<?php
		}
		}	
		else{
			?>
			<script type="text/javascript">
				alert("Sorry Invalid image format or file is too large ! ");
			</script>
			<?php
		}
	}
 ?>

 <script type="text/javascript">
	function close_change_pic_info(){
		$("#btn_sub_info").hide('fast');
		$("#change_btn_info").show('fast');
		$("image").val('');
	}
</script>



<script type="text/javascript">
	$(document).ready(function(){
		$("#image").on('change',function(){
			var mypicfile = document.getElementById('image');
			var info = mypicfile.value;
			if (mypicfile.value == "") 
			{
				$("#btn_sub_info").hide('fast');
				$("#change_btn_info").show('fast');
			}
			else
			{
				$("#btn_sub_info").show('fast');
				$("#change_btn_info").hide('fast');
				//alert(info);
			}
		});
	});
</script>