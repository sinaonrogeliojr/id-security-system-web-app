<?php
session_start();
if (!isset($_SESSION['id'])) {
	@header('location:../');	
}
 include_once('../config.php');?>

<!DOCTYPE html>
<html lang="en-US">
<head>

	<title><?php echo $_SESSION['fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>

<body onload="myinfo();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<div class="text-center">
			<?php 
				
				echo '<img data-toggle="modal" data-target="#show_mypic_info" src="data:image/jpeg;base64,'.base64_encode($_SESSION['mypic']).'" class="img-thumbnail" width="150"/>';
						 
			?>
			
			 <div>
			 </div>
			
			 </div>
			<br>

			<div id="my_info"></div>
			
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>



<script type="text/javascript">
	function edit_modal(id,ln,fn,mn,age,gender,address,contact,grade,section){
		//alert(id+' '+ln+' '+fn+' '+mn+' '+age+' '+gender+' '+address+' '+contact+' '+grade+' '+section);
		$("#myid").val(id);
		$("#lname").val(ln);
		$("#fname").val(fn);
		$("#mname").val(mn);
		$("#myage").val(age);
		$("#gender").val(gender);
		$("#mycontact").val(contact);
		$("#address").val(address);
		$("#grade_my").val(grade);
		$("#section_my").val(section);
		$("#edit_info").modal({'backdrop':'static'});
	}	
</script>
<div class="modal fade" role="dialog" id="edit_info">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Edit <button class="close" data-dismiss="modal">&times;</button></div>
			<div class="modal-body">
			
			<div class="form-group">
				<label>ID number</label>
				<input type="text" name="myid" id="myid" disabled="" class="form-control"/>
			</div>

			<div class="form-group">
				<label>Lastname</label>
				<input type="text" style="text-transform: uppercase;"  name="lname" id="lname" class="form-control" placeholder="Enter lastname.." />
			</div>

			<div class="form-group">
				<label>Firstname</label>
				<input type="text" style="text-transform: uppercase;"  name="fname" id="fname" class="form-control" placeholder="Enter firstname.." />
			</div>

			<div class="form-group">
				<label>Middlename</label>
				<input type="text" style="text-transform: uppercase;"  name="mname" id="mname" class="form-control" placeholder="Enter middlename.." />
			</div>

			<div class="form-group">
				<label>Age</label>
				<input type="number" name="myage" id="myage" class="form-control" placeholder="Enter age.." />
			</div>

			<div class="form-group">
				<label>Gender</label>
				<select name="gender" style="text-transform: uppercase;"  id="gender" class="form-control">
					<option selected="" disabled="">Choose gender</option>
					<option>MALE</option>
					<option>FEMALE</option>
				</select>
			</div>

			<div class="form-group">
				<label>Address</label>
				<textarea style="text-transform: uppercase;"  name="address" id="address" class="form-control" placeholder="Enter address.."></textarea>
			</div>

			<div class="form-group">
				<label>Contact number</label>
				<input type="text" onkeypress="return num_only(event);" name="mycontact" maxlength="10" id="mycontact" class="form-control" placeholder="Enter contact number" />
			</div>

			<div class="form-group">
				<label>Grade</label>
				<input  name="grade_my" id="grade_my"  style="text-transform: uppercase;" class="form-control" placeholder="Enter grade.." list="grades" >
				<?php include_once("list_grade.php"); ?>
			</div>
			

			<div class="form-group">
				<label>Section</label>
				<input  name="section_my" style="text-transform: uppercase;"  id="section_my" class="form-control" placeholder="Enter sections.." list="section" >
				<?php include_once("list_section.php"); ?>
			</div>
			


			</div>
			<div class="modal-footer">
				<button class="btn btn-success" onclick="save_update();" id="btn_up">Save </button>
			</div>
		</div>
	</div>
</div>

</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>
<script src="../js/function.js"></script>
<script src="../js/client.js"></script>
</html>
<?php include_once("modal_info.php"); ?>
