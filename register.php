<?php
 session_start();
 if (isset($_SESSION['id'])) {
 	@header('location:client/');
 }
 include_once('config.php');
 ?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title>Ssja</title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/w3.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<link rel="stylesheet" type="text/css" href="font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
</head>
<body>
<?php 
include_once("nav.php");
 ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<div class="text-center">

				<img src="img/sec.png" class="animated jackInTheBox" width="160">
				<h3>Register Security Account</h3>
			</div>
			<hr>
			<div class=" form-group">
				<label>Name</label>
				<input type="text" name="sec_name" id="sec_name" placeholder="Enter your name..." class="form-control text-capitalize">
			</div>

			<div class=" form-group">
				<label>Username</label>
				<input type="text" name="sec_uname" id="sec_uname" placeholder="Enter username..." class="form-control">
			</div>
			
			<div class=" form-group">
				<label>Password</label>
				<input type="password" name="sec_pwd" id="sec_pwd" placeholder="Enter password..." class="form-control">
			</div>
			<hr>
			<div class="text-right">
				<button id="btn_reg_sec" class="btn btn-primary btn-block" onclick="reg_sec();">Register</button>
				<a href="index.php" class="btn btn-danger btn-block" >Cancel</a>
			</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>

<script type="text/javascript">
	function reg_sec(){
		var sec_name,sec_uname,sec_pwd;
		sec_name = document.getElementById('sec_name');
		sec_uname = document.getElementById('sec_uname');
		sec_pwd = document.getElementById('sec_pwd');
		//length
		var name,uname,pwd;
		name = sec_name.value;
		uname = sec_uname.value;
		pwd = sec_pwd.value;

		if (sec_name.value == "") 
		{
			sec_name.focus();
		}
		else if (name.length < 5) 
		{
			sec_name.focus();
			swal("Oops","Name is too short","error");
		}
		else if (sec_uname.value == "") 
		{
			sec_uname.focus();
		}
		else if (uname.length < 5) 
		{
			sec_uname.focus();
			swal("Oops","Username is too short","error");
		}
		else if (sec_pwd.value == "") 
		{
			sec_pwd.focus();
		}
		else if (pwd.length < 5) 
		{
			sec_pwd.focus();
			swal("Oops","Password is too short","error");
		}
		else
		{
			var mydata = 'sec_name=' + sec_name.value + '&sec_uname=' + sec_uname.value + '&sec_pwd=' + sec_pwd.value;
			//alert(mydata);
			$.ajax({
				type:"POST",
				url:"signup_sec.php",
				data:mydata,
				cache:false,
				beforeSend:function(){
					$("#btn_reg_sec").text('loading...');
				},
				success:function(data){
					$("#btn_reg_sec").text('Register');
					//alert(data);
					if (data == 1) 
					{
						swal("Success","Account has been registered !","success");
						$("#sec_name").val('');
						$("#sec_uname").val('');
						$("#sec_pwd").val('');
						setTimeout(function(){window.location="index.php";},2000);
					}
					else if (data == 2) 
					{
						swal("Oops","Account is already exist !","error");
					}
					else
					{
						alert(data);
					}
				}
			});
		}
	}
</script>

</body>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/sweetalert.min.js"></script>
<script src="js/login.js"></script>
</html>