<?php
	session_start();
	if (!isset($_SESSION['id_sec'])) {
		@header('location:../');	
	}
	 include_once('../config.php');?>
	<!DOCTYPE html>
	<html lang="en-US">
	<head>
		<title><?php echo $_SESSION['sec_name'] ?></title>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="icon/png" href="../img/ssjalogo2.png">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../css/w3.css">
		<link rel="stylesheet" type="text/css" href="../css/animate.css">
		<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	</head>

<body onload="load_now();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<div class="text-center">
			</div>

			<div class="alert alert-info">	
			<h4 class="text-center"><i class="fa fa-edit"></i> Visitors Log</h4>
			<hr>
			<div class="text-center">
				<button data-toggle="modal" data-target="#add_visit" class="btn btn-primary"><i class="fa fa-plus"></i> Visitors</button>
			<a href="index.php" class="btn btn-danger">Go Back</a>
			</div>
			</div>

			<script type="text/javascript">
				function search_visitor(src){
					var mysrc = src;
					if (src == "") 
					{
						//alert("asd");
						$("#load_src").hide('fast');
						$("#load_data").show('fast');
						$("#page_load").show('fast');
					}
					else
					{
						$("#appender").removeClass('animated jackInTheBox');
						$("#load_src").show('fast');
						$("#load_data").hide('fast');
						$("#page_load").hide('fast');
						//alert("hello");
						var	mydata = 'mysrc=' + mysrc; 
						$.ajax({
							type:"POST",
							url:"visitors.php",
							data:mydata,
							cache:false,
							beforeSend:function(){
								$("#load_src").html('<center><img src="../img/flat.gif" width="80"></center>');
							},
							success:function(data){
								//alert(data);
								if (data == 1) 
								{
									$("#load_src").html('<div class="well">No records found...</div>');
								}
								else
								{
									$("#load_src").html(data);
								}
							}
						});
					}
				}
			</script>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-search"></i></span>
					<input type="text" name="search_visit" id="search_visit" oninput="search_visitor(this.value);" class="form-control" placeholder="Search...">
				</div>
			</div>	
			<hr>
			<div class="">
				<div id="b-up" onclick="backtop();" style="display: none; position:fixed; z-index: 10; right:15px; bottom:15px;"> <button class="btn w3-black w3-opacity btn-lg animated rubberBand"><i class="fa fa-arrow-up"></i></button></div>	
				<div id="load_src"></div>
				<div id="load_data"></div>
				<div id="page_load"></div>
				<br>
			</div>
			
		</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>

</body>
	<script src="../js/jquery.min.js"></script>
	<script src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/sweetalert.min.js"></script>
	<script src="../js/guard.js"></script>
</html>

<?php include_once('modal.php'); ?>