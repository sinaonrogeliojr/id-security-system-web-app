<?php
	session_start();
	if (!isset($_SESSION['id_sec'])) {
		@header('location:../');	
	}
	 include_once('../config.php');?>
	<!DOCTYPE html>
	<html lang="en-US">
	<head>
		<title><?php echo $_SESSION['sec_name'] ?></title>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="icon/png" href="../img/ssjalogo2.png">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../css/w3.css">
		<link rel="stylesheet" type="text/css" href="../css/animate.css">
		<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	</head>

<script type="text/javascript">
		function check_notif(){
		$.ajax({
			type:"POST",
			url:"checK_notif.php",
			data:"",
			cache:false,
			success:function(data){
				//alert(data);
				// if (data == 1 || data == 2)
				// {
				// 	$("#new_notif").hide('fast');
				// }
				// else if(data == 0)
				// {
				// 	$("#new_notif").show('fast');
				// }

				if (data == 0) 
				{
					$("#new_notif").hide('fast');
				}
				else
				{
					$("#new_notif").show('fast');
					$("#new_notif").text(data);
				}
				
			}
		});
	}
</script>

<script type="text/javascript">
	setInterval(function(){check_notif();},2000);
</script>
<body onload="check_notif();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<div class="text-center">
			</div>
			<div class="alert alert-info">
					<h4 class=" w3-large w3-padding-top text-center"><b> <?php echo $_SESSION['sec_name'] ?></b></h4>
				<hr>
				<div class="text-center"><i class="fa fa-shield "></i> Security & Information</div>
				
				
			</div>
			<hr>	
			<div class="">
				 <a href="announce.php" class="btn btn-block btn-lg w3-blue w3-hover-shadow w3-small" style="text-align:left;"><i class="fa fa-send"></i> ANNOUNCEMENT <span class="badge w3-red" id="new_notif" style="display: none;"><b> <i class="fa fa-envelope"></i> </b></span></a>

				 <div class="btn-group btn-group-justified btn-block">
				 	<a href="add_visitors.php" class="btn btn-block w3-blue btn-lg w3-hover-shadow w3-border-right w3-border-white w3-small"><i class="fa fa-edit"></i> VISITORS LOG</a>
					<a href="noid.php" class="btn btn-block w3-blue btn-lg w3-hover-shadow w3-border-right w3-border-white w3-small"><i class="fa fa-user"></i> NO ID ENTRY</a>
				 </div>
				 <div class="btn-group btn-group-justified btn-block">
				 	<a href="information.php" class="btn btn-block w3-blue btn-lg w3-hover-shadow w3-border-right w3-border-white w3-small" ><i class="fa fa-list-alt"></i> CHECK ATTENDANCE</a>
				 	<a href="calendar_event.php" class="btn btn-block w3-blue btn-lg w3-hover-shadow w3-border-right w3-border-white w3-small" ><i class="fa fa-calendar"></i> Calendar Event</a>
				 </div>
				<a href="#logout" data-toggle="modal"  data-target="#logout_me" class="btn btn-block btn-lg w3-red w3-hover-shadow w3-small" style="text-align:left;"><i class="fa fa-arrow-left"></i> LOGOUT</a>
			</div>
			
		</div>
		</div>

		<div class="col-sm-3"></div>
	</div>
</div>
</body>
	<script src="../js/jquery.min.js"></script>
	<script src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/sweetalert.min.js"></script>
</html>

<?php include_once('modal.php'); ?>