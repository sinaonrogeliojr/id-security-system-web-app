<?php
	session_start();
	if (!isset($_SESSION['id_sec'])) {
		@header('location:../');	
	}
	 include_once('../config.php');?>
	<!DOCTYPE html>
	<html lang="en-US">
	<head>
		<title><?php echo $_SESSION['sec_name'] ?></title>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="icon/png" href="../img/ssjalogo2.png">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../css/w3.css">
		<link rel="stylesheet" type="text/css" href="../css/animate.css">
		<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	</head>
<body onload="show_all();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<div class="text-center">
			</div>

			<input type="hidden" name="start" id="start" value="0">
			<input type="hidden" name="limit" id="limit" value="10">
			<a href="index.php" class="btn btn-block btn btn-primary">Home</a>
			<br>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i> </span>
					<select class="form-control" id="src_user" name="src_user" oninput="show_all(); $('#limit').val(10);" onchange="show_all();  $('#limit').val(10);">
						<option>Student</option>
						<option>Teacher</option>
						<option>Staff</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon" id="rem_btn" onclick="clear_btn();"><i class="fa fa-search" id="src_label"></i> <i id="src_rem" style="display: none;" class="fa fa-remove"></i></span>
					<input type="text" name="src_all" id="src_all" class="form-control" oninput="key_off_person(this.value);" placeholder="Search...">
					<span class="input-group-addon btn btn-info" onclick="sort_filter();">Go</span>
				</div>
			</div>
			<hr>	
			<div class="">
				<div id="all_person"></div>
				<div id="all_person_src"></div>
			</div>
		</div>
		</div>

		<div class="col-sm-3"></div>
	</div>
</div>
</body>
	<script src="../js/jquery.min.js"></script>
	<script src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/sweetalert.min.js"></script>
	<script src="../js/no_id.js"></script>
</html>

<?php include_once('modal.php'); ?>