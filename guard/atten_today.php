<?php 
session_start();
include_once("../config.php");

// 'Y-m-d'
$date = date('Y-m-d');

if (isset($_POST['date_n'],$_POST['name']) && $_POST['date_n'] != "" && $_POST['name'] != "" ) {


	$date_n = mysqli_real_escape_string($con,$_POST['date_n']);
	$name = mysqli_real_escape_string($con,$_POST['name']);


	$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_attendance a left join tbl_students b on a.person_id=b.student_id where CONCAT(b.lastname,', ',b.firstname) = '$name' and a.date_trans='$date_n' group by a.date_timelog order by a.date_timelog,a.trans_id desc  LIMIT 0,20 ");
	if (mysqli_num_rows($sql)>0) {
	while ($row = mysqli_fetch_assoc($sql)) {

						$date = date_create($row['date_timelog']);
						$date_show = date_format($date,'h:i:s');
						$day_show = date_format($date,'l');
						$date_right = date_create($row['date_trans']);
						$date_show_right = date_format($date,'F d, Y');
	?>
	<div class="well  w3-green w3-card-2 w3-hover-teal">
								<h6><?php echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'] ?></h6>
								<p><b><?php echo $day_show; ?> <span class="pull-right"q	><span class="w3-xxlarge"><i class="fa fa-user"></i></span><span class="w3-large"><i class="fa fa-check"></i></span></span></b> </p>
								<p class="w3-small"><?php echo $date_show_right ?>  <br>  <span class="w3-xxlarge"><?php echo $date_show; ?> <?php  echo $row['am_pm_status'] ?></span></p>	
							
	</div>
	<hr/>
	<?php
	}
	}
	else
	{
	?>
	<div class="well">No records found</div>
	<?php
	}
}

else
{



if (isset($_POST['mysrc'])) {

	$mysrc = mysqli_real_escape_string($con,$_POST['mysrc']);

	$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_attendance a left join tbl_students b on a.person_id=b.student_id where CONCAT(b.lastname,', ',b.firstname) like '%$mysrc%' and a.date_trans='$date' group by a.date_timelog,a.date_trans,a.am_pm_status order by b.lastname asc LIMIT 0,50 ");
	if (mysqli_num_rows($sql)>0) {
	while ($row = mysqli_fetch_assoc($sql)) {

						$date = date_create($row['date_timelog']);
						$date_show = date_format($date,'h:i');
						$day_show = date_format($date,'l');
						$date_right = date_create($row['date_trans']);
						$date_show_right = date_format($date,'F d, Y');
	?>
	<div class="well  w3-green w3-card-2 w3-hover-teal">
								<h6><?php echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'] ?></h6>
								<p><b><?php echo $day_show; ?> <span class="pull-right"q	><span class="w3-xxlarge"><i class="fa fa-user"></i></span><span class="w3-large"><i class="fa fa-check"></i></span></span></b> </p>
								<p class="w3-small"><?php echo $date_show_right ?>  <br>  <span class="w3-xxlarge"><?php echo $date_show; ?> <?php  echo $row['am_pm_status'] ?></span></p>	
							
	</div>
	<hr/>
	<?php
	}
	}
	else
	{
	?>
	<div class="well">No records found</div>
	<?php
	}
}
else if (isset($_POST['mysrcs'])) {
	$me = mysqli_real_escape_string($con,$_POST['mysrcs']);
	$mydate = date_create($me);
	$mysrcs = $me ;

	$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_attendance a left join tbl_students b on a.person_id=b.student_id where a.date_trans like '%$mysrcs%'  group by a.date_timelog,a.date_trans,a.am_pm_status LIMIT 0,20");
	if (mysqli_num_rows($sql)>0) {
	while ($row = mysqli_fetch_assoc($sql)) {

						$date = date_create($row['date_timelog']);
						$date_show = date_format($date,'h:i');
						$day_show = date_format($date,'l');
						$date_right = date_create($row['date_trans']);
						$date_show_right = date_format($date,'F d, Y');
	?>
	<div class="well  w3-green w3-card-2 w3-hover-teal">
								<h6><?php echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'] ?></h6>
								<p><b><?php echo $day_show; ?> <span class="pull-right"q	><span class="w3-xxlarge"><i class="fa fa-user"></i></span><span class="w3-large"><i class="fa fa-check"></i></span></span></b> </p>
								<p class="w3-small"><?php echo $date_show_right ?>  <br>  <span class="w3-xxlarge"><?php echo $date_show; ?> <?php  echo $row['am_pm_status'] ?></span></p>	
							
	</div>
	<hr/>
	<?php
	}
	}
	else
	{
	?>
	<div class="well">No records found</div>
	<?php
	}
}
}


 ?>