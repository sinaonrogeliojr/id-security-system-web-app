<?php
	session_start();
	if (!isset($_SESSION['id_sec'])) {
		@header('location:../');	
	}
	 include_once('../config.php');?>
	<!DOCTYPE html>
	<html lang="en-US">
	<head>
		<title><?php echo $_SESSION['sec_name'] ?></title>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="icon/png" href="../img/ssjalogo2">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../css/w3.css">
		<link rel="stylesheet" type="text/css" href="../css/animate.css">
		<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	</head>

<body >
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<div class="text-center">
			</div>
			<div class="alert alert-info">
				<h4 class=" w3-large w3-padding-top"><b><i class="fa fa-shield"></i> <?php echo $_SESSION['sec_name'] ?></b></h4>
				<hr>
				<div >Information</div>
			</div>
			<hr>
			<div class="">
				
				<a href="#logout" data-toggle="modal"  data-target="#logout_me" class="btn btn-block btn-default w3-hover-shadow" style="text-align:left;"><i class="fa fa-arrow-left"></i> Logout</a>
			</div>
		</div>
		</div>

		<div class="col-sm-3"></div>
	</div>
</div>
</body>
	<script src="../js/jquery.min.js"></script>
	<script src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/sweetalert.min.js"></script>
</html>

<?php include_once('modal.php'); ?>