<?php
	session_start();
	if (!isset($_SESSION['id_sec'])) {
		@header('location:../');	
	}
	 include_once('../config.php');?>
	<!DOCTYPE html>
	<html lang="en-US">
	<head>
		<title><?php echo $_SESSION['sec_name'] ?></title>
		 <meta charset="utf-8">
		 <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="icon" type="icon/png" href="../img/ssjalogo2">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../css/w3.css">
		<link rel="stylesheet" type="text/css" href="../css/animate.css">
		<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	</head>

<script type="text/javascript">


function input_src(){
	$("#dt").show('fast');	
}


function input_dt(){
	$("#sr").show('fast');	
}

function display_attendance_src(input){
	var inputs = document.getElementById('search');
	if (inputs.value == "") 
	{
		$("#attend").html('<div class="well">Search name or date...</div>'); 
	}
	else
	{
	var mysrc = input;
	var mydata = 'mysrc=' + mysrc;
	//$("#dt").hide('fast');	
	//alert(mydata);
	
	//alert(mydata);
	$.ajax({
		type:"POST",
		url:"atten_today.php",
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#attend").html('<center><img src="../img/flat.gif" width="100"></center>');
		},
		success:function(data){
			//alert(data);
			$("#attend").html(data);
			//console.log('Searching for name ');
		}
	});
	}
	
}




function display_attendance_date(input){
	var inputs_dt = document.getElementById('search_dt');
	if (inputs_dt.value == "") 
	{
		$("#attend").html('<div class="well">Search name or date...</div>'); 
	}
	else
	{
	var mysrcs = input;
	var mydata = 'mysrcs=' + mysrcs;
	//alert(mydata);
//	$("#sr").hide('fast');	
	//alert(mydata);
	$.ajax({
		type:"POST",
		url:"atten_today.php",
		data:mydata,
		cache:false,
		beforeSend:function(){
			$("#attend").html('<center><img src="../img/flat.gif" width="100"></center>');
		},
		success:function(data){
			//alert(data);
			$("#attend").html(data);
			//console.log('Searching for date');
		}
	});
	}
	
}


function display_same(){
	var date_n,name;
	date_n = document.getElementById('search_dt');
	name = document.getElementById('search');
	if (date_n.value == "" && name.value != "") 
	{
		 display_attendance_src(name.value);
	}
	else if (date_n.value != "" && name.value == "") 
	{
		display_attendance_date(date_n.value);
	}
	else if (date_n.value == "" && name.value == "") 
	{
		$("#attend").html('<div class="well">Search name or date...</div>'); 
	}
	else
	{
		var	myinfo = 'date_n=' + date_n.value + '&name=' + name.value;
		//alert(myinfo);
		$.ajax({
			type:"POST",
			url:"atten_today.php",
			data:myinfo,
			cache:false,
			beforeSend:function(){
			$("#attend").html('<center><img src="../img/flat.gif" width="100"></center>');
			},
			success:function(data){
				$("#attend").html(data);
				//console.log('Searching for Name and Date');

			}
		});
	}

}
</script>
<style>
    .jl-drop
    {
     position: relative;
    }
    .jl-drop .jl-drop-menu{
     position: absolute;
    top: 100%;
    left: 0;
    z-index: 1000;
    float: left;
    min-width: 100%;
    padding: 5px 0;
    margin: 2px 0 0;
    font-size: 14px;
    text-align: left;
    list-style: none;
    background-color: #fff;
    -webkit-background-clip: padding-box;
            background-clip: padding-box;
    border: 1px solid #ccc;
    border: 1px solid rgba(0, 0, 0, .15);
    border-radius: 4px;
    -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
            box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
    }
    .jl-drop-menu > li > a {
      display: block;
      padding: 3px 20px;
      clear: both;
      font-weight: normal;
      line-height: 1.42857143;
      color: #333;
      white-space: nowrap;
    }
    .jl-drop-menu  > li > a:hover,
    .jl-drop-menu  > li > a:focus {
      color: #262626;
      text-decoration: none;
      background-color: #b2b2b2;
    }
</style>


<script type="text/javascript">
 function my_src(id){
  var myid = id;
  var info = 'myid=' + myid;
  var this_me = document.getElementById('search');
  if (this_me.value == "") 
  {
     $("#mysearch").hide('fast');
  }
  else
  {
     $.ajax({
    type:"POST",
    url:"list_student.php",
    data:info,
    cache:false,
    beforeSend:function(){
      $("#mysearch").show('fast');
    },
    success:function(data){
      $("#mysearch").html(data);
    }
  });
  }
 
 }
</script>


<script type="text/javascript">
	function clear_btn_name(){
		var a = document.getElementById('search');
		if (a.value != "") 
		{
				$("#clr_input").show('fast');
		}
		else
		{
				$("#clr_input").hide('fast');
		}
	
	}

	function clear_btn_date(){
		var now_date = document.getElementById('get_now');
		var a = document.getElementById('search_dt');
		if (a.value == "" || a.value != now_date.value) 
		{
				$("#clr_inputs").show('fast');
		}
		else
		{
				$("#clr_inputs").hide('fast');
		}
	
	}
</script>


<body>
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<a href="index.php" class="btn btn-lg btn-primary btn-block">Home</a>
			<br/>
			<input type="hidden" name="get_now" id="get_now" value="<?php echo  date('Y-m-d'); ?>"/>
				<div class="input-group" id="sr">
							<span class="input-group-addon"><i class="fa fa-search"></i></span>
							<input type="text" name="search" id="search" onfocus=" display_same(); display_attendance_src(this.value);" onchange="my_src(this.value); display_attendance_date(this.value);   display_same(); display_attendance_src(this.value); clear_btn_name();"  onblur="input_src(); display_same();" oninput="my_src(this.value); display_attendance_date(this.value);  display_same(); display_attendance_src(this.value); clear_btn_name();" placeholder="Search..." class="form-control " />
							<span  class="input-group-addon  w3-ripple"><i id="clr_input" onclick="$('#mysearch').hide('fast'); $(this).hide('fast'); $('#search').val(''); $('#search').focus(); display_same();" style="display: none;"  class="fa fa-remove  w3-large"></i></span>
			</div>
			<div class="jl-drop">
			 <div id="mytoggle">
		         <div id="mysearch"></div>
		       </div>
			</div>
			<br>
				<div class="input-group" id="dt">
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<input type="date" name="search_dt" id="search_dt" value="<?php echo  date('Y-m-d'); ?>" onblur="input_dt(); display_same();" oninput="clear_btn_date(); display_attendance_date(this.value); display_same();" onfocus="display_attendance_date(this.value); display_same();" onchange=" display_attendance_date(this.value); display_same();" class="form-control "  />
							<span  class="input-group-addon  w3-ripple"><i id="clr_inputs" onclick="$('#search').focus(); $('#clr_inputs').hide('fast');  $('#search_dt').val('<?php echo  date('Y-m-d'); ?>'); display_same();  " style="display: none;"  class="fa fa-remove  w3-large"></i></span>
							
				</div>
			<hr>
			<div id="attend"><div id="page_load"></div><div class="well">Search name or date...</div></div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
	<script src="../js/jquery.min.js"></script>
	<script src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/sweetalert.min.js"></script>
</html>

