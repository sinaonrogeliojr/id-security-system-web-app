<?php 
session_start();
include_once("../config.php");
$src_user = mysqli_real_escape_string($con,$_POST['src_user']);
$limit = mysqli_real_escape_string($con,$_POST['limit']);
$start = mysqli_real_escape_string($con,$_POST['start']);

if (isset($_POST['src_all'])) {
$srch = $_POST['src_all'];


	if ($src_user == 'Student') {
		// echo $src_user;
		$total = mysqli_query($con,"SELECT count(student_id) from tbl_students");
		$count = mysqli_fetch_assoc($total);
		$s_total = $count['count(student_id)'];
		$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_students a left join tbl_student_picture b on a.student_id = b.student_id where concat(a.firstname,' ',' ',a.lastname,' ',a.mi) like '%$srch%' order by a.lastname,a.firstname,a.mi asc LIMIT ".$start.",".$limit."");
		if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
				?>
				<div class="well w3-card w3-hover-shadow"  onclick="add_no_id('<?php echo $row['student_id'] ?>')">
					<h5 class="w3-small" ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
						if ($str != null) {
							echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						}
						else
						{
							echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
						}
					?>
						<!-- <button class="close" style="margin-top:8px;" onclick="visit_teacher('<?php echo $row['teacher_id'] ?>')"><i class="fa fa-chevron-right"></i></button> -->
					</h5>
					</div>
				<?php
			}
			if ($s_total > $limit) {
				?>
				<br>
				<button class="btn btn-info btn-block" id="load_more" onclick="load_more(); $(this).text('Loading...');">Load more</button>
				<script type="text/javascript">
					function load_more(){
						var a = $("#limit").val();
						var new_limit = Number(a) + Number(a);
						$("#limit").val(new_limit);
						show_all_more();
					}
				</script>
				<?php
			}
			else
			{
				?>
				<div class="well">End of records...</div>
				<?php
			}
			
		}
		else
		{
			?>
			<div class="well">No records found...</div>
			<?php
		}
		//end of user student
	}
	else if ($src_user == 'Staff') {
	$total = mysqli_query($con,"SELECT count(staff_id) from tbl_staff");
		$count = mysqli_fetch_assoc($total);
		$s_total = $count['count(staff_id)'];
		$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_staff a left join tbl_staff_picture b on a.staff_id = b.staff_id where concat(a.firstname,' ',' ',a.lastname,' ',a.mi) like '%$srch%' order by a.lastname,a.firstname,a.mi asc LIMIT ".$start.",".$limit."");
		if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
				?>
				<div class="well w3-card w3-hover-shadow"  onclick="add_no_id('<?php echo $row['staff_id'] ?>')">
					<h5 class="w3-small" ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
						if ($str != null) {
							echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						}
						else
						{
							echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
						}
					?>
						<!-- <button class="close" style="margin-top:8px;" onclick="visit_teacher('<?php echo $row['teacher_id'] ?>')"><i class="fa fa-chevron-right"></i></button> -->
					</h5>
					</div>
				<?php
			}
			if ($s_total > $limit) {
				?>
				<br>
				<button class="btn btn-info btn-block" id="load_more" onclick="load_more(); $(this).text('Loading...');">Load more</button>
				<script type="text/javascript">
					function load_more(){
						var a = $("#limit").val();
						var new_limit = Number(a) + Number(a);
						$("#limit").val(new_limit);
						show_all_more();
					}
				</script>
				<?php
			}
			else
			{
				?>
				<div class="well">End of records...</div>
				<?php
			}
			
		}
		else
		{
			?>
			<div class="well">No records found...</div>
			<?php
		}
		//end
	}
	else if ($src_user == 'Teacher') {
			//start
	$total = mysqli_query($con,"SELECT count(teacher_id) from tbl_teachers");
		$count = mysqli_fetch_assoc($total);
		$s_total = $count['count(teacher_id)'];
		$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_teachers a left join tbl_teacher_picture b on a.teacher_id = b.teacher_id where concat(a.firstname,' ',' ',a.lastname,' ',a.mi) like '%$srch%' order by a.lastname,a.firstname,a.mi asc LIMIT ".$start.",".$limit."");
		if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
				?>
				<div class="well w3-card w3-hover-shadow" onclick="add_no_id('<?php echo $row['teacher_id'] ?>')">
					<h5 class="w3-small" ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
						if ($str != null) {
							echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						}
						else
						{
							echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
						}
					?>
						<!-- <button class="close" style="margin-top:8px;" onclick="visit_teacher('<?php echo $row['teacher_id'] ?>')"><i class="fa fa-chevron-right"></i></button> -->
					</h5>
					</div>
				<?php
			}
			if ($s_total > $limit) {
				?>
				<br>
				<button class="btn btn-info btn-block" id="load_more" onclick="load_more(); $(this).text('Loading...');">Load more</button>
				<script type="text/javascript">
					function load_more(){
						var a = $("#limit").val();
						var new_limit = Number(a) + Number(a);
						$("#limit").val(new_limit);
						show_all_more();
					}
				</script>
				<?php
			}
			else
			{
				?>
				<div class="well">End of records...</div>
				<?php
			}
			
		}
		else
		{
			?>
			<div class="well">No records found...</div>
			<?php
		}
		//end
	}

}
else
{

	if ($src_user == 'Student') {
		// echo $src_user;
		$total = mysqli_query($con,"SELECT count(student_id) from tbl_students");
		$count = mysqli_fetch_assoc($total);
		$s_total = $count['count(student_id)'];
		$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_students a left join tbl_student_picture b on a.student_id = b.student_id order by a.lastname,a.firstname,a.mi asc LIMIT ".$start.",".$limit."");
		if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
				?>
				<div class="well w3-card w3-hover-shadow"  onclick="add_no_id('<?php echo $row['student_id'] ?>')">
					<h5 class="w3-small" ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
						if ($str != null) {
							echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						}
						else
						{
							echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
						}
					?>
						<!-- <button class="close" style="margin-top:8px;" onclick="visit_teacher('<?php echo $row['teacher_id'] ?>')"><i class="fa fa-chevron-right"></i></button> -->
					</h5>
					</div>
				<?php
			}
			if ($s_total > $limit) {
				?>
				<br>
				<button class="btn btn-info btn-block" id="load_more" onclick="load_more(); $(this).text('Loading...');">Load more</button>
				<script type="text/javascript">
					function load_more(){
						var a = $("#limit").val();
						var new_limit = Number(a) + Number(a);
						$("#limit").val(new_limit);
						show_all_more();
					}
				</script>
				<?php
			}
			else
			{
				?>
				<div class="well">End of records...</div>
				<?php
			}
			
		}
		else
		{
			?>
			<div class="well">No records found...</div>
			<?php
		}
		//end of user student
	}
	else if ($src_user == 'Staff') {
	$total = mysqli_query($con,"SELECT count(staff_id) from tbl_staff");
		$count = mysqli_fetch_assoc($total);
		$s_total = $count['count(staff_id)'];
		$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_staff a left join tbl_staff_picture b on a.staff_id = b.staff_id order by a.lastname,a.firstname,a.mi asc LIMIT ".$start.",".$limit."");
		if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
				?>
				<div class="well w3-card w3-hover-shadow"  onclick="add_no_id('<?php echo $row['staff_id'] ?>')">
					<h5 class="w3-small" ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
						if ($str != null) {
							echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						}
						else
						{
							echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
						}
					?>
						<!-- <button class="close" style="margin-top:8px;" onclick="visit_teacher('<?php echo $row['teacher_id'] ?>')"><i class="fa fa-chevron-right"></i></button> -->
					</h5>
					</div>
				<?php
			}
			if ($s_total > $limit) {
				?>
				<br>
				<button class="btn btn-info btn-block" id="load_more" onclick="load_more(); $(this).text('Loading...');">Load more</button>
				<script type="text/javascript">
					function load_more(){
						var a = $("#limit").val();
						var new_limit = Number(a) + Number(a);
						$("#limit").val(new_limit);
						show_all_more();
					}
				</script>
				<?php
			}
			else
			{
				?>
				<div class="well">End of records...</div>
				<?php
			}
			
		}
		else
		{
			?>
			<div class="well">No records found...</div>
			<?php
		}
		//end
	}
	else if ($src_user == 'Teacher') {
			//start
	$total = mysqli_query($con,"SELECT count(teacher_id) from tbl_teachers");
		$count = mysqli_fetch_assoc($total);
		$s_total = $count['count(teacher_id)'];
		$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_teachers a left join tbl_teacher_picture b on a.teacher_id = b.teacher_id order by a.lastname,a.firstname,a.mi asc LIMIT ".$start.",".$limit."");
		if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
				?>
				<div class="well w3-card w3-hover-shadow" onclick="add_no_id('<?php echo $row['teacher_id'] ?>')">
					<h5 class="w3-small" ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
						if ($str != null) {
							echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						}
						else
						{
							echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
						}
					?>
						<!-- <button class="close" style="margin-top:8px;" onclick="visit_teacher('<?php echo $row['teacher_id'] ?>')"><i class="fa fa-chevron-right"></i></button> -->
					</h5>
					</div>
				<?php
			}
			if ($s_total > $limit) {
				?>
				<br>
				<button class="btn btn-info btn-block" id="load_more" onclick="load_more(); $(this).text('Loading...');">Load more</button>
				<script type="text/javascript">
					function load_more(){
						var a = $("#limit").val();
						var new_limit = Number(a) + Number(a);
						$("#limit").val(new_limit);
						show_all_more();
					}
				</script>
				<?php
			}
			else
			{
				?>
				<div class="well">End of records...</div>
				<?php
			}
			
		}
		else
		{
			?>
			<div class="well">No records found...</div>
			<?php
		}
		//end
	}

}

?>

<script type="text/javascript">
	function add_no_id(id){
		var pid = id;
		var mydata = 'pid=' + pid;

		swal({
		  title: "Are you sure ?",
		  text: "Do you want to add attendance?",
		  type: "info",
		  confirmButtonText: "Yes, I'm sure!",
		  showCancelButton: true,
		  closeOnConfirm: false,
		  showLoaderOnConfirm: true
		},
		function(){
		  setTimeout(function(){
		 // alert(mydata);
		  	$.ajax({
		  		type:"POST",
		  		url:"attend_person.php",
		  		data:mydata,
		  		cache:false,
		  		success:function(data){
		  			//alert(data);
		  			if (data == 1) 
		  			{
		  				swal("Success","Attendance has been added!","success");
		  			}
		  		}
		  	});
		  }, 2000);
		});
		
	}
</script>