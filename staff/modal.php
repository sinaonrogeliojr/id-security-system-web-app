<div class="modal fade" role="dialog" id="logout_me">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Logout</div>
			<div class="modal-body">
				<h5>Do you want to logout your account ?</h5>
				<hr>
				<a href="logout.php" class="btn btn-block btn-primary">Yes</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-default">No</a>
			</div>
		</div>
	</div>
</div>
<!--separator -->
<div class="modal fade" role="dialog" id="show_mypic">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Profile Picture</div>
			<div class="modal-body text-center">
				<?php 
				echo '<img src="data:image/jpeg;base64,'.base64_encode($_SESSION['st_picture']).'" class="img-thumbnail animated jackInTheBox" width="220"/>';
			 ?>
			 <hr>
			 <h5><?php echo $_SESSION['st_fullname'] ?></h5>
			 <h5><?php echo $_SESSION['st_post'] ?></h5>
			</div>
			
			<form  method="post"  enctype="multipart/form-data">
				<input type="file" name="image" id="image"  style="display: none;'">
				<div style="margin:20px 20px 5px 20px;">
					<input type="submit" id="btn_sub" name="btn_sub" style="display: none;" value="Update Picture" type="submit" class="btn btn-success btn-block">
				</div>
			</form>
			<div style="margin:0px 20px 20px 20px;">
				<button id="change_btn" class="btn btn-block btn-primary" onclick="$('#image').click();">Change Picture</button>
				<button onclick="close_change_pic();" data-dismiss="modal" class="btn btn-block btn-danger">Close</button>
			</div>
		</div>
	</div>
</div>
<?php 
	if (isset($_POST['btn_sub'])) {
		include_once("../config.php");
		$myid = $_SESSION['st_id'];
		$imageName=mysqli_real_escape_string($con,$_FILES["image"]["name"]);
		$imageData=mysqli_real_escape_string($con,file_get_contents($_FILES["image"]["tmp_name"]));
		$imageType=mysqli_real_escape_string($con,$_FILES["image"]["type"]);
		$date=Date("l F d, Y h:i:sa");
		?>
		<?php
		if(substr($imageType,0,5) == "image")
		{	

			$update = mysqli_query($con,"UPDATE tbl_staff_picture set image='$imageData' where staff_id='$myid'");	
			if ($update) {

			$stmt = mysqli_query($con,"SELECT a.*,b.* from tbl_staff a left join tbl_staff_picture b on a.staff_id = b.staff_id where b.staff_id='$myid'");
			$injec = mysqli_fetch_assoc($stmt);
			$_SESSION['st_picture'] = $injec['image'];
			?>
			<script type="text/javascript">
				alert("Profile Picture has been updated !");
				window.location="myinfo.php";
			</script>
			<?php
		}
		}	
		else{
			?>
			<script type="text/javascript">
				alert("Sorry Invalid image format or file is too large ! ");
			</script>
			<?php
		}
	}
 ?>

<script type="text/javascript">
	function close_change_pic(){
		$("#btn_sub").hide('fast');
		$("#change_btn").show('fast');
		$("image").val('');
	}
</script>



<script type="text/javascript">
	$(document).ready(function(){
		$("#image").on('change',function(){
			var mypicfile = document.getElementById('image');
			var info = mypicfile.value;
			if (mypicfile.value == "") 
			{
				$("#btn_sub").hide('fast');
				$("#change_btn").show('fast');
			}
			else
			{
				$("#btn_sub").show('fast');
				$("#change_btn").hide('fast');
				//alert(info);
			}
		});
	});
</script>




