<?php

session_start();
include_once("../config.php");
$myid = $_SESSION['st_id'];
$start = $_POST['start'];
$limit = $_POST['limit'];

$sql = mysqli_query($con,"SELECT * from tbl_documents where category = 'STAFF' or category = 'ALL' order by id desc limit ".$_POST['start'].", ".$_POST['limit']." ");

$sqls = mysqli_query($con,"SELECT count(id) from tbl_documents where uploaded_by='$myid'");
$get_max = mysqli_fetch_assoc($sqls);

if (mysqli_num_rows($sql)>0) {
	while ($row = mysqli_fetch_assoc($sql)) {
		$date = date_create($row['date_uploaded']);
		$mdate = date_format($date,'M d Y, h:i a');
			?>
			<div class="panel panel-info w3-card-2" id="<?php echo $row['id'] ?>">
				<div class="panel-heading">
					<h5 class="text-center"> <b><?php echo $row['doc_name']; ?> </b></h5>
				</div>
				<div class="panel-body">
					<div class="text-center">
						<?php 

						if($row['file_type'] == 'image/jpg' || $row['file_type'] == 'image/jpeg' || $row['file_type'] == 'image/png'){ ?>

							<a title="View Data" href="#" onclick="view_doc_data('<?php echo $row['id'] ?>')"><img src="<?php echo '../pr_pane/'.$row['file_name']; ?>" class="img img-resonsive img_size_150 img-thumbnail"></a>
			               
						<?php }else{ ?>

							<a title="View Data" href="../pr_pane/<?php echo $row['file_name']; ?>" target="_blank"><img src="../img/doc.jpg" class="img img-resonsive img_size_150 img-thumbnail"></a>

						<?php }
						
						?>

					<!-- <hr> -->
					<p></p>
					</div>
						
				</div>
				<div class="panel-footer">
					<div class="text-right w3-text-dark-grey"><b><small class="text-left">File Type: <?php echo $row['file_type']; ?></small></b> | <small><?php echo $mdate ?></small></div>
				</div>
			</div>
			<?php
	}

	if ($limit >= $get_max['count(id)']) {
	?>
	<button class="btn btn-block w3-light-grey  btn-lg">End of records..</button>
	<br>
	<?php
	}
	else
	{
	?>
	<button class="btn btn-block btn-info w3-card-2 btn-lg" onclick="load_more();">Load more</button>
	<br>
	<?php
	}
}
else
{
	?>
	<div class="well">No data found...</div>
	<?php
}
?>


<script type="text/javascript">
	function update_post(id,doc_name,category,file_name){
		// alert(id+' '+type+' '+msg);
		$("#doc_id").val(id);
		$("#txt_doc_name").val(doc_name);
		$("#txt_category").val(category);
		$("#el").val(id);
		//alert(aud);
		$("#btn_cupdate").show('fast');
		$("#btn_ann").text('Update');
		$("#"+id+"").hide('fast');
	}

	function load_more(){
	var start = document.getElementById('start');
	var limit = document.getElementById('limit');
	var b;
	b = Number(limit.value) + 10; 
	$("#limit").val(b);
	setTimeout(function(){
			display_documents();
	},1000);
	}
</script>
<script type="text/javascript">
	function del_post(id,file_name){

	var mydata = 'id=' + id + '&file_name=' + file_name;
    
    swal({
         title: "Are you sure ?",
         text: "You want to delete this post ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:"delete_document.php",
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 showToast.show("File has been deleted!",1900);
               	 // 
               	 $('#'+id+'').addClass('animated bounceOut');
               	 setTimeout(function(){display_documents();},1000);
                }
                else
                {
                    alert(data);
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });
	}
</script>
