<?php 
session_start();
?>
<table class="table table-hover w3-white w3-round-large table-bordered table-condensed">
					<tr>
						<td class="text-center">
							<button data-toggle="modal" data-target="#show_mypic" class="btn btn-primary btn-lg"><i class="fa fa-image"></i></button>
							<button class="btn btn-success btn-lg" onclick="$('#fn').val('<?php echo $_SESSION['st_fn'] ?>'); $('#mn').val('<?php echo $_SESSION['st_mn'] ?>'); $('#ln').val('<?php echo $_SESSION['st_ln'] ?>'); $('#bday').val('<?php echo $_SESSION['st_bday'] ?>'); $('#gender').val('<?php echo $_SESSION['st_gender'] ?>'); $('#age').val('<?php echo $_SESSION['st_age'] ?>'); $('#email').val('<?php echo $_SESSION['st_email'] ?>'); $('#number').val('<?php echo $_SESSION['st_contact'] ?>'); $('#address').val('<?php echo $_SESSION['st_addr'] ?>');" data-toggle="modal" data-target="#edit_teacher"><i class="fa fa-edit"></i></button>
							<a href="index.php" class="btn btn-danger btn-lg"><i class="fa fa-home"></i></a>
						</td>
					</tr>

					<tr>
						<td class=""><h6 ><strong>STAFF ID:</strong> <?php echo $_SESSION['st_id'] ?></h6></td>
					</tr>
					<tr>
						<td class=""><h6 ><strong>NAME:</strong> <?php echo $_SESSION['st_fullname'] ?></h6></td>
					</tr>
					<tr>
						<td class=""><h6 ><strong>BIRTHDATE:</strong> <?php $date_get = date_create($_SESSION['st_bday']); echo date_format($date_get,'M d, Y') ?></h6></td>
					</tr>
					<tr>
						<td class=""><h6 ><strong>GENDER:</strong> <?php echo $_SESSION['st_gender'] ?></h6></td>
					</tr>
					<tr>
						<td class=""><h6 ><strong>AGE:</strong> <?php echo $_SESSION['st_age'] ?></h6></td>
					</tr>
					<tr>
						<td class=""><h6 ><strong>CONTACT:</strong> <?php echo $_SESSION['st_contact'] ?></h6></td>
					</tr>
					<tr>
						<td class=""><h6 ><strong>E-MAIL:</strong> <?php echo $_SESSION['st_email'] ?></h6></td>
					</tr>
					<tr>
						<td class=""><h6 ><strong>ADDRESS:</strong> <?php echo $_SESSION['st_addr'] ?></h6></td>
					</tr>
					<tr>
						<td class=""><h6 ><strong>POSITION:</strong> <?php echo $_SESSION['st_post'] ?></h6></td>
					</tr>
				</table>


