<?php
session_start();
if (!isset($_SESSION['st_id'])) {
	@header('location:../');
}
 include_once('../config.php');?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['st_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<script type="text/javascript">
	function get_latest_info(){
		$.ajax({
			type:"POST",
			url:"my_data.php",
			data:"",
			cache:false,
			beforeSend:function(){
			$("#myinfo").html('<br><br><center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#myinfo").html(data);
			}
		});
	}

	function get_up_pics(){
		$.ajax({
			type:"POST",
			url:"new_pic.php",
			data:"",
			cache:false,
			success:function(data){
				$("#my_pictures").html(data);
			}
		});
	}
		function load_pics(){
		$.ajax({
			type:"POST",
			url:"new_pic.php",
			data:"",
			cache:false,
			beforeSend:function(){
				$("#my_pictures").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#my_pictures").html(data);
			}
		});
	}
</script>

<script type="text/javascript">
	setInterval(function(){get_up_pics();},2000);
</script>
<body class="w3-light-grey" onload="load_pics(); get_latest_info();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<div class="text-center">
			<div id="my_pictures"></div>
			</div>
			<hr>
			<div class="">
				<div id="myinfo"></div>
			</div>
			
		</div>
		</div>

		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>
<script type="text/javascript">
	function num_only(evt){
		var charcode= (evt.which) ? evt.which : event.keycode
		if (charcode > 31 && (charcode < 48 ||  charcode >57))
			return false;
		return true;
	}
</script>


</html>

<?php include_once('modal.php'); include_once('mymodal_info.php'); ?>