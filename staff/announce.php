<?php
session_start();
if (!isset($_SESSION['st_id'])) {
	@header('location:../');	
}
 include_once('../config.php');?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['st_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="../css/showToast.css">
</head>

<script type="text/javascript">
	function display_announce(){
		var start = document.getElementById('start');
		var limit = document.getElementById('limit');

		var mydata = 'start=' + start.value + '&limit=' + limit.value;
		$.ajax({
			type:"POST",
			url:"display_announcement.php",
			data:mydata,
			cache:false,
			success:function(data){
				$("#load_announce").html(data);
			}
		});
	}


	function check_seen(){
		$.ajax({
			type:"POST",
			url:"check_seen.php",
			data:"",
			cache:false,
			success:function(data){
				// if (data == 1) 
				// {
				// 	$("#new_notif").hide('fast');
				// }
				// else if(data == 0)
				// {
				// 	$("#new_notif").show('fast');
				// }
			}
		});
	}
</script>
<script type="text/javascript">
	setInterval(function(){display_announce(); check_seen();},2000);
</script>
<body onload="display_announce(); check_seen();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">

		<div class="">
			<input type="hidden" name="start" id="start" value="0">
			<input type="hidden" name="limit" id="limit" value="10">
			<a href="index.php" class="btn btn-primary btn-block">Home</a>
			<hr>
			<h4>Announcement</h4>
			<hr>
			
		</div>
		<div id="load_pend"></div>
		<div id="load_announce"></div>
		<!-- <div class="well">No Records found...</div> -->
		</div>

		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>
<script src="../js/showToast.js"></script>

</html>

<?php include_once('modals.php'); ?>