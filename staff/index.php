<?php
session_start();
if (!isset($_SESSION['st_id'])) {
	@header('location:../');	
}
 include_once('../config.php');?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['st_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<script type="text/javascript">
	function get_up_pics(){
		$.ajax({
			type:"POST",
			url:"new_pic.php",
			data:"",
			cache:false,
			success:function(data){
				$("#my_pictures").html(data);
			}
		});
	}
		function load_pics(){
		$.ajax({
			type:"POST",
			url:"new_pic.php",
			data:"",
			cache:false,
			beforeSend:function(){
				$("#my_pictures").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#my_pictures").html(data);
			}
		});
	}
		function check_notif(){
		$.ajax({
			type:"POST",
			url:"checK_notif.php",
			data:"",
			cache:false,
			success:function(data){
				//alert(data);
				// if (data == 1 || data == 2)
				// {
				// 	$("#new_notif").hide('fast');
				// }
				// else if(data == 0)
				// {
				// 	$("#new_notif").show('fast');
				// }

				if (data == 0) 
				{
					$("#new_notif").hide('fast');
				}
				else
				{
					$("#new_notif").show('fast');
					$("#new_notif").text(data);
				}
				
			}
		});
	}
</script>

<script type="text/javascript">
	setInterval(function(){get_up_pics(); check_notif();},2000);
</script>
<body class="w3-light-grey" onload="load_pics(); check_notif();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<div class="text-center">
			<div id="my_pictures"></div>
			</div>
			<h4 class="small w3-padding-top text-center animated pulse"><b><?php echo $_SESSION['st_fullname'] ?></b></h4>
			<h4 class="small w3-padding-top text-center animated pulse"><b><?php echo $_SESSION['st_post'] ?></b></h4>
			
			<hr>
			<div class="">
			<a href="announce.php" class="btn btn-block  w3-blue w3-hover-shadow w3-small" style="text-align:left;"><span class="badge w3-red" id="new_notif" style="display: none;"><b> <i class="fa fa-envelope"></i> </b></span> <i class="fa fa-send"></i> ANNOUNCEMENT </a>
			<a href="myinfo.php" class="btn btn-block  w3-blue w3-hover-shadow w3-small" style="text-align:left;"><i class="fa fa-info"></i> MY INFO</a>
			<a href="attendance.php" class="btn btn-block  w3-blue w3-hover-shadow w3-small" style="text-align:left;"><i class="fa fa-list-alt"></i> ATTENDANCE</a>
			<a href="calendar_event.php" class="btn btn-block  w3-blue w3-hover-shadow w3-small" style="text-align:left;"><i class="fa fa-calendar"></i> Calendar Event</a>
			<a href="documents.php" class="btn btn-block  w3-blue w3-hover-shadow w3-small" style="text-align:left;"><i class="fa fa-file"></i> Documents</a>
				<?php 
					if (isset($_SESSION['pr_id'])) {
						?>
							<a href="../pr_pane/break_staff.php" class="btn btn-block  w3-red w3-hover-shadow w3-small" style="text-align:left;"><i class="fa fa-arrow-left"></i> HOME</a>
						<?php
					}
					else
					{
						?>
							<a href="#logout" data-toggle="modal"  data-target="#logout_me" class="btn btn-block w3-red w3-hover-shadow w3-small" style="text-align:left;"><i class="fa fa-arrow-left"></i> Logout</a>
						<?php
					}

					 ?>
			</div>
			
		</div>
		</div>

		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>

</html>

<?php include_once('modals.php'); ?>