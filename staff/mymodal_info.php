
<div class="modal fade" role="dialog" id="edit_teacher">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Edit Info <button data-dismiss="modal" class="close">&times;</button></div>
			<div class="modal-body">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Firstname</span>
						<input type="text"  style="text-transform: uppercase;" name="fn" id="fn" class="form-control" placeholder="Enter your firstname..." value="<?php echo $_SESSION['st_fn']; ?>">
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Middlename</span>
						<input type="text"  style="text-transform: uppercase;" name="mn" id="mn" class="form-control" placeholder="Enter your middlename/Initial..." >
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Lastname</span>
						<input type="text"  style="text-transform: uppercase;" name="ln" id="ln" class="form-control" placeholder="Enter your lastname..." value="<?php echo $_SESSION['st_ln']; ?>">
					</div>
				</div>	

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Birthdate</span>
						<input type="date" name="bday" id="bday" class="form-control" value="<?php echo $_SESSION['st_bday']; ?>">
					</div>
				</div>	

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Gender</span>
						<select class="form-control" name="gender" id="gender" >
							<option selected="" disabled="">Choose your gender</option>
							<option>MALE</option>
							<option>FEMALE</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Age</span>
						<input type="text" maxlength="2"  onkeypress="return num_only(event);" name="age" id="age" min="1" class="form-control" placeholder="Enter your age..."  value="<?php echo $_SESSION['st_age']; ?>">
					</div>
				</div>	

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Email</span>
						<input type="text" name="email" id="email" class="form-control" placeholder="Enter your email..." value="<?php echo $_SESSION['st_email']; ?>">
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Contact number</span>
						<input type="text" maxlength="10" onkeypress="return num_only(event);" name="number" id="number" class="form-control" placeholder="Enter your contact number..." value="<?php echo $_SESSION['st_contact']; ?>">
					</div>
				</div>


				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Address</span>
						<textarea class="form-control" style="text-transform: uppercase;"  class="address" id="address" placeholder="Enter your address..." value="<?php echo $_SESSION['st_addr']; ?>"></textarea>
					</div>
				</div>
				</div>
			<div class="modal-footer">
				<button class="btn btn-success" id="btn-sv" onclick="save_changes();">Save Changes</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function save_changes(){
		var fn,mn,ln,bday,gender,age,email,number,address;
		fn = document.getElementById('fn');
		mn = document.getElementById('mn');
		ln = document.getElementById('ln');
		bday = document.getElementById('bday');
		gender = document.getElementById('gender');
		age = document.getElementById('age');
		email = document.getElementById('email');
		number = document.getElementById('number');
		address = document.getElementById('address');
		var mynum = number.value;

		if (fn.value == "") 
		{
			fn.focus();
		}
		else if (mn.value == "") 
		{
			mn.focus();
		}
		else if (ln.value == "") 
		{
			ln.focus();
		}
		else if (bday.value == "") 
		{
			bday.focus();
		}
		else if (gender.value == "Choose your gender" || gender.value == "") 
		{
			gender.focus();
		}
		else if (age.value == "" || age.value == 0) 
		{
			age.focus();
		}
		else if (email.value == "") 
		{
			email.focus();
		}
		else if (number.value == "") 
		{
			number.focus();
		}
		else if (mynum.length < 10) 
		{
				number.focus();
				swal("Oops!","Contact number must be 10 digits","error");
		}
		else if (address.value == "") 
		{
			address.focus();
		}
		else
		{
		 var mydata = 'fn=' + fn.value + '&mn=' + mn.value + '&ln=' + ln.value + '&bday=' + bday.value + '&gender=' + gender.value + '&age=' + age.value + '&email=' + email.value + '&number=' + number.value + '&address=' + address.value ;
		 //alert(mydata);

		 $.ajax({
		 	type:"POST",
		 	url:"update_teacher.php",
		 	data:mydata,
		 	cache:false,
		 	beforeSend:function(){
		 		$("#btn-sv").text('Loading...');
		 	},
		 	success:function(data){
		 	$("#btn-sv").text('Save Changes');
		 	if (data == 3) 
		 	{
		 		email.focus();
		 		swal("Oops!","Please enter valid email address!","error");
		 	}
		 	else if (data == 1) 
		 	{
		 			swal("Success","Successfully update records!","success");
		 			 get_latest_info();
		 			$("#edit_teacher").modal('hide');
		 			
		 	}
		 	else
		 	{
		 		alert(data);
		 	}
		 	}
		 });
		}


	}
</script>