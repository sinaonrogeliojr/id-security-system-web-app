<?php
session_start();
if (!isset($_SESSION['pr_id'])) {
	@header('location:../');	
}
 include_once('../config.php');?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['pr_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>

<script type="text/javascript">

function show_teachers_src(src){
		if (src == "") 
		{
			show_teachers();
		}
		else
		{
		var search = src;
		var mydata = 'search=' + search;
		$.ajax({
			type:"POST",
			url:"tbl_teachers.php",
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#teachers").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#teachers").html(data);
			}

		});
		}
	}



	function show_teachers(){
		var start,limit;
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		var mydata = 'start=' + start.value + '&limit=' + limit.value;
		$.ajax({
			type:"POST",
			url:"tbl_teachers.php",
			data:mydata,
			cache:false,
			success:function(data){
					$("#teachers").html(data);
					$("#btn_load_tc").text('load more');
				
			}

		});
	}


	function display_teac(){
		var start,limit;
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		var mydata = 'start=' + start.value + '&limit=' + limit.value;
		$.ajax({
			type:"POST",
			url:"tbl_teachers.php",
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#teachers").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#teachers").html(data);
			}

		});
	}
</script>

<input type="hidden" name="start" id="start" value="0">
<input type="hidden" name="limit" id="limit" value="10">


<body class="w3-light-grey"  onload="display_teac();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			
			<div class="">
				<a href="index.php" class="btn btn-primary btn-block">Home</a>
				<br>
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-search"></i></span>
						<input type="text" name="src" oninput="show_teachers_src(this.value);" id="src" class="form-control" placeholder="Search...">
					</div>
				</div>
				<div id="teachers"></div>
			</div>
			
		</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>

</html>

<?php include_once('modals.php'); ?>