<?php  
session_start();
if (!isset($_SESSION['pr_id'])) {
  @header('location:../');  
}
include_once('../config.php');

$teacher_id = $_SESSION['pr_id'];
 //export.php  
 if(!empty($_FILES["excel_file2"]))  
 {  
      $file_array = explode(".", $_FILES["excel_file2"]["name"]);  
      if($file_array[1] == "xlsx")  
      {  

        include("../teacher/PHPExcel/IOFactory.php");
           $output = '';  
           $output .= "  
           <label class='text-success'>Data Inserted</label>  
                <table class='table table-bordered'>  
                     <tr>  
                          <th>SUBJECT CODE</th>  
                          <th>SUBJECT DESC</th>  
                          <th>DAY</th> 
                          <th>TIME</th>   
                     </tr>  
                     ";  

           $object = PHPExcel_IOFactory::load($_FILES["excel_file2"]["tmp_name"]);  

           foreach($object->getWorksheetIterator() as $worksheet)  
           {  

                $highestRow = $worksheet->getHighestRow();

                $sem = $worksheet->getCell('C4')->getValue();
                $section = $worksheet->getCell('C7')->getValue();
               
                for($row=12; $row<=$highestRow; $row++)  
                {  

                     $no = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(0, $row)->getValue()); 
                     $subject_code = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(1, $row)->getValue());  
                     $subject_desc = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(2, $row)->getValue()); 
                     $day = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(3, $row)->getValue()); 
                     $time_ = mysqli_real_escape_string($con, $worksheet->getCellByColumnAndRow(4, $row)->getValue());
         
                      if($no === ""){

                      }else{

                        $chk_data = mysqli_query($con, "SELECT * FROM tbl_schedule where section = '$section' and subject_code = '$subject_code' and school_year = '$sem '");

                        if(mysqli_num_rows($chk_data) > 0){
                          //update
                          //echo 'update';
                          $query = "  
                         UPDATE tbl_schedule set subject_code = '".$subject_code."',subject_desc = '".$subject_desc."',day_ = '".$day."',time_ = '".$time_."' where section = '".$section."' and subject_code = '".$subject_code."' and school_year = '".$sem ."'";  

                        }else{
                          
                          //insert
                          $query = "  
                         INSERT INTO tbl_schedule(section, subject_code,subject_desc,day_,time_,school_year,is_active )
                         VALUES ('".$section."', '".$subject_code."', '". $subject_desc ."', '". $day ."', '".$time_."', '".$sem."',1)  
                         ";  
                        }

                         mysqli_query($con, $query);  

                         if($query){
                          //echo 'insert';
                         }
                         $output .= '  
                         <tr>   
                              <td>'.$subject_code.'</td>  
                              <td>'.$subject_desc.'</td>  
                              <td>'.$day.'</td>  
                              <td>'.$time_.'</td>  
                         </tr>  
                         '; 

                      }
                }  
           }  

           $output .= '</table>';  
           echo $output;  

      }  
      else  
      {  
           echo '<label class="text-danger">Invalid File</label>';  
      }  
 }  

 ?>  