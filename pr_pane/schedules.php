<?php
session_start();
if (!isset($_SESSION['pr_id'])) {
	@header('location:../');	
}
 include_once('../config.php');

$section = $_GET['section'];
$sql = mysqli_query($con, "SELECT t2.`grade`, t1.`section` FROM tbl_section t1
LEFT JOIN tbl_grade t2 ON t1.`grade_id` = t2.`ID` where t1.`section` = '$section'");

$row = mysqli_fetch_assoc($sql);

?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['pr_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="../css/showToast.css">
</head>

<script type="text/javascript">

// function show_sched_src(src){
// 		var section = $('#section').val();
// 		var day_ = $('#day_').val();

// 		if (src == "") 
// 		{
// 			show_schedules();
// 		}
// 		else
// 		{
// 		var search = src;
// 		var mydata = 'search=' + search + '&section=' + section + '&day_=' + day_ ;
// 		$.ajax({
// 			type:"POST",
// 			url:"tbl_schedules.php",
// 			data:mydata,
// 			cache:false,
// 			beforeSend:function(){
// 				$("#schedules").html('<center><img src="../img/flat.gif" width="50"></center>');
// 			},
// 			success:function(data){
// 				$("#schedules").html(data);
// 			}

// 		});
// 		}
// 	}

	function show_schedules(){
		var start,limit,section;
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		section = $('#section').val();
		var day_ = $('#day_2').val();

		var mydata = 'start=' + start.value + '&limit=' + limit.value + '&section=' + section + '&day_2=' + day_ ;
		$.ajax({
			type:"POST",
			url:"tbl_schedules.php",
			data:mydata,
			cache:false,
			success:function(data){
					$("#schedules").html(data);
					$("#btn_gl").text('load more');
				
			}

		});
	}

	function display_schedules(){
		var start,limit,section;
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		section = $('#section').val();
		var day_ = $('#day_2').val();
		var mydata = 'start=' + start.value + '&limit=' + limit.value + '&section=' + section + '&day_2=' + day_ ;
		$.ajax({
			type:"POST",
			url:"tbl_schedules.php",
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#schedules").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#schedules").html(data);
			}

		});
	}

	function goBack() {
	  window.history.back();
	}

	function manage_sched() {
		var sched_id,section,subject_code,subject_desc,day_,time_,school_year;

		sched_id = $('#sched_id').val();
		section = $('#section').val();
		subject_code = $('#subject_code').val();
		subject_desc = $('#subject_desc').val();
		day_ = $('#day_').val();
		time_ = $('#time_').val();
		school_year = $('#school_year').val();

		if(subject_code === ""){
			$('#subject_code').focus();
		}else if(subject_desc === ""){
			$('#subject_desc').focus();
		}else{

			var mydata = 'sched_id=' + sched_id + '&section=' + section + '&subject_code=' + subject_code + '&subject_desc=' + subject_desc + '&day_=' + day_ + '&time_=' + time_;

			$.ajax({
			type:"POST",
			url:"update_schedule.php",
			data:mydata,
			cache:false,
			success:function(data){

				if(data == 1){
					showToast.show("Schedule Successfully Saved",1000); 
					$('#sched_id').val('');
					$('#subject_code').val('');
					$('#subject_desc').val('');
					$('#day_').val('');
					$('#time_').val('');
					$('#school_year').val('');
					$('#manage_schedule').modal('hide');
					show_schedules();
				}else if(data == 2){
					showToast.show("Schedule Successfully Updated",1000); 
					$('#sched_id').val('');
					$('#subject_code').val('');
					$('#subject_desc').val('');
					$('#day_').val('');
					$('#time_').val('');
					$('#school_year').val('');
					$('#manage_schedule').modal('hide');
					show_schedules();
				}else{
					alert(data);
				}

			}

		});
		
		}

	}

function clear_schedule(){

	var section = $('#section').val();
	var mydata = 'section=' + section;

	swal({
         title: "Are you sure ?",
         text: "You want to clear this schedule for " + section,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:"clear_sched.php",
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
                 show_schedules();
              	 showToast.show("Schedules has been removed!",1000);
                }
                else
                {
                    alert(data);
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });

	}

</script>

<input type="hidden" name="start" id="start" value="0">
<input type="hidden" name="limit" id="limit" value="10">
<input type="hidden" id="section" value="<?php echo $_GET['section']; ?>">
<input type="hidden" id="section_id" value="<?php echo $_GET['section_id']; ?>">

<body class="w3-light-grey"  onload="display_schedules();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<div class="">
				<div class="btn-group btn-group-justified btn-block">
					<a href="index.php" class="btn btn-primary btn-block"><span class="fa fa-home"></span> Home</a>
					<a onclick="goBack();" class="btn btn-primary btn-block"><span class="fa fa-arrow-left"></span> Back</a>
				</div>
				<div class="w3-panel w3-red">SCHEDULE FOR <b><em><?php echo $row['grade'] . " - " . $row['section']; ?></em></b>
				</div> 
				<div class="btn-group btn-group-justified btn-block">
					<a class="btn btn-sm btn-default btn-block" onclick="view_modal_sched();"><span class="fa fa-upload"></span> Upload Schedule</a>
					<a class="btn btn-sm btn-default btn-block" data-toggle="modal" data-target="#manage_schedule"><span class="fa fa-plus"></span> Add Schedule</a>
					<a class="btn btn-sm btn-default btn-block" onclick="clear_schedule();"><span class="fa fa-trash"></span> Clear Schedule</a>
				</div>
				<div class="form-group">
				  <label for="day_2">Select Day: </label>
				  <select class="form-control" id="day_2" oninput="display_schedules();" onchange="display_schedules();">
				  	<option value="ALL">All</option>
				    <option value="MONDAY">MONDAY</option>
				    <option value="TUESDAY">TUESDAY</option>
				    <option value="WEDNESDAY">WEDNESDAY</option>
				    <option value="THURSDAY">THURSDAY</option>
				    <option value="FRIDAY">FRIDAY</option>
				    <option value="SATURDAY">SATURDAY</option>
				  </select>
				</div>
				<div id="schedules"></div>
			</div>
			
		</div>

		</div>

		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>
<script src="../js/showToast.js"></script>

<?php include_once('modals.php'); ?>

</html>

<script type="text/javascript">
	function view_modal_sched(){
		$('#m-header').html('Upload Schedule');
		$('#upload_schedule').modal('show');
	}
</script>
