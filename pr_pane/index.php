<?php
session_start();
if (!isset($_SESSION['pr_id'])) {
	@header('location:../');	
}
 include_once('../config.php');?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['pr_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<script type="text/javascript">
	function get_up_pics(){
		$.ajax({
			type:"POST",
			url:"new_pic.php",
			data:"",
			cache:false,
			success:function(data){
				$("#my_pictures").html(data);
			}
		});
	}
		function load_pics(){
		$.ajax({
			type:"POST",
			url:"new_pic.php",
			data:"",
			cache:false,
			beforeSend:function(){
				$("#my_pictures").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#my_pictures").html(data);
			}
		});
	}
</script>

<script type="text/javascript">
	setInterval(function(){get_up_pics();},2000);
</script>
<body class="w3-light-grey" onload="load_pics();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<div class="form-group"></div>
			<div class="text-center">
			<div id="my_pictures"></div>
			</div>
			<h3 class="w3-medium w3-padding-top text-center w3-text-dark-grey"><b><?php echo $_SESSION['pr_fullname'] ?></b></h3>
			<h3 class="w3-medium w3-padding-top text-center w3-text-dark-grey"><b><?php echo $_SESSION['pr_post'] ?></b></h3>
			<hr>
			<div class="">

				<div class="btn-group btn-group-justified btn-block">
				<a href="announce.php" class="btn btn-lg w3-blue w3-hover-shadow w3-border-right w3-small" ><i class="fa fa-send"></i> ANNOUNCEMENT</a>

				<a href="event.php" class="btn btn-lg w3-blue w3-hover-shadow w3-border-left w3-border-right w3-small"><i class="fa fa-calendar"></i> EVENT</a>

				<a href="add_visitors.php" class="btn w3-blue btn-lg w3-hover-shadow w3-border-left w3-small"><i class="fa fa-edit"></i> VISITORS</a>
				
				</div>
				<div class="btn-group btn-group-justified btn-block">
					<a href="list_teachers.php" class="btn btn-lg w3-blue w3-hover-shadow w3-border-right w3-small"> <i class="fa fa-book"></i> TEACHERS </a>
					<a href="advisory.php" class="btn btn-lg w3-blue w3-hover-shadow w3-border-left w3-border-right w3-small" > <i class="fa fa-user"></i> STUDENTS</a>
					<a href="staff.php" class="btn w3-blue btn-lg w3-hover-shadow w3-border-left w3-small" > <i class="fa fa-shield"></i> STAFF</a>
				</div>
				<!-- <label><small>TEACHERS</small></label> -->

				<div class="btn-group btn-group-justified btn-block">
					<a href="early.php" class="btn btn-block btn-lg w3-blue w3-hover-shadow w3-small  w3-border-right"><i class="fa fa-twitter"></i> EARLY BIRDS</a>

				<a href="late_absent.php" class="btn btn-block btn-lg w3-blue w3-hover-shadow w3-small  w3-border-left  w3-border-right"><i class="fa fa-remove"></i> ABSENT</a>

				<a href="late.php" class="btn btn-block btn-lg w3-blue w3-hover-shadow w3-small  w3-border-left"><i class="fa fa-warning"></i> LATE</a>

				</div>
<!-- 				<a href="setup.php" class="btn btn-block btn-lg w3-blue w3-hover-shadow w3-small" style="text-align:left;"><i class="fa fa-gear"></i> SCHOOL SETUP</a>
 -->			<div class="btn-group btn-group-justified btn-block">
					<a href="setup.php" class="btn btn-lg w3-blue w3-hover-shadow w3-border-right w3-small"> <i class="fa fa-book"></i> SCHOOL SETUP </a>
					<a href="documents.php" class="btn btn-lg w3-blue w3-hover-shadow w3-border-left w3-border-right w3-small" > <i class="fa fa-file"></i> DOCUMENTS</a>
					<a href="grade_level.php" class="btn btn-lg w3-blue w3-hover-shadow w3-border-left w3-border-right w3-small" > <i class="fa fa-calendar"></i> SCHEDULES</a>
				</div>
				<a href="#logout" data-toggle="modal"  data-target="#logout_me" class="btn btn-block btn-lg w3-red w3-hover-shadow w3-small" style="text-align:left;"><i class="fa fa-arrow-left"></i> LOGOUT</a>
				<br><br>
			</div>
			
		</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>

</html>

<?php include_once('modals.php'); ?>