<?php
session_start();
if (!isset($_SESSION['pr_id'])) {
	@header('location:../');	
}
 include_once('../config.php');?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['pr_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>

<script type="text/javascript">

function show_section_src(src){
		grade_id = $('#grade_id').val();
		if (src == "") 
		{
			show_sections();
		}
		else
		{
		var search = src;
		var mydata = 'search=' + search + '&grade_id=' + grade_id;
		$.ajax({
			type:"POST",
			url:"tbl_sections.php",
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#sections").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#sections").html(data);
			}

		});
		}
	}

	function show_sections(){
		var start,limit;
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		grade_id = $('#grade_id').val();
		var mydata = 'start=' + start.value + '&limit=' + limit.value + '&grade_id=' + grade_id;
		$.ajax({
			type:"POST",
			url:"tbl_sections.php",
			data:mydata,
			cache:false,
			success:function(data){
					$("#sections").html(data);
					$("#btn_gl").text('load more');
				
			}

		});
	}

	function display_section(){
		var start,limit,grade_id;
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		grade_id = $('#grade_id').val();
		var mydata = 'start=' + start.value + '&limit=' + limit.value + '&grade_id=' + grade_id;
		$.ajax({
			type:"POST",
			url:"tbl_sections.php",
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#sections").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#sections").html(data);
			}

		});
	}

	function goBack() {
	  window.history.back();
	}

</script>

<input type="hidden" name="start" id="start" value="0">
<input type="hidden" name="limit" id="limit" value="10">
<input type="hidden" id="grade_id" value="<?php echo $_GET['grade_id']; ?>">

<body class="w3-light-grey"  onload="display_section();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			
			<div class="">
				<div class="btn-group btn-group-justified btn-block">
					<a href="index.php" class="btn btn-primary btn-block"><span class="fa fa-home"></span> Home</a>
					<a href="#" onclick="goBack();" class="btn btn-primary btn-block"><span class="fa fa-arrow-left"></span> Back</a>
				</div>
				
				<div class="w3-panel w3-blue">MANAGE SCHEDULE / GRADE LEVELS / SECTIONS</div> 
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-search"></i></span>
						<input type="text" name="src" oninput="show_section_src(this.value);" id="src" class="form-control" placeholder="Search...">
					</div>
				</div>
				<div id="sections"></div>
			</div>
			
		</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>

</html>

<?php include_once('modals.php'); ?>