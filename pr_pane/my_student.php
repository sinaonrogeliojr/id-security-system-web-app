<?php
session_start();
include_once("../config.php");

if (isset($_POST['myid'])) {

				 $mysrc = mysqli_real_escape_string($con,$_POST['myid']);
				$stmt = mysqli_query($con,"SELECT a.*,b.* from tbl_students a left join tbl_student_picture b on a.student_id = b.student_id where CONCAT(a.lastname,', ',a.firstname,' ',a.mi) like '%$mysrc%'  group by a.lastname,a.firstname,a.mi order by a.lastname,a.firstname,a.mi asc LIMIT 0,5 ");

				if (mysqli_num_rows($stmt)>0) {
					while ($row = mysqli_fetch_assoc($stmt)) {
						$popups = '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="190" class="img-thumbnail"/>';
					?>
					<script type="text/javascript">
						$('[data-toggle="popover"]').popover(); 
					</script>
					<div class="well w3-card" data-toggle="popover" data-placement="top" data-trigger="hover" data-content='<?php echo $popups; ?>' data-html="true">
					<h5 class="w3-small" ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
						if ($str != null) {
							echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						}
						else
						{
							echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
						}
					?>
						<button class="close" style="margin-top:8px;" onclick="visit_student('<?php echo $row['student_id'] ?>')"><i class="fa fa-chevron-right"></i></button>
					</h5>
					</div>
					<?php
					}
				}
				else
				{
				?>
				<div class="well">No records found...</div>
				<?php
				}
}
else
{

				$start = $_POST['start'];
				$limit = $_POST['limit'];

		 		$sqls = mysqli_query($con,"SELECT count(student_id) from tbl_students");
				$get_max = mysqli_fetch_assoc($sqls);
				$stmt = mysqli_query($con,"SELECT a.*,b.* from tbl_students a left join tbl_student_picture b on a.student_id = b.student_id  group by a.lastname,a.firstname,a.mi order by a.lastname,a.firstname,a.mi asc limit ".$start.",".$limit." ");

				if (mysqli_num_rows($stmt)>0) {
					while ($row = mysqli_fetch_assoc($stmt)) {
						$popups = '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="190" class="img-thumbnail"/>';
					?>
					<script type="text/javascript">
						$('[data-toggle="popover"]').popover(); 
					</script>
					<div class="well w3-card" data-toggle="popover" data-placement="top" data-trigger="hover" data-content='<?php echo $popups; ?>' data-html="true">
					<h5 class="w3-small" ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
						if ($str != null) {
							echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						}
						else
						{
							echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
						}
					?>
						<button class="close" style="margin-top:8px;" onclick="visit_student('<?php echo $row['student_id'] ?>')"><i class="fa fa-chevron-right"></i></button>
					</h5>
					</div>
						<?php
					}

					if ($limit >= $get_max['count(student_id)']) {
					?>
					<button class="btn btn-block w3-light-grey  btn-lg">limit of records..</button>
					<br>
					<?php
					}
					else
					{
					?>
					<button class="btn btn-block btn-info w3-card-2 btn-lg" id="btn_load_tc" onclick="load_more(); $(this).text('Loading...');">Load more</button>
					<br>
					<?php
					}
				}
				else
				{
				?>
				<div class="well">No records found...</div>
				<?php	
				}



}


			

?>


<script type="text/javascript">
	function load_more(){
	var start = document.getElementById('start');
	var limit = document.getElementById('limit');
	var b;
	b = Number(limit.value) + 10; 
	$("#limit").val(b);
	setTimeout(function(){
	get_up_list();
	},1000);
	}
</script>

<script type="text/javascript">
	function visit_student(id){
		var tid = id;
		var info = 'tid=' + tid;
		$.ajax({
			type:"POST",
			url:"get_session_from_student.php",
			data:info,
			cache:false,
			success:function(data){
				//alert(data);
				if (data == 1) 
				{
					window.location="../client";
				}
				else
				{
					alert(data);
				}
			}
		});
	}
</script>