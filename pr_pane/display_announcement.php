<?php 
session_start();
include_once("../config.php");
$myid = $_SESSION['pr_id'];
$start = $_POST['start'];
$limit = $_POST['limit'];

$sql = mysqli_query($con,"SELECT * from tbl_announcement where userid='$myid' order by id desc limit ".$_POST['start'].", ".$_POST['limit']." ");

$sqls = mysqli_query($con,"SELECT count(id) from tbl_announcement  where userid='$myid'");
$get_max = mysqli_fetch_assoc($sqls);

if (mysqli_num_rows($sql)>0) {
	while ($row = mysqli_fetch_assoc($sql)) {
		$date = date_create($row['date_post']);
		$mdate = date_format($date,'M d Y, h:i a');
		if ($row['type'] == null) {
			?>
			<div class="panel panel-info w3-card-2" id="<?php echo $row['id'] ?>">
				<div class="panel-heading"> <span class="w3-round-large w3-blue" style="padding:4px;">Others</span> Concern: <?php echo $row['audience'] ?>
					<span class="dropdown pull-right">
					<button class="close dropdown-toggle" data-toggle="dropdown"><i class="fa fa-chevron-down "></i></button>
						<ul class="dropdown-menu">
							<li><a href="#" class="" onclick="update_post('<?php echo $row['post_id'] ?>','<?php echo $row['type'] ?>','<?php echo $row['msg'] ?>','<?php echo $row['id'] ?>','<?php echo $row['audience'] ?>');"><i class="fa fa-edit"></i> Edit</a></li>
							<li class="divider"></li>
							<li><a href="#" class="" onclick="del_post('<?php echo $row['post_id'] ?>','<?php echo $row['id'] ?>');"><i class="fa fa-trash" ></i> Delete</a></li>
						</ul>
					</span>
				</div>
				<div class="panel-body">
						<p><b>From office of the principal</b></p>
					<hr>
					<p class="w3-text-dark-grey" style="word-break:break-all;"><?php echo $row['msg']; ?></p>
					<p></p>
				</div>
				<div class="panel-footer">
					<div class="text-right w3-text-dark-grey"><small><?php echo $mdate ?></small></div>
				</div>
			</div>
			<?php
		}
		else
		{
			?>
			<div class="panel panel-info w3-card-2" id="<?php echo $row['id'] ?>">
				<div class="panel-heading"> <span class="w3-round-large w3-blue" style="padding:4px;"><?php echo $row['type']; ?></span> Concern: <?php echo $row['audience'] ?>
					<span class="dropdown pull-right">
					<button class="close dropdown-toggle" data-toggle="dropdown"><i class="fa fa-chevron-down "></i></button>
						<ul class="dropdown-menu">
							<li><a href="#" class="" onclick="update_post('<?php echo $row['post_id'] ?>','<?php echo $row['type'] ?>','<?php echo $row['msg'] ?>','<?php echo $row['id'] ?>','<?php echo $row['audience'] ?>');"><i class="fa fa-edit"></i> Edit</a></li>
							<li class="divider"></li>
							<li><a href="#" class="" onclick="del_post('<?php echo $row['post_id'] ?>','<?php echo $row['id'] ?>');"><i class="fa fa-trash" ></i> Delete</a></li>
						</ul>
					</span>
				</div>
				<div class="panel-body">
						<p><b>From office of the principal</b></p>
					<hr>
					<p class="w3-text-dark-grey" style="word-break:break-all;"><?php echo $row['msg']; ?></p>
					<p></p>
				</div>
				<div class="panel-footer">
					<div class="text-right w3-text-dark-grey"><small><?php echo $mdate ?></small></div>
				</div>
			</div>

			<?php
		}
	}

	if ($limit >= $get_max['count(id)']) {
	?>
	<button class="btn btn-block w3-light-grey  btn-lg">End of records..</button>
	<br>
	<?php
	}
	else
	{
	?>
	<button class="btn btn-block btn-info w3-card-2 btn-lg" onclick="load_more();">Load more</button>
	<br>
	<?php
	}
}
else
{
	?>
	<div class="well">No data found...</div>
	<?php
}
?>


<script type="text/javascript">
	function update_post(id,type,msg,element,aud){
		// alert(id+' '+type+' '+msg);
		$("#user_id").val(id);
		$("#cat").val(type);
		$("#post_ann").val(msg);
		$("#el").val(element);
		$("#audience").val(aud);
		//alert(aud);
		$("#btn_cupdate").show('fast');
		$("#btn_ann").text('Update');
		$("#"+element+"").hide('fast');
	}

	function load_more(){
	var start = document.getElementById('start');
	var limit = document.getElementById('limit');
	var b;
	b = Number(limit.value) + 10; 
	$("#limit").val(b);
	setTimeout(function(){
			display_announce();
	},1000);
	}
</script>
<script type="text/javascript">
	function del_post(id,element){
	var myid = id;
	var mydata = 'myid=' + myid;
    
    swal({
         title: "Are you sure ?",
         text: "You want to delete this post ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:"del_post.php",
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
              	 showToast.show("Post has been deleted!",1900);
               	 // 
               	 $('#'+element+'').addClass('animated bounceOut');
               	 setTimeout(function(){display_announce();},1000);
                }
                else
                {
                    alert(data);
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });
	}
</script>
