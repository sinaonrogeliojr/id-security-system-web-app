<div class="modal fade" role="dialog" id="logout_me">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Logout</div>
			<div class="modal-body">
				<h5>Do you want to logout your account ?</h5>
				<hr>
				<a href="logout.php" class="btn btn-block btn-primary">Yes</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-default">No</a>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" role="dialog" id="add_visit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Add Visitors</div>
			<div class="modal-body">

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">FullName</span>
						<input type="text" name="name" id="name" class="text-capitalize form-control" placeholder="Enter visitor's name...">
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Agency</span>
						<input type="text" name="agency" id="agency" class="text-capitalize form-control" placeholder="Enter visitor's agency...">
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Contact#</span>
						<input type="text" name="contact" maxlength="10" onkeypress="return num_only(event);" id="contact" class="text-capitalize form-control" placeholder="Enter contact number...">
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">E-mail</span>
						<input type="text" name="email" id="email" class="form-control" placeholder="Enter e-mail...">
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Purpose</span>
						<textarea name="text" id="purpose" name="purpose" class="form-control" placeholder="Enter your purpose..."></textarea>
					</div>
				</div>
			
				<input type="hidden" name="d_visit" id="d_visit" value="<?php echo date('Y-m-d') ?>">


				<button class="btn btn-primary btn-block" id="btn_visit" onclick="save_visit();">Save</button>
				<button class="btn btn-danger btn-block" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	function attach_visitor_image(img,name,id){

		$("#fn").text(name);
		$("#mid").val(id);
		console.log(img);
		// alert(img);
		if (img != 'data:image/jpeg;base64,') 
		{
			$("#img_src").html('<img src="'+img+'" class="img-thumbnail animated jackInTheBox" width="220">');
		}
		else
		{
			$("#img_src").html('<h3 class="text-center w3-text-grey">Select Photo</h3>');

		}
		
		$("#show_visitor_pic").modal('show');
	}
</script>

<div class="modal fade" role="dialog" id="show_visitor_pic">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Profile Picture</div>
			<div class="modal-body text-center">
				<div id="img_src"></div>
			 <hr>
			 <h5 class="text-capitalize" id="fn"></h5>
			</div>
			
			<form  method="post"  enctype="multipart/form-data">
				<input type="hidden" name="mid" id="mid">
				<input type="file" name="image" id="image"  style="display: none;'">
				<div style="margin:20px 20px 5px 20px;">
					<input type="submit" id="btn_sub" name="btn_sub" style="display: none;" value="Update Picture" type="submit" class="btn btn-success btn-block">
				</div>
			</form>
			<div style="margin:0px 20px 20px 20px;">
				<button id="change_btn" class="btn btn-block btn-primary" onclick="$('#image').click();">Change Picture</button>
				<button onclick="close_change_pic();" data-dismiss="modal" class="btn btn-block btn-danger">Close</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#image").on('change',function(){
			var mypicfile = document.getElementById('image');
			var info = mypicfile.value;
			if (mypicfile.value == "") 
			{
				$("#btn_sub").hide('fast');
				$("#change_btn").show('fast');
			}
			else
			{
				$("#btn_sub").show('fast');
				$("#change_btn").hide('fast');
				//alert(info);
			}
		});
	});
</script>


<script type="text/javascript">
	function close_change_pic(){
		$("#btn_sub").hide('fast');
		$("#change_btn").show('fast');
		$("image").val('');
	}
</script>
<?php 
	if (isset($_POST['btn_sub'])) {
		include_once("../config.php");
		$imageName=mysqli_real_escape_string($con,$_FILES["image"]["name"]);
		$imageData=mysqli_real_escape_string($con,file_get_contents($_FILES["image"]["tmp_name"]));
		$imageType=mysqli_real_escape_string($con,$_FILES["image"]["type"]);
		$mid = mysqli_real_escape_string($con,$_POST['mid']);
		$date=Date("l F d, Y h:i:sa");
		?>
		<?php
		if(substr($imageType,0,5) == "image")
		{
			$update = mysqli_query($con,"UPDATE tbl_visitors set image='$imageData' where transid='$mid'");	
			if ($update) {
			?>
			<script type="text/javascript">
				alert("Visitor's Picture has been updated !");
				// window.location="index.php";
				load_now();
			</script>
			<?php
		}
		}	
		else{
			?>
			<script type="text/javascript">
				alert("Sorry Invalid image format or file is too large ! ");
			</script>
			<?php
		}
	}
 ?>
