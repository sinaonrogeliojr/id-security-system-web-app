<div class="modal fade" role="dialog" id="add_visit">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Add Visitors</div>
			<div class="modal-body">
					<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">FullName</span>
						<input type="text" name="name" id="name" class="text-capitalize form-control" placeholder="Enter visitor's name...">
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Agency</span>
						<input type="text" name="agency" id="agency" class="text-capitalize form-control" placeholder="Enter visitor's agency...">
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Contact#</span>
						<input type="text" name="contact" maxlength="10" onkeypress="return num_only(event);" id="contact" class="text-capitalize form-control" placeholder="Enter contact number...">
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">E-mail</span>
						<input type="text" name="email" id="email" class="form-control" placeholder="Enter e-mail...">
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Purpose</span>
						<textarea name="text" id="purpose" name="purpose" class="form-control" placeholder="Enter your purpose..."></textarea>
					</div>
				</div>

				<input type="hidden" name="d_visit" id="d_visit" value="<?php echo date('Y-m-d') ?>">

				<button class="btn btn-primary btn-block" id="btn_visit" onclick="save_visit();">Save</button>
				<button class="btn btn-danger btn-block" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" role="dialog" id="logout_me">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Logout</div>
			<div class="modal-body">
				<h5>Do you want to logout your account ?</h5>
				<hr>
				<a href="logout.php" class="btn btn-block btn-primary">Yes</a>
				<a href="#cancel" data-dismiss="modal" class="btn btn-block btn-default">No</a>
			</div>
		</div>
	</div>
</div>
<!--separator -->
<div class="modal fade" role="dialog" id="show_mypic">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Profile Picture</div>
			<div class="modal-body text-center">
				<?php 
				echo '<img src="data:image/jpeg;base64,'.base64_encode($_SESSION['pr_picture']).'" class="img-thumbnail animated jackInTheBox" width="220"/>';
			 ?>
			 <hr>
			 <h5><?php echo $_SESSION['pr_fullname'] ?></h5>
			 <h5><?php echo $_SESSION['pr_post'] ?></h5>
			</div>
			
			<form  method="post"  enctype="multipart/form-data">
				<input type="file" name="image" id="image"  style="display: none;'">
				<div style="margin:20px 20px 5px 20px;">
					<input type="submit" id="btn_sub" name="btn_sub" style="display: none;" value="Update Picture" type="submit" class="btn btn-success btn-block">
				</div>
			</form>
			<div style="margin:0px 20px 20px 20px;">
				<button id="change_btn" class="btn btn-block btn-primary" onclick="$('#image').click();">Change Picture</button>
				<button onclick="close_change_pic();" data-dismiss="modal" class="btn btn-block btn-danger">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- UPLOAD SCHEDULE -->
<!--separator -->
<div class="modal fade" role="dialog" id="upload_schedule">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" id="m-header">UPLOAD SCHEDULE</div>
			<div class="modal-body text-center">
				<div class="form">
					<form method="post" id="export_excel">  
	                     <input type="file" name="excel_file2" id="excel_file2" style="display: none;"/>
	                     <button type="button" class="btn btn-default btn-small btn-block" id="btn-select" onclick="$('#excel_file2').click();"><span class="fa fa-file"></span> Select File</button>  
                	</form>    
                	<label id="loader"></label>
	                <div id="result">  
	                </div>  
				</div>
			</div>
			
			<div style="margin:0px 20px 20px 20px;">
				<button onclick="close_change_pic();" data-dismiss="modal" class="btn btn-block btn-danger">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- UPLOAD SCHEDULE -->

<!-- UPDATE SCHEDULE -->
<div class="modal fade" role="dialog" id="manage_schedule">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header" id="s-header">Add Schedule</div>

			<div class="modal-body">
				<input type="hidden" id="sched_id">
				<div class="form-group">
					<label>Subject Code</label>
					<input type="text" name="subject_code" id="subject_code" class="text-capitalize form-control" placeholder="Enter Subject Code...">
				</div>
				<div class="form-group">
					<label>Subject Description</label>
					<input type="text" name="subject_desc" id="subject_desc" class="text-capitalize form-control" placeholder="Enter Subject Description...">
				</div>

				<div class="form-group">
				  <label for="day_">Select Day: </label>
				  <select class="form-control" id="day_">
				    <option value="Monday">Monday</option>
				    <option value="Tuesday">Tuesday</option>
				    <option value="Wednesday">Wednesday</option>
				    <option value="Thursday">Thursday</option>
				    <option value="Friday">Friday</option>
				    <option value="Saturday">Saturday</option>
				  </select>
				</div>

				<div class="form-group">
						<label>Time Schedule:</label>
						<input type="text" name="time_" id="time_" class="text-capitalize form-control" placeholder="Enter Schedule">
				</div>

				<button class="btn btn-primary btn-block" id="btn_visit" onclick="manage_sched();">Save</button>
				<button class="btn btn-danger btn-block" data-dismiss="modal">Cancel</button>

			</div>
		</div>
	</div>
</div>

<!-- UPDATE SCHEDULE -->
<div class="modal fade" role="dialog" id="manage_sms_schedule">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header" id="sms-header">Add SMS Schedule</div>

			<div class="modal-body">
				<input type="hidden" id="sms_id">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Message</span>
						<textarea  class="form-control responsive" id="message" name="message"></textarea>
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Date</span>
						<input type="date" name="date_" id="date_" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<select class="form-control" id="audience" name="audience">
						<option value="All">All</option>
						<option value="PTA Officers">PTA Officers</option>
						<option value="Teachers">Teachers</option>
						<option value="Students">Students</option>
					</select>
				</div>

				<button class="btn btn-primary btn-block" id="btn_visit" onclick="save_sms_setup();">Save</button>
				<button class="btn btn-danger btn-block" data-dismiss="modal">Cancel</button>

			</div>
		</div>
	</div>
</div>


<?php 
	if (isset($_POST['btn_sub'])) {
		include_once("../config.php");
		$myid = $_SESSION['pr_id'];
		$imageName=mysqli_real_escape_string($con,$_FILES["image"]["name"]);
		$imageData=mysqli_real_escape_string($con,file_get_contents($_FILES["image"]["tmp_name"]));
		$imageType=mysqli_real_escape_string($con,$_FILES["image"]["type"]);
		$date=Date("l F d, Y h:i:sa");
		?>
		<?php
		if(substr($imageType,0,5) == "image")
		{	

			$update = mysqli_query($con,"UPDATE tbl_staff_picture set image='$imageData' where staff_id='$myid'");	
			if ($update) {

			$stmt = mysqli_query($con,"SELECT a.*,b.* from tbl_staff a left join tbl_staff_picture b on a.staff_id = b.staff_id where b.staff_id='$myid'");
			$injec = mysqli_fetch_assoc($stmt);
			$_SESSION['pr_picture'] = $injec['image'];
			?>
			<script type="text/javascript">
				alert("Profile Picture has been updated !");
				window.location="index.php";
			</script>
			<?php
		}
		}	
		else{
			?>
			<script type="text/javascript">
				alert("Sorry Invalid image format or file is too large ! ");
			</script>
			<?php
		}
	}
 ?>

<script type="text/javascript">
	function close_change_pic(){
		$("#btn_sub").hide('fast');
		$("#change_btn").show('fast');
		$("image").val('');
	}
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#image").on('change',function(){
			var mypicfile = document.getElementById('image');
			var info = mypicfile.value;
			if (mypicfile.value == "") 
			{
				$("#btn_sub").hide('fast');
				$("#change_btn").show('fast');
			}
			else
			{
				$("#btn_sub").show('fast');
				$("#change_btn").hide('fast');
				//alert(info);
			}
		});
	});
</script>

<script type="text/javascript">

$(document).ready(function(){  

      $('#excel_file2').change(function(){  
           $('#export_excel').submit();  
      });  

      $('#export_excel').on('submit', function(event){  
           event.preventDefault();  
           $.ajax({  
                url:"export_sched.php",  
                method:"POST",  
                data:new FormData(this),  
                contentType:false,  
                processData:false,  
                beforeSend:function(){
                	$('#loader').html('Uploading File...');
                },
                success:function(data){  
                     $('#result').html(data);  
                     $('#excel_file2').val('');
                     $('#loader').html('');
                     showToast.show("Schedule Successfully Saved",1000);
                     setTimeout(function()
                     	{  $('#upload_schedule').modal('hide'); 
                     	display_schedules();
                     },2000);
                }  
           });  
      });  
 });  

 
 $(document).ready(function() {
  $("#manage_schedule").on("hidden.bs.modal", function() {

    $('#sched_id').val('');
	$('#subject_code').val('');
	$('#subject_desc').val('');
	$('#day_').val('Monday');
	$('#time_').val('');
	$('#school_year').val('');

  });
});

</script>