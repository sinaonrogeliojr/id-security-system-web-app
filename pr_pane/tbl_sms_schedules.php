<?php 
session_start();
include_once("../config.php");

$start = $_POST['start'];
$limit = $_POST['limit'];

$sqls = mysqli_query($con,"SELECT count(id) from tbl_sms_schedule");
$get_max = mysqli_fetch_assoc($sqls);
		
	$q = mysqli_query($con, "SELECT * from tbl_sms_schedule");

	if(mysqli_num_rows($q) > 0){

		while ($d = mysqli_fetch_assoc($q)) { ?>
			
			<div class="panel panel-info w3-card-2">
				<div class="panel-heading"> Concern: <?php echo $d['category']; ?>
					<span class="dropdown pull-right">
					<button class="close dropdown-toggle" data-toggle="dropdown"><i class="fa fa-chevron-down "></i></button>
						<ul class="dropdown-menu">
							<li><a href="#" class="" onclick="edit_sms_schedule('<?php echo $d['id'] ?>','<?php echo $d['message'] ?>','<?php echo $d['date_sent'] ?>','<?php echo $d['category'] ?>')"><i class="fa fa-edit"></i> Edit</a></li>
							<li class="divider"></li>
							<li><a href="#" class="" onclick="delete_sms_sched('<?php echo $d['id'] ?>');"><i class="fa fa-trash" ></i> Delete</a></li>
						</ul>
					</span>
				</div>
				<div class="panel-body">
					<div class="row">
									
									<div class="col-sm-8" style="background-color: #337ab7; color: #fff;">

										<h3><b><?php echo $d['message']; ?></b></h3>
									</div>
									<div class="col-sm-4">
										<h4>
											<b>
												Date: <?php echo $d['date_sent']?>
											</b>
										</h4>
										<h4>
											<b>
												Status: <?php 

													if($d['status'] == 1){ ?>

														<span class="label label-success">SENT</span>
													
													<?php }elseif ($d['status'] == 0) { ?>
														<span class="label label-warning">PENDING</span>
													<?php }

												?>
											</b>
										</h4>

									</div>

								</div>
								
				</div>
				<div class="panel-footer">
					<!-- <div class="text-right w3-text-dark-grey"><small><?php echo $mdate ?></small></div> -->
				</div>
			</div>

			<?php }
	}		

		if ($limit >= $get_max['count(id)']) 
		{ ?>
			<button class="btn btn-block w3-gray btn-lg">End of records...</button>
			<br>
			<?php
		}
		else
		{
		?>
			<button class="btn btn-block btn-info w3-card-2 btn-lg" id="btn_gl" onclick="load_more(); $(this).text('Loading...');">Load more</button> <br>
		<?php
		}
			

?>

<script type="text/javascript">

	function load_more(){
	var start = document.getElementById('start');
	var limit = document.getElementById('limit');
	var b;
	b = Number(limit.value) + 10; 
	$("#limit").val(b);
	setTimeout(function(){
	show_sections();
	},500);
	}

</script>

<script type="text/javascript">
	function visit_teacher(id){
		var tid = id;
		var info = 'tid=' + tid;
		$.ajax({
			type:"POST",
			url:"get_session_from_teacher.php",
			data:info,
			cache:false,
			success:function(data){
				//alert(data);
				if (data == 1) 
				{
					window.location="../teacher";
				}
				else
				{
					alert(data);
				}
			}
		});
	}

	function edit_sms_schedule(id,message,date_sent,audience){
		$('#sms_id').val(id);
		$('#message').val(message);
		$('#date_').val(date_sent);
		$('#audience').val(audience);
		$('#manage_sms_schedule').modal('show');
		$('#sms-header').html('Update SMS Schedule');

	}

	function delete_sms_sched(id){

	var mydata = 'id=' + id;

	swal({
         title: "Are you sure ?",
         text: "You want to delete this sms schedule",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:"delete_sms_sched.php",
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
                 display_schedules();
              	 showToast.show("Schedule has been removed!",1000);
                }
                else
                {
                    alert(data);
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });

	}

</script>