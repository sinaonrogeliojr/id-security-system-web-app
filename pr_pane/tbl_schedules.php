<?php 
session_start();
include_once("../config.php");
$section = mysqli_real_escape_string($con,$_POST['section']);
$day_ = mysqli_real_escape_string($con,$_POST['day_2']);

$start = $_POST['start'];
$limit = $_POST['limit'];

$sqls = mysqli_query($con,"SELECT count(ID) from tbl_schedule where section = '$section'");
$get_max = mysqli_fetch_assoc($sqls);
	
	if($day_ === "ALL"){
		$days = mysqli_query($con, "SELECT * from tbl_days");
	}else{
		$days = mysqli_query($con, "SELECT * from tbl_days where days = '$day_'");
	}

	if(mysqli_num_rows($days) > 0){

		while ($d = mysqli_fetch_assoc($days)) { ?>
			
			<div class="panel panel-info w3-card-2">

				<div class="panel-heading" style="border-bottom: 2px solid #337ab7;">
					<b><?php echo $d['days'] ?> SCHEDULE</b>
				</div>

				<div class="panel-body"> 

				<?php	

				$days_ = $d['days'];

				$sql = mysqli_query($con, "SELECT * from tbl_schedule where section = '$section' and day_ = '$days_' order by id ASC limit ".$_POST['start'].", ".$_POST['limit']." ");

					if (mysqli_num_rows($sql)>0) {

						while ($row = mysqli_fetch_assoc($sql)) { ?>
							
							<!-- <div class="well w3-card-2 w3-hover-teal">
								
								<div class="row">
									
									<div class="col-sm-12">
										<span class="dropdown pull-right">
										<button class="close dropdown-toggle" data-toggle="dropdown"><i class="fa fa-chevron-down "></i></button>
										<ul class="dropdown-menu">
											<li><a class="" onclick="edit_schedule('<?php echo $row['id'] ?>','<?php echo $row['subject_code'] ?>','<?php echo $row['subject_desc'] ?>','<?php echo $row['day_'] ?>','<?php echo $row['time_'] ?>')"><i class="fa fa-edit"></i> Edit</a></li>
											<li class="divider"></li>
											<li><a class="" onclick="delete_schedule('<?php echo $row['id'] ?>','<?php echo $row['subject_code'] ?>','<?php echo $row['subject_desc'] ?>')"><i class="fa fa-trash" ></i> Delete</a></li>
										</ul>
									</span>
										<h5 class="text-capitalize">Subject: <b><?php echo $row['subject_code'] . " - " . $row['subject_desc']; ?></b></h5>
										<h5 class="text-capitalize" >Schedule: <b><?php echo $row['time_']; ?></b></h5>
									</div>
								</div>
							</div> -->

							<div class="well w3-card-2 w3-hover-white" style="margin-top: -5px; margin-bottom: 10px;">
								
								<div class="row">
									
									<div class="col-sm-4" style="background-color: #337ab7; color: #fff;">

										<h3><b><?php echo $row['time_']; ?></b></h3>
									</div>
									<div class="col-sm-7">
										<h3>
											<b>
												<?php echo $row['subject_code']?>
											</b>
										</h3>
										<h4>
											<?php echo $row['subject_desc']?>
										</h4>

									</div>
									<div class="col-sm-1 pull-right">
										<span class="dropdown pull-right">
											<button class="close dropdown-toggle" data-toggle="dropdown"><i class="fa fa-chevron-down "></i></button>
											<ul class="dropdown-menu">
												<li><a class="" onclick="edit_schedule('<?php echo $row['id'] ?>','<?php echo $row['subject_code'] ?>','<?php echo $row['subject_desc'] ?>','<?php echo $row['day_'] ?>','<?php echo $row['time_'] ?>')"><i class="fa fa-edit"></i> Edit</a></li>
												<li class="divider"></li>
												<li><a class="" onclick="delete_schedule('<?php echo $row['id'] ?>','<?php echo $row['subject_code'] ?>','<?php echo $row['subject_desc'] ?>')"><i class="fa fa-trash" ></i> Delete</a></li>
											</ul>
										</span>
									</div>

									<!-- <div class="col-sm-12">
										<h5 class="text-capitalize">Subject: <b><?php echo $row['subject_code'] . " - " . $row['subject_desc']; ?></b></h5>
										<h5 class="text-capitalize">Schedule: <b><?php echo $row['time_']; ?></b></h5>
									</div> -->
								</div>
								
							</div>

						<?php
						}

					}
					else
					{
						echo '<div class="well">No records found...</div>';
					} ?>

				</div>

			</div>

		<?php

		}

		if ($limit >= $get_max['count(ID)']) 
		{ ?>
			<button class="btn btn-block w3-gray btn-lg">End of records...</button>
			<br>
			<?php
		}
		else
		{
		?>
			<button class="btn btn-block btn-info w3-card-2 btn-lg" id="btn_gl" onclick="load_more(); $(this).text('Loading...');">Load more</button> <br>
		<?php
		}
	}		

?>

<script type="text/javascript">

	function load_more(){
	var start = document.getElementById('start');
	var limit = document.getElementById('limit');
	var b;
	b = Number(limit.value) + 10; 
	$("#limit").val(b);
	setTimeout(function(){
	show_sections();
	},500);
	}

</script>

<script type="text/javascript">
	function visit_teacher(id){
		var tid = id;
		var info = 'tid=' + tid;
		$.ajax({
			type:"POST",
			url:"get_session_from_teacher.php",
			data:info,
			cache:false,
			success:function(data){
				//alert(data);
				if (data == 1) 
				{
					window.location="../teacher";
				}
				else
				{
					alert(data);
				}
			}
		});
	}

	function edit_schedule(id,subject_code,subject_desc,day,time_){
		$('#sched_id').val(id);
		$('#subject_code').val(subject_code);
		$('#subject_desc').val(subject_desc);
		$('#day_').val(day);
		$('#time_').val(time_);
		$('#manage_schedule').modal('show');
		$('#s-header').html('Update Schedule');

	}

	function delete_schedule(id,subject_code,subject_desc){

	var mydata = 'id=' + id;

	swal({
         title: "Are you sure ?",
         text: "You want to delete this schedule for " + subject_code + ' - ' + subject_desc,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:"delete_sched.php",
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == 1) 
                {
                 show_schedules();
              	 showToast.show("Schedule has been removed!",1000);
                }
                else
                {
                    alert(data);
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });

	}

</script>