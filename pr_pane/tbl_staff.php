<?php 
session_start();
include_once("../config.php");


if (isset($_POST['search'])) {
	$src = $_POST['search'];

	$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_staff a left join tbl_staff_picture b on a.staff_id = b.staff_id where concat(a.lastname,', ',a.firstname,' ',a.mi) like '%$src%' order by a.lastname,a.firstname,a.mi asc LIMIT 0,10");
if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
						$popups = '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="190" class="img-thumbnail"/><hr><p class="text-center">'.$row['lastname'].', '.$row['firstname'].' '.$row['mi'].'</p><p class="text-center">'.$row['position'].'</p>';
					?>
					<script type="text/javascript">
						$('[data-toggle="popover"]').popover(); 
					</script>
					<div class="well w3-card w3-hover-shadow" data-toggle="popover" data-placement="auto" data-trigger="hover" data-content='<?php echo $popups; ?>' data-html="true">
					<h5 class="w3-small" ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
						if ($str != null) {
							echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						}
						else
						{
							echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
						}
					?>
						<button class="close" style="margin-top:8px;" onclick="visit_teacher('<?php echo $row['staff_id'] ?>')"><i class="fa fa-chevron-right"></i></button>
					</h5>
					</div>
					<?php
					}
				}
				else
				{
					echo '<div class="well">No records found...</div>';
				}	
}
else
{
$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_staff a left join tbl_staff_picture b on a.staff_id = b.staff_id order by a.lastname,a.firstname,a.mi asc");
if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
						$popups = '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="190" class="img-thumbnail"/><hr><p class="text-center">'.$row['lastname'].', '.$row['firstname'].' '.$row['mi'].'</p><p class="text-center">'.$row['position'].'</p>';
					?>
					<script type="text/javascript">
						$('[data-toggle="popover"]').popover(); 
					</script>
					<div class="well w3-card w3-hover-shadow" data-toggle="popover" data-placement="auto" data-trigger="hover" data-content='<?php echo $popups; ?>' data-html="true">
					<h5 class="w3-small" ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
						if ($str != null) {
							echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						}
						else
						{
							echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
						}
						?>
						<button class="close" style="margin-top:8px;" onclick="visit_teacher('<?php echo $row['staff_id'] ?>')"><i class="fa fa-chevron-right"></i></button>
					</h5>
					</div>
					<?php
					}
				}
				else
				{
					echo '<div class="well">No records found...</div>';
				}		
}


 ?>

<script type="text/javascript">
	function visit_teacher(id){
		var tid = id;
		var info = 'tid=' + tid;
		$.ajax({
			type:"POST",
			url:"get_session_from_staff.php",
			data:info,
			cache:false,
			success:function(data){
				//alert(data);
				if (data == 1) 
				{
					window.location="../staff";
				}
				else
				{
					alert(data);
				}
			}
		});
	}
</script>