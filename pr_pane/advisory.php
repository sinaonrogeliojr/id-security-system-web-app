<?php
session_start();
if (!isset($_SESSION['pr_id'])) {
	@header('location:../');	
}
 include_once('../config.php');?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['pr_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<script type="text/javascript">

		function get_up_list(){
		var start,limit;
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		var	 mydata = 'start=' + start.value + '&limit=' + limit.value;

		$.ajax({
			type:"POST",
			url:"my_student.php",
			data:mydata,
			cache:false,
			success:function(data){
				$("#src_mystudent").html(data);
			}
		});
		}


		function get_up_load(){
		var start,limit;
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		var	 mydata = 'start=' + start.value + '&limit=' + limit.value;

		$.ajax({
			type:"POST",
			url:"my_student.php",
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#src_mystudent").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#src_mystudent").html(data);
			}
		});
		}


	function get_up_list_src(){
		$("#src_mystudent").hide('fast');
		$("#srch_mystudent").show('fast');
		var	me = document.getElementById('src');
		var myinfo = 'myid=' + me.value;
		$.ajax({
			type:"POST",
			url:"my_student.php",
			data:myinfo,
			cache:false,
			beforeSend:function(){
				$("#srch_mystudent").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#srch_mystudent").html(data);
			}
		});
		
	}
	</script>


<script type="text/javascript">
	function input_stat(){
		var	me = document.getElementById('src');
		if (me.value == "") {
			$("#src_mystudent").show('fast');
			$("#srch_mystudent").hide('fast');
			$("#stud_src").hide('fast');
			$("#stud_clear").hide('fast');
			$("#stud_label").show('fast');
			
		}
		else
		{
			$("#stud_clear").show('fast');
			$("#stud_label").hide('fast');
			$("#stud_src").show('fast');
		}
	}

	function clear_src(){
			$("#src_mystudent").show('fast');
			$("#srch_mystudent").hide('fast');
			$("#stud_src").hide('fast');
			$("#stud_clear").hide('fast');
			$("#stud_label").show('fast');
			$("#src").val('');
	}
</script>


<input type="hidden" name="start" id="start" value="0">
<input type="hidden" name="limit" id="limit" value="10">	
<body  onload=" get_up_load();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
		<div>
			<div class="">
				<a href="index.php" class="btn btn-primary btn-block">Back to Home</a>
			<hr>
		
			<div class="">
				<div class="input-group">
					<span class="input-group-addon" ><i class="fa fa-search" id="stud_label"></i> <i class="fa fa-remove" style="display: none;" id="stud_clear" onclick="clear_src();"> </i></span>
					<input type="text" name="src" id="src" class="form-control" oninput="input_stat();" placeholder="Search...">
					<span class="input-group-addon w3-blue" onclick="get_up_list_src();"><span id="stud_src" style="display: none;">Search</span></span>
				</div>
				<br>
				<div id="src_mystudent"></div>
				<div id="srch_mystudent"></div>
				
			</div>
		</div>
		</div>
		<div class="col-sm-3"></div>
	</div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>

</html>

<?php include_once('modals.php'); ?>