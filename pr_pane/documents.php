<?php
session_start();
if (!isset($_SESSION['pr_id'])) {
	@header('location:../');	
}
 include_once('../config.php');?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title><?php echo $_SESSION['pr_fullname'] ?></title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="icon" type="icon/png" href="../img/ssjalogo2">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="../css/showToast.css">
	<style type="text/css">
		.img_size{
			width: 250px;
			height: 250px;
			object-fit: cover;
		}
		.img_size_150{
			width: 150px;
			height: 150px;
			object-fit: cover;
		}
		.none{
			display: none;
		}
	</style>
</head>


<body onload="display_documents();">
<?php include_once("nav.php"); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
			<div class="col-sm-6">
				<a href="index.php" class="btn btn-primary btn-block">Home</a>
				<h4>DOCUMENTS</h4>
				<hr>

				<p class="statusMsg"></p>
				
				<input type="hidden" name="el" id="el">
				<input type="hidden" name="start" id="start" value="0">
				<input type="hidden" name="limit" id="limit" value="10">
				<form enctype="multipart/form-data" id="fupForm" >

				    <div class="form-group">

						<input type="hidden" name="doc_id" id="doc_id">
						<input type="hidden" name="doc_type" id="doc_type">
						<input type="hidden" id="f_name" name="f_name">
						<div class="input-group">
							<span class="input-group-addon">Document Name:</span>
							<input type="text" class="form-control" id="txt_doc_name" name="txt_doc_name" placeholder="Document Name ..." />
						</div>

					</div>

					<div class="form-group">
						<select class="form-control" id="txt_category" name="txt_category" >
							<option selected="" value="ALL">All</option>
							<option value="STUDENTS">Students</option>
							<option value="TEACHERS">Teachers</option>
							<option value="STAFF">Staff</option>
						</select>
					</div>

					<div class="col-sm-12" id="file_update">

					</div>
				    
				    <div class="form-group">
				        <input type="file" class="form-control" id="file" name="file" style="display: none;"/>
				        <button type="button" class="btn btn-danger btn-small none" id="btn_remove_file" onclick="remove_file();"><span class="fa fa-trash"></span> Remove Document/Picture</button>
				        <button type="button" class="btn btn-dark btn-small" id="btn-select" onclick="$('#file').click();"><span class="fa fa-upload"></span> Upload Document/Picture</button>
				    </div>

					<div class="text-center" id="preview_file"></div>

				    <input type="submit" name="submit" id="btn_ann" class="btn btn-info btn-block submitBtn" value="SAVE"/>

				</form>
				<button style="display: none;" class="btn btn-danger btn-block" onclick="clear_update();" id="btn_cupdate">Cancel</button>
				<hr>
			<div id="load_pend"></div>
			<div id="load_documents"></div>
			<!-- <div class="well">No Records found...</div> -->
			</div>
		<div class="col-sm-3"></div>
	</div>

</div>

<div class="modal fade" role="dialog" id="doc_data">
	<div class="modal-dialog">
		<div class="modal-content" id="load_doc_data">
			
	</div>
</div>

</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>
<script src="../js/showToast.js"></script>

</html>

<script type="text/javascript">

	function view_doc_data(id){
		var mydata = 'id=' + id;
		$.ajax({
			type:"POST",
			url:"load_doc_data.php",
			data:mydata,
			cache:false,
			success:function(data){
				$('#doc_data').modal('show');
				$("#load_doc_data").html(data);
			}
		});
	}

	function remove_file(){
		var file_name = $('#f_name').val();
		var mydata = 'f_name=' + file_name;
		swal({
	     title: "Are you sure ?",
	     text: "You want to delete this post ?",
	    type: "warning",
	    showCancelButton: true,
	    confirmButtonColor: "#DD6B55",
	    confirmButtonText: "Yes, delete",
	    closeOnConfirm: true
	},
	function(isConfirm){
	  if (isConfirm) 
	  {
	    $.ajax({
	        type:"POST",
	        url:"delete_temp_file.php",
	        data:mydata,
	        cache:false,
	        success:function(data){
	        	// alert(data);
	            if (data == "") 
	            {
	          	 showToast.show("File has been removed!",1000);
	          	  $('#file_update').html("");
	          	  $('#btn_remove_file').addClass('none');
	           	 // 
	           	 //$('#preview_file').addClass('animated bounceOut');
	           	 //$('#preview_file').html("");
	           	
	            }
	            else
	            {
	                alert(data);
	            }
	        }
	    });
	     // 
	  } 
	  else 
	  {

	  }
	});
	}

</script>

<script type="text/javascript">
	function display_documents(){
		var start = document.getElementById('start');
		var limit = document.getElementById('limit');

		var mydata = 'start=' + start.value + '&limit=' + limit.value;
		$.ajax({
			type:"POST",
			url:"display_documents.php",
			data:mydata,
			cache:false,
			success:function(data){
				$("#load_documents").html(data);
			}
		});
	}
</script>

<script>

	$(document).ready(function(e){

	    $("#fupForm").on('submit', function(e){
	        e.preventDefault();
	        var a = $("#el").val();
	        $.ajax({
	            type: 'POST',
	            url: 'upload_file.php',
	            data: new FormData(this),
	            contentType: false,
	            cache: false,
	            processData:false,
	            beforeSend: function(){
	                $('.submitBtn').attr("disabled","disabled");
	                if (doc_id.value == "")
					{
						$("#load_pend").prepend('<div class="well w3-blue">Loading...</div>');
					}
					else
					{
						$("#"+a+"").html('<div class="well w3-blue">Updating...</div>');
					}
	            },
	            success: function(msg){
	                $('.statusMsg').html('');
	                if(msg == 1){
	                    $('#fupForm')[0].reset();
	                    $('.statusMsg').html('<span style="font-size:18px;color:#34A853">Form data submitted successfully.</span>');

	                    setTimeout(function(){   
					        $('.statusMsg').html('');
					        showToast.show('New File Succefully Uploaded!',3000);
					    },2000);
	                    display_documents();
						$("#load_pend").html('');
						$("#preview_file").html('');
	                }else if(msg == 2){
	                	$('#fupForm')[0].reset();
	                    $('.statusMsg').html('<span style="font-size:18px;color:#34A853">Form data submitted successfully.</span>');

	                    setTimeout(function(){   
					        $('.statusMsg').html('');
					        showToast.show('File Succefully Updated!',3000);
					    },2000);
	                    display_documents();
						$("#load_pend").html('');
						$("#preview_file").html('');
						$('#btn_cupdate').css('display','none');
	                }
	                else{
	                    $('.statusMsg').html('<span style="font-size:18px;color:#EA4335">Some problem occurred, please try again.</span>');
	                }
	                $('#fupForm').css("opacity","");
	                $(".submitBtn").removeAttr("disabled");
	            }
	        });
	    });
	    
	    //file type validation
	    $("#file").change(function() {
	        var file = this.files[0];
	        var imagefile = file.type;
	        var form_data = new FormData();
	        //alert(imagefile);
	        $('#doc_type').val(imagefile.trim(""));
	        var match= ["application/pdf","application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword","image/jpeg","image/png","image/jpg", "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.ms-powerpoint", "text/plain"];
	        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]) || (imagefile==match[3]) || (imagefile==match[4]) || (imagefile==match[5]) || (imagefile==match[6]) || (imagefile==match[7]) || (imagefile==match[8]) || (imagefile==match[9]) || (imagefile==match[10]))){
	            alert('INVALID FILE!');
	            $("#file").val('');
	            return false;
	        }else{


				form_data.append("file", document.getElementById('file').files[0]);

	        	$.ajax({
			      url:"upload_file_temp.php",
			      method:"POST",
			      data: form_data,
			      contentType: false,
			      cache: false,
			      processData: false,
			      beforeSend:function(data){
			        $('#preview_file').html("<label class='text-success col-sm-12'> Uploading File... <span class='fa fa-spinner fa-spin'></span></label>");      
			      },   
			      success:function(data)
			      {
			       if(data == 404){

					swal("Warning", "Song Already Exists.", "warning");
					$('#preview_file').html("<label class='text-danger col-sm-12'>File Uploading Failed... <span class='fa fa-spinner fa-spin'></span></label>");  

			       }else{
			       	  $('#btn_remove_file').addClass('none');
			       	  $('#file_update').html('');
			          $('#preview_file').html(data);
			          var file_name = $('#file_name').val();
			          $('#f_name').val(file_name);

			       }
			      
			      }
			      });


	        }
	    });
	});
</script>

<script type="text/javascript">
function clear_update(){
	var a = $("#el").val();
	$("#"+a+"").show('fast');
	$("#txt_category").val('');
	$("#txt_doc_name").val('');
	$("#doc_id").val('');
	$("#btn_cupdate").hide('fast');
	$("#btn_ann").text('Post');
	$("#doc_type").val("");
	$('#file_update').html('');
	$('#btn_remove_file').addClass('none');
}
</script>

<script type="text/javascript">
	function del_temp_file(file_name){

	var mydata = 'file_name=' + file_name;
    
    swal({
         title: "Are you sure ?",
         text: "You want to delete this post ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete",
        closeOnConfirm: true
    },
    function(isConfirm){
      if (isConfirm) 
      {
        $.ajax({
            type:"POST",
            url:"delete_temp_file.php",
            data:mydata,
            cache:false,
            success:function(data){
            	// alert(data);
                if (data == "") 
                {
              	 showToast.show("File has been removed!",1000);
              	  $('#fupForm')[0].reset();
              	  $('#preview_file').html("");
               	 // 
               	 //$('#preview_file').addClass('animated bounceOut');
               	 //$('#preview_file').html("");
               	
                }
                else
                {
                    alert(data);
                }
            }
        });
         // 
      } 
      else 
      {

      }
    });
	}
</script>