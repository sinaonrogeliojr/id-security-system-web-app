<?php 
session_start();
include_once("../config.php");
$grade_id = mysqli_real_escape_string($con,$_POST['grade_id']);
if (isset($_POST['search'])) {
	
	$src = mysqli_real_escape_string($con,$_POST['search']);
	
	$sql = mysqli_query($con, "SELECT * from tbl_section where grade_id = '$grade_id' and section like '%$src%' order by ID ASC LIMIT 0,10");

		if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) 
			{ ?>

			<div class="well w3-card w3-hover-shadow" >
				<!-- <p class="w3-small"><button class="btn btn-sm" onclick="view_modal_sched('<?php //echo $row['ID']; ?>','<?php //echo $row['section']; ?>')"><span class="fa fa-upload"></span> Upload Schedule</button> </p> -->
				<form method="get" action="schedules.php">  
					<p class="w3-large"><?php echo $row['section']; ?></p>	
					<input type="text" style="display: none" id="section" name="section" value="<?php echo $row['section']; ?>" autocomplete="off"/>
		            <button  class="close" id="btn_view_grades" style="margin-top: -27px;" type="submit"><i class="fa fa-chevron-right"></i></button>
				</form>
			</div>

			<?php
			}
		}
		else
		{
			echo '<div class="well">No records found...</div>';
		}	
}
else
{

$start = $_POST['start'];
$limit = $_POST['limit'];

$sqls = mysqli_query($con,"SELECT count(ID) from tbl_section where grade_id = '$grade_id'");
$get_max = mysqli_fetch_assoc($sqls);

	$sql = mysqli_query($con, "SELECT * from tbl_section where grade_id = '$grade_id' order by ID ASC limit ".$_POST['start'].", ".$_POST['limit']." ");

	if (mysqli_num_rows($sql)>0) {
		while ($row = mysqli_fetch_assoc($sql)) { ?>
			<div class="well w3-card w3-hover-shadow" >
				<!-- <p class="w3-small"><button class="btn btn-sm" onclick="view_modal_sched('<?php //echo $row['ID']; ?>','<?php //echo $row['section']; ?>')"><span class="fa fa-upload"></span> Upload Schedule</button> </p> -->
				<form method="get" action="schedules.php">  
					<p class="w3-large"><?php echo $row['section']; ?></p>	
					<input type="text" style="display: none" id="section" name="section" value="<?php echo $row['section']; ?>" autocomplete="off"/>
					<input type="text" style="display: none" id="section_id" name="section_id" value="<?php echo $row['ID']; ?>" autocomplete="off"/>
		            <button  class="close" id="btn_view_grades" style="margin-top: -27px;" type="submit"><i class="fa fa-chevron-right"></i></button>
				</form>
			</div>

		<?php
		}

		if ($limit >= $get_max['count(ID)']) 
		{ ?>
			<button class="btn btn-block w3-light-grey  btn-lg">End of records..</button>
			<br>
			<?php
		}
		else
		{
		?>
			<button class="btn btn-block btn-info w3-card-2 btn-lg" id="btn_gl" onclick="load_more(); $(this).text('Loading...');">Load more</button> <br>
		<?php
		}

	}
	else
	{
		echo '<div class="well">No records found...</div>';
	}		
}

?>

<script type="text/javascript">

	function load_more(){
	var start = document.getElementById('start');
	var limit = document.getElementById('limit');
	var b;
	b = Number(limit.value) + 10; 
	$("#limit").val(b);
	setTimeout(function(){
	show_sections();
	},500);
	}

</script>

<script type="text/javascript">
	function visit_teacher(id){
		var tid = id;
		var info = 'tid=' + tid;
		$.ajax({
			type:"POST",
			url:"get_session_from_teacher.php",
			data:info,
			cache:false,
			success:function(data){
				//alert(data);
				if (data == 1) 
				{
					window.location="../teacher";
				}
				else
				{
					alert(data);
				}
			}
		});
	}

	

</script>