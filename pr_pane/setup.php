<?php 
session_start();
include_once("../config.php");
if (!isset($_SESSION['pr_id'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title>Ssja</title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="../css/showToast.css">
</head>
<script type="text/javascript">
	function mform_load(){
		$.ajax({
			type:"POST",
			url:"get_form.php",
			data:"",
			cache:false,
			beforeSend:function(){
				$("#my_form").html('<center><h3 class="animated pulse infinite">Intializing data....</h3></center>');
			},
			success:function(data){
				$("#my_form").html(data);
			}
		});
	}
</script>

<script type="text/javascript">
	function set_setup(){
		var time_in,early;
		time_in = document.getElementById('time_in');
		early = document.getElementById('early');
		mid = document.getElementById('mid');
		early_bird = document.getElementById('early_bird');

		if (time_in.value == "") {
			time_in.focus();
		}
		else if (early.value == "") {
			early.focus();
		}
		else
		{
			var mydata = 'time_in=' + time_in.value + '&early=' + early.value + '&mid=' + mid.value + '&early_bird=' + early_bird.value;
			// alert(mydata);
			$.ajax({
				type:"POST",
				url:"set_setup.php",
				data:mydata,
				cache:false,
				beforeSend:function(){
					$("#my_form").html('<center><h3 class="animated pulse infinite">Intializing data....</h3></center>');
				},
				success:function(data){
					if (data == 1 || data == 2) 
					{
						swal("Success","succesfuly save setup!","success");
						mform_load();
					}
					else
					{
						alert(data);
					}
				}
			});
		}
	}

	function save_sms_setup	(){
		
		var message,date_,audience,sms_id;

		message	= $('#message').val();
		date_	= $('#date_	').val();
		audience = $('#audience').val();
		sms_id = $('#sms_id').val();

		if (message == "") {
			message.focus();
		}
		else if (date_ == "") {
			date_.focus();
		}
		else
		{
			var mydata = 'message=' + message + '&date_=' + date_ + '&audience=' + audience + '&sms_id=' + sms_id;
			// alert(mydata);
			$.ajax({
				type:"POST",
				url:"save_sms_setup.php",
				data:mydata,
				cache:false,
				beforeSend:function(){
					//$("#p-view-2").html('<center><h3 class="animated pulse infinite">Intializing data....</h3></center>');
				},
				success:function(data){
					if (data == 1 || data == 2) 
					{
						swal("Success","succesfuly save setup!","success");
						display_schedules();	
						$('#manage_sms_schedule').modal('hide');
						$('#message').val('');
						$('sms_id').val('');
						$('date_').val('');
						$('#audience').val('ALL');
						//mform_load();
					}
					else
					{
						alert(data);
					}
				}

			});

		}

	}


	function show_schedules(){
		var start,limit,section;
		start = document.getElementById('start');
		limit = document.getElementById('limit');

		var mydata = 'start=' + start.value + '&limit=' + limit.value ;
		$.ajax({
			type:"POST",
			url:"tbl_sms_schedules.php",
			data:mydata,
			cache:false,
			success:function(data){
					$("#sms_schedules").html(data);
					$("#btn_gl").text('load more');
				
			}

		});
	}

	function display_schedules(){
		var start,limit,section;
		start = document.getElementById('start');
		limit = document.getElementById('limit');
		section = $('#section').val();
		var day_ = $('#day_2').val();
		var mydata = 'start=' + start.value + '&limit=' + limit.value;
		$.ajax({
			type:"POST",
			url:"tbl_sms_schedules.php",
			data:mydata,
			cache:false,
			beforeSend:function(){
				$("#sms_schedules").html('<center><img src="../img/flat.gif" width="50"></center>');
			},
			success:function(data){
				$("#sms_schedules").html(data);
			}

		});
	}

</script>
<body onload="mform_load(); display_schedules();">
<?php 
include_once("nav.php");
 ?>
 <input type="hidden" name="start" id="start" value="0">
<input type="hidden" name="limit" id="limit" value="10">
<div class="container-fluid">
	<div class="row">

		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<a href="index.php" class="btn btn-primary btn-block">Home</a><br>
			<div class="tab-menu">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li class="active">
                <a href="#p-view-1" role="tab" data-toggle="tab">School Setup</a>
              </li>
              <li>
                <a href="#p-view-2" role="tab" data-toggle="tab">SMS Setup</a>
              </li>
            </ul>
          </div>
			<hr>
          <div class="tab-content">
            <div class="tab-pane" id="p-view-1">
              <div class="tab-inner">
                <div id="my_form"></div>
                </div>
            </div>
            <div class="tab-pane active" id="p-view-2">
              <div class="tab-inner">
             
              		<button class="btn btn-sm btn-info" data-toggle="modal" data-target='#manage_sms_schedule'><span class="fa fa-plus"></span> Add</button>
    
              	<div id="sms_schedules">
              		
              	</div>
              	<!-- <div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Message</span>
						<textarea  class="form-control responsive" id="message" name="message"></textarea>
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">Date</span>
						<input type="date" name="date_" id="date_" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<select class="form-control" id="audience" name="audience">
						<option>All</option>
						<option>PTA Officers</option>
						<option>Teachers</option>
						<option>Students</option>
					</select>
				</div>

				<button class="btn btn-primary btn-block" onclick="save_sms_setup();">Save Changes</button>

				<a href="index.php" class="btn btn-danger btn-block">Cancel</a> -->

              </div>
            </div>
           </div>
			
						
		<div class="col-sm-3"></div>
	</div>
</div>

<script type="text/javascript">
		function num_only(evt){
		var charcode= (evt.which) ? evt.which : event.keycode
		if (charcode > 31 && (charcode < 48 ||  charcode >57))
			return false;
		return true;
	}
</script>

</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>
<script src="../js/login.js"></script>
<script src="../js/showToast.js"></script>
</html>

<?php include_once("modals.php"); ?>