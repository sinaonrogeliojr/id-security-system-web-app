<?php 
session_start();
include_once("../config.php");
 
$sql = mysqli_query($con,"SELECT * from tbl_attendance_base");

if (mysqli_num_rows($sql)>0) {
	$row = mysqli_fetch_assoc($sql);
	 $time_of = date_create($row['time_in']);
	?>
	<input type="hidden" name="mid" id="mid" value="<?php echo $row['transid'] ?>">
	<div class="form-group">
		<div class="input-group">
			<span class="input-group-addon">Late Time In</span>
			<input type="time" name="time_in" id="time_in" class="form-control" value="<?php echo $row['time_in']; ?>">
		</div>
	</div>
<hr>
	<div class="form-group">
		<div class="input-group">
			<span class="input-group-addon"> Early Birds Time In</span>
			<input type="time" name="early_bird"  value="<?php echo $row['early_birds'] ?>" id="early_bird" class="form-control" placeholder="Enter Limit">
		</div>
	</div>

	<div class="form-group">
		<div class="input-group">
			<span class="input-group-addon"> Early Birds Limit</span>
			<input type="text" name="early" onkeypress="return num_only(event);" value="<?php echo $row['attend_limit'] ?>" maxlength="2" id="early" class="form-control" placeholder="Enter Limit">
		</div>
	</div>

	<button class="btn btn-primary btn-block" onclick="set_setup();">Save</button>
	<a href="index.php" class="btn btn-danger btn-block">Cancel</a>
	<?php
}
else
{
	?>
	<input type="hidden" name="mid" id="mid" >
	<div class="form-group">
		<div class="input-group">
			<span class="input-group-addon">Late Time In</span>
			<input type="time" name="time_in" id="time_in" class="form-control" >
		</div>
	</div>
<hr>
	<div class="form-group">
		<div class="input-group">
			<span class="input-group-addon"> Early Birds Time In</span>
			<input type="time" name="early_bird" id="early_bird" class="form-control" placeholder="Enter Limit">
		</div>
	</div>

	<div class="form-group">
		<div class="input-group">
			<span class="input-group-addon"> Early Birds Limit</span>
			<input type="text" name="early" onkeypress="return num_only(event);" maxlength="2" id="early" class="form-control" placeholder="Enter Limit">
		</div>
	</div>

	<button class="btn btn-primary btn-block" onclick="set_setup();">Save Changes</button>

	<a href="index.php" class="btn btn-danger btn-block">Cancel</a>
	<?php
}

?>