<?php 
session_start();
include_once("../config.php");




if (isset($_POST['search'])) {
	$src = $_POST['search'];

	$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_teachers a left join tbl_teacher_picture b on a.teacher_id = b.teacher_id where concat(a.lastname,', ',a.firstname,' ',a.mi) like '%$src%' order by a.lastname,a.firstname,a.mi asc LIMIT 0,10");
if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
						$popups = '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="190" class="img-thumbnail"/><hr><p class="text-center">'.$row['lastname'].', '.$row['firstname'].' '.$row['mi'].'</p><p class="text-center">'.$row['grade'].', '.$row['advisory'].'</p>';
					?>
					<script type="text/javascript">
						$('[data-toggle="popover"]').popover(); 
					</script>
					<div class="well w3-card w3-hover-shadow" data-toggle="popover" data-placement="auto" data-trigger="hover" data-content='<?php echo $popups; ?>' data-html="true">
					<h5 class="w3-small" ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
						if ($str != null) {
							echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						}
						else
						{
							echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
						}
					?>
						<button class="close" style="margin-top:8px;" onclick="visit_teacher('<?php echo $row['teacher_id'] ?>')"><i class="fa fa-chevron-right"></i></button>
					</h5>
					</div>
					<?php
					}
				}
				else
				{
					echo '<div class="well">No records found...</div>';
				}	
}
else
{


$start = $_POST['start'];
$limit = $_POST['limit'];

$sqls = mysqli_query($con,"SELECT count(teacher_id) from tbl_teachers");
$get_max = mysqli_fetch_assoc($sqls);

$sql = mysqli_query($con,"SELECT a.*,b.* from tbl_teachers a left join tbl_teacher_picture b on a.teacher_id = b.teacher_id order by a.lastname,a.firstname,a.mi asc limit ".$_POST['start'].", ".$_POST['limit']." ");
if (mysqli_num_rows($sql)>0) {
			while ($row = mysqli_fetch_assoc($sql)) {
						$popups = '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="190" class="img-thumbnail"/><hr><p class="text-center">'.$row['lastname'].', '.$row['firstname'].' '.$row['mi'].'</p><p class="text-center">'.$row['grade'].', '.$row['advisory'].'</p>';
					?>
					<script type="text/javascript">
						$('[data-toggle="popover"]').popover(); 
					</script>
					<div class="well w3-card w3-hover-shadow" data-toggle="popover" data-placement="auto" data-trigger="hover" data-content='<?php echo $popups; ?>' data-html="true">
					<h5 class="w3-small" ><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($row['image']).'"  width="60" class="img-thumbnail"/>' ?> <?php $str = $row['mi'];  
						if ($str != null) {
							echo $row['lastname'].', '.$row['firstname'].' '.$str[0].'.';
						}
						else
						{
							echo $row['lastname'].', '.$row['firstname'].' '.$row['mi'];
						}
						?>
						<button class="close" style="margin-top:8px;" onclick="visit_teacher('<?php echo $row['teacher_id'] ?>')"><i class="fa fa-chevron-right"></i></button>
					</h5>
					</div>
					<?php
					}

					if ($limit >= $get_max['count(teacher_id)']) {
					?>
					<button class="btn btn-block w3-light-grey  btn-lg">End of records..</button>
					<br>
					<?php
					}
					else
					{
					?>
					<button class="btn btn-block btn-info w3-card-2 btn-lg" id="btn_load_tc" onclick="load_more(); $(this).text('Loading...');">Load more</button>
					<br>
					<?php
					}

				}
				else
				{
					echo '<div class="well">No records found...</div>';
				}		
}


 ?>


<script type="text/javascript">
	function load_more(){
	var start = document.getElementById('start');
	var limit = document.getElementById('limit');
	var b;
	b = Number(limit.value) + 10; 
	$("#limit").val(b);
	setTimeout(function(){
	show_teachers();
	},1000);
	}
</script>

<script type="text/javascript">
	function visit_teacher(id){
		var tid = id;
		var info = 'tid=' + tid;
		$.ajax({
			type:"POST",
			url:"get_session_from_teacher.php",
			data:info,
			cache:false,
			success:function(data){
				//alert(data);
				if (data == 1) 
				{
					window.location="../teacher";
				}
				else
				{
					alert(data);
				}
			}
		});
	}
</script>