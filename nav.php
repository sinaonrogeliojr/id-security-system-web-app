
<?php 
	
	include('config.php');

	$sql = mysqli_query($con, "SELECT * from tbl_company where companyid = 1");

	if (mysqli_num_rows($sql)>0) {

		$row = mysqli_fetch_assoc($sql);

		$company_name = $row['company_name1'];
		$address = $row['address'];

	}

?>

<nav class="navbar navbar-default w3-teal w3-card-2">
	<div class="container-fluid text-center">
		<a href="index.php" class="	text-sm"><h2 style="margin-bottom:0px; margin-top:5px;"><small class=" w3-text-white"><?php echo $company_name; ?></small></h2> 
			<h3 style="margin-top:0px;"><small class="w3-text-white"><?php echo $address; ?></small></h3></a>
	</div>
</nav>
