setInterval(function(){get_new_session();},2000);

function get_new_session(){
		$.ajax({
			type:"POST",
			url:"session_user.php",
			data:"",
			cache:false,
			success:function(data)
			{
				//alert(data);
			}
		});
	}

	function update_pg_set(){	
	var fn,mn,ln,gender,address,occupation,contact,email;
	fn = document.getElementById('pgfn');
	mn = document.getElementById('pgmn');
	ln = document.getElementById('pgln');
	address = document.getElementById('pgaddress');
	gender = document.getElementById('pggender');
	occupation = document.getElementById('pgoccupation');
	contact = document.getElementById('pgcontact');
	email = document.getElementById('pgemail');

	var lencon = contact.value;

	if (ln.value == "") 
	{
		ln.focus();
	}
	else if (fn.value == "") 
	{
		fn.focus();
	}
	else if (mn.value == "") 
	{
		mn.focus();
	}
	else if (gender.value == 'Choose your gender' || gender.value == "") 
	{
		gender.focus();
	}
	else if (address.value == "") 
	{
		address.focus();
	}
	else if (occupation.value == "") 
	{
		occupation.focus();
	}
	else if (contact.value == "") 
	{
		contact.focus();
	}
	else if (lencon.length < 10) 
	{
		swal("Oops","Contact number must be 10 digits!","error");
	}
	else if (email.value == "") 
	{
		email.focus();
	}
	else
	{
		var info = 'stat=' + $("#stat").val() + '&id=' + $("#pgid").val()+ '&fn=' + $("#pgfn").val()+ '&mn=' + $("#pgmn").val()+ '&ln=' + $("#pgln").val()+ '&gender=' + $("#pggender").val()+ '&address=' + $("#pgaddress").val()+ '&occupation=' + $("#pgoccupation").val()+ '&contact=' + $("#pgcontact").val()+ '&email=' + $("#pgemail").val();

	$.ajax({
		type:"POST",
		url:"update_pg.php",
		data:info,
		cache:false,
		beforeSend:function(){
			$("#btn_up_pg").text(' Please wait Loading...');
		},
		success:function(data){
			//alert(data);
			$("#btn_up_pg").text('Save Changes');
			if (data == 1 || data == 2) 
			{
				swal("Success","Record has been saved !","success");
				clear_pg();
				$("#info_edit_pg").modal('hide');
				show_guardians();
			}
			else if (data == 3) 
			{
				email.focus();
				swal("Oops","Please enter valid email address!","error");
			}
			else
			{
				alert(data);
			}
		}
	});
	}		
	}



	function clear_pg(){
		$("#stat").val('');
		$("#pgid").val('');
		$("#pgfn").val('');
		$("#pgmn").val('');
		$("#pgln").val('');
		$("#pggender").val('Choose your gender');
		$("#pgaddress").val('');
		$("#pgoccupation").val('');
		$("#pgcontact").val('');
		$("#pgemail").val('');
	}


	function save_update(){
		var myid,lname,mname,fname,age,gender,mycontact,address,grade_my,section_my;
		myid =document.getElementById('myid');
		lname =document.getElementById('lname');
		mname =document.getElementById('mname');
		fname =document.getElementById('fname');
		age =document.getElementById('myage');
		gender =document.getElementById('gender');
		mycontact =document.getElementById('mycontact');
		address =document.getElementById('address');
		grade_my =document.getElementById('grade_my');
		section_my =document.getElementById('section_my');
		if (lname.value == "") 
		{
			lname.focus();
		}
		else if (mname.value == "") 
		{
			mname.focus();
		}
		else if (fname.value == "") 
		{
			fname.focus();
		}
		else if (age.value == "") 
		{
			age.focus();
		}
		else if (gender.value =="") 
		{
			gender.focus();
		}
		else if (address.value == "") 
		{
			address.focus();
		}
		else if (mycontact.value == "") 
		{
			mycontact.focus();
		}
		
		else if (grade_my.value == "") 
		{
			grade_my.focus();
		}
		else if (section_my.value == "") 
		{
			section_my.focus();
		}
		else 
		{
		var myupdate =  'id=' + myid.value + '&ln=' + lname.value + '&mn=' + mname.value + '&fn=' + fname.value + '&age=' + age.value + '&gender=' + gender.value + '&contact=' + mycontact.value + '&address=' + address.value + '&grade=' + grade_my.value + '&section=' + section_my.value ;
			//alert(myupdate);
			$.ajax({
				type:"POST",
				url:"update_student.php",
				data:myupdate,
				cache:false,
				beforeSend:function(){
					$("#btn_up").text("Updating...");
				},
				success:function(data){
					$("#btn_up").text("Save");
					//alert(data);
					if (data == 1) 
					{
						swal("Success","Information has been updated !","success");
						myinfo();
						$("#edit_info").modal('hide');
					}
					else
					{
						alert(data);
					}
				}
			});

		}

	}