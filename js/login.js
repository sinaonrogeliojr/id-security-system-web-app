function login(){
	var uname,pwd;
	uname = document.getElementById('uname');
	pwd = document.getElementById('pwd');

	if (uname.value == "") 
	{
		uname.focus();
	}
	else if (pwd.value == "")
	{
	pwd.focus();
	}
	else 
	{
	var mydata = 'uname=' + uname.value + '&pwd=' + pwd.value;
	$.ajax({
		type:'POST',
		url:'login.php',
		data:mydata,
		cache:false,
		beforeSend:function(){
		$("#btn_log").text('Loading...');
		},
		success:function(data){
		$("#btn_log").text('Login');
		// alert(data);
		if (data == 1) 
		{
			window.location='client/';
		}
		else if (data == 3) {
			window.location='guard/';
		}
		else if (data == 4) 
		{
			window.location='admin_pane/';
		}
		else if (data == 5) 
		{
			window.location='teacher/';
			//window.location='admin_pane/';
		}
		else if (data == 6) 
		{
			window.location='staff/';
			//window.location='admin_pane/';
		}
		else if (data == 100) 
		{
			//alert("admin");
			//window.location='teacher/';
			window.location='pr_pane/';
		}
		else
		{
		swal("Oops!","Invalid account !","error");
		}
		}
	});
	}
}

function user_type(){
		var a = document.getElementById('usertype');
		if (a.value == 'Student') 
		{	
			$("#uname_l").text('Identification Number');
			$("#pwd_l").text('Lastname');
			$("#uname").attr("placeholder", "Enter student id...");
			$("#pwd").attr("placeholder", "Enter lastname...");
			$("#forms").hide('fast');
			$('#load').html('<br><br><center><img src="img/flat.gif" width="80"></center>');
			setTimeout(function(){$("#forms").show('fast'); $('#load').html('');},500);
		}
		else if (a.value == 'System Provider' || a.value == 'Security') 
		{
			$("#uname_l").text('Username');
			$("#pwd_l").text('Password');
			$("#uname").attr("placeholder", "Enter username...");
			$("#pwd").attr("placeholder", "Enter password...");
			$("#forms").hide('fast');
			$('#load').html('<br><br><center><img src="img/flat.gif" width="80"></center>');
			setTimeout(function(){$("#forms").show('fast'); $('#load').html('');},500);
		}
		else if (a.value == 'Teacher') 
		{
			$("#uname_l").text('Identification Number');
			$("#pwd_l").text('Lastname');
			$("#uname").attr("placeholder", "Enter teacher id...");
			$("#pwd").attr("placeholder", "Enter lastname...");
			$("#forms").hide('fast');
			$('#load').html('<br><br><center><img src="img/flat.gif" width="80"></center>');
			setTimeout(function(){$("#forms").show('fast'); $('#load').html('');},500);
		}
		else if (a.value == 'Staff') 
		{
			$("#uname_l").text('Identification Number');
			$("#pwd_l").text('Lastname');
			$("#uname").attr("placeholder", "Enter staff id...");
			$("#pwd").attr("placeholder", "Enter lastname...");
			$("#forms").hide('fast');
			$('#load').html('<br><br><center><img src="img/flat.gif" width="80"></center>');
			setTimeout(function(){$("#forms").show('fast'); $('#load').html('');},500);
		}
// $("#uname").attr("placeholder", "Type an ID");
}