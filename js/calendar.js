

	function get_up_pic(){
		$.ajax({
			type:"POST",
			url:"new_pic_info.php",
			data:"",
			cache:false,
			beforeSend:function(){
				$("#my_picture").html('<center>Loading...</center>');
			},
			success:function(data){
				$("#my_picture").html(data);
			}
		});
	}

function save_app()
{
	var subject,location,st_date,st_time,end_date,end_time,email,body;
		subject = document.getElementById('subject');
		location = document.getElementById('location');
		st_date = document.getElementById('start_date');
		st_time = document.getElementById('start_time');
		end_date = document.getElementById('end_date');
		end_time = document.getElementById('end_time');
		email = document.getElementById('email');
		body = document.getElementById('body');
		

		if (subject.value == "") 
		{
			subject.focus();
		}
		else if (location.value == "") 
		{
			location.focus();
		}
		else if (st_date.value == "") 
		{
			st_date.focus();
		}
		else if (st_time.value == "") 
		{
		st_time.focus();	
		}
		else if (end_date.value == "") 
		{
			end_date.focus();
		}
		else if (end_time.value == "") 
		{
			end_time.focus();
		}
		else if (email.value == "") 
		{
			email.focus();
		}
		else if (body.value == "") 
		{
			body.focus();
		}
		else 
		{
			var info = 'subject=' + subject.value +'&location=' + location.value +'&st_date=' + st_date.value +'&st_time=' + st_time.value + '&end_date=' + end_date.value + '&end_time=' + end_time.value + '&email=' + email.value + '&body=' + body.value;
			$.ajax({
				type:"POST",
				url:"save_app.php",
				data:info,
				cache:false,
				beforeSend:function(){
				$("#btn_save").text('Saving...');
				},
				success:function(data){
				//alert(data);
				$("#btn_save").text('Save');
				if (data == 2) 
				{
					swal("Oops!","Please enter valid e-mail !","info");
				}
				else if (data == 1) 
				{
					swal("Success!","Appointment has been saved !","success");	
					clear();
				}
				}
			});
		}
}


function clear(){
	$("#subject").val('');
	$("#location").val('');
	$("#start_time").val('');
	$("#start_date").val('');
	$("#end_time").val('');
	$("#end_date").val('');
	$("#email").val('');
	$("#body").val('');
}


function save_cont()
{
	var name,contact,email;
		name = document.getElementById('name');
		contact = document.getElementById('contact');
		email = document.getElementById('email');
		var counter = contact.value;
		

		if (name.value == "") 
		{
			name.focus();
		}
		else if (contact.value == "") 
		{
			contact.focus();
		}
		else if (counter.length <= 10) 
		{
			contact.focus();
			swal("Oops!","Contact number must be 11 digits","info");
		}
		else if (email.value == "") 
		{
			email.focus();
		
		
		
		}
		else 
		{
			var info = 'name=' + name.value + '&contact=' + contact.value + '&email=' + email.value ;
			$.ajax({
				type:"POST",
				url:"save_cont.php",
				data:info,
				cache:false,
				beforeSend:function(){
				$("#btn_save").text('Saving...');
				},
				success:function(data){
				//alert(data);
				$("#btn_save").text('Save');
				if (data == 2) 
				{
					swal("Oops!","Please enter valid e-mail !","info");
				}
				else if (data == 3) 
				{
					swal("Oops!","Name is  too short","info");
				}
				else if (data == 1) 
				{
					swal("Success!","Contact has been saved !","success");	
					clear();
				}
				}
			});
		}
}


function num_only(evt){
		var charcode= (evt.which) ? evt.which : event.keycode
		if (charcode > 31 && (charcode < 48 ||  charcode >57))
			return false;
		return true;
	}

function clear(){
	$("#name").val('');
	$("#contact").val('');
	$("#email").val('');
}


