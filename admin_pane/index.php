<?php 
session_start();
include_once("../config.php");
if (!isset($_SESSION['admin_pane'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title>Ssja</title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<body>
<?php 
include_once("nav.php");
 ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<div class="text-center">
				<h3><i class="fa fa-shield w3-xxxlarge"></i></h3>
				<h3>System Provider</h3>
			</div>
			<hr>
			<a href="setup.php" class="btn btn-block btn-lg w3-blue w3-hover-shadow" style="text-align:left;"><i class="fa fa-gear"></i> SCHOOL SETUP</a>

			<a href="register_sec.php" class="btn btn-block btn-lg w3-blue w3-hover-shadow" style="text-align:left;"><i class="fa fa-edit"></i> REGISTER SECURITY</a>

			<a href="#logout" data-toggle="modal"  data-target="#logout_me" class="btn btn-block btn-lg w3-red w3-hover-shadow" style="text-align:left;"><i class="fa fa-arrow-left"></i> Logout</a>
						
		<div class="col-sm-3"></div>
	</div>
</div>

</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>
<script src="../js/login.js"></script>
</html>

<?php include_once("modals.php"); ?>