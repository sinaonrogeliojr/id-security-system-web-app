<?php 
session_start();
include_once("../config.php");
if (!isset($_SESSION['admin_pane'])) {
	@header('location:../');
}
 ?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<title>Ssja</title>
	 <meta charset="utf-8">
	 <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/w3.css">
	<link rel="stylesheet" type="text/css" href="../css/animate.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../font/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
</head>
<script type="text/javascript">
	function mform_load(){
		$.ajax({
			type:"POST",
			url:"get_form.php",
			data:"",
			cache:false,
			beforeSend:function(){
				$("#my_form").html('<center><h3 class="animated pulse infinite">Intializing data....</h3></center>');
			},
			success:function(data){
				$("#my_form").html(data);
			}
		});
	}
</script>

<script type="text/javascript">
	function set_setup(){
		var time_in,early;
		time_in = document.getElementById('time_in');
		early = document.getElementById('early');
		mid = document.getElementById('mid');
		early_bird = document.getElementById('early_bird');

		if (time_in.value == "") {
			time_in.focus();
		}
		else if (early.value == "") {
			early.focus();
		}
		else
		{
			var mydata = 'time_in=' + time_in.value + '&early=' + early.value + '&mid=' + mid.value + '&early_bird=' + early_bird.value;
			// alert(mydata);
			$.ajax({
				type:"POST",
				url:"set_setup.php",
				data:mydata,
				cache:false,
				beforeSend:function(){
					$("#my_form").html('<center><h3 class="animated pulse infinite">Intializing data....</h3></center>');
				},
				success:function(data){
					if (data == 1 || data == 2) 
					{
						swal("Success","succesfuly save setup!","success");
						mform_load();
					}
					else
					{
						alert(data);
					}
				}
			});
		}
	}
</script>
<body onload="mform_load();">
<?php 
include_once("nav.php");
 ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<div class="text-center">
				<h3><i class="fa fa-gear w3-xlarge"></i> School Setup</h3>
			</div>
			<hr>
			<div id="my_form"></div>
						
		<div class="col-sm-3"></div>
	</div>
</div>

<script type="text/javascript">
		function num_only(evt){
		var charcode= (evt.which) ? evt.which : event.keycode
		if (charcode > 31 && (charcode < 48 ||  charcode >57))
			return false;
		return true;
	}
</script>

</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/sweetalert.min.js"></script>
<script src="../js/login.js"></script>
</html>

<?php include_once("modals.php"); ?>