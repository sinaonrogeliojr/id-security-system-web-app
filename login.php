<?php 
session_start();
include_once('config.php');

$uname = mysqli_real_escape_string($con,strtoupper($_POST['uname']));
$pwd = mysqli_real_escape_string($con,strtoupper($_POST['pwd']));

$admin_uname = strtolower($uname);
$admin_pwd = strtolower($pwd);

$stmt = mysqli_query($con,"SELECT a.*,b.* from tbl_students a left join tbl_student_picture b on a.student_id = b.student_id where a.student_id='$uname' and a.lastname='$pwd' ");


$stmt_guard = mysqli_query($con,"SELECT * from tbl_user_account where user_id='$admin_uname' and user_pwd='$admin_pwd'");

$admin = mysqli_query($con,"SELECT * from tbl_user_account where user_id='$admin_uname' and user_pwd='$admin_pwd'");


$principal = mysqli_query($con,"SELECT a.*,b.*,c.* from tbl_user_account a left join tbl_staff b on a.access_form=b.position left join tbl_staff_picture c on b.staff_id = c.staff_id  where a.user_id='$admin_uname' and a.user_pwd='$admin_pwd'");



$teacher = mysqli_query($con,"SELECT a.*,b.* from tbl_teachers a left join tbl_teacher_picture b on a.teacher_id = b.teacher_id where a.teacher_id='$uname' and a.lastname='$pwd' or a.teacher_id='$admin_uname' and a.lastname='$pwd'");


$staff = mysqli_query($con,"SELECT a.*,b.* from tbl_staff a left join tbl_staff_picture b on a.staff_id = b.staff_id where a.staff_id='$uname' and a.lastname='$pwd' or a.staff_id='$admin_uname' and a.lastname='$pwd'");



if (mysqli_num_rows($stmt)>0) {
	
	$injec = mysqli_fetch_assoc($stmt);
	
	if ($injec['student_id'] === $uname && $injec['lastname'] === $pwd) {
	echo 1;
	$_SESSION['id'] = $injec['student_id'];
	$_SESSION['transid'] = $injec['trans_id'];
	$_SESSION['mypic'] = $injec['image'];
	$_SESSION['lname'] = $injec['lastname'];
	$_SESSION['fname'] = $injec['firstname'];
	$_SESSION['mname'] = $injec['mi'];
	$_SESSION['fullname'] = $injec['lastname'].', '.$injec['firstname'].' '.$injec['mi'];
	$_SESSION['gender'] = $injec['gender'];
	$_SESSION['age'] = $injec['age'];
	$_SESSION['grade'] = $injec['grade'];
	$_SESSION['section'] = $injec['section'];
	$_SESSION['address'] = $injec['address'];
	$_SESSION['contact'] = $injec['contact'];
	$_SESSION['parent_id'] = $injec['parent_id'];
	$_SESSION['guardian_id'] = $injec['guardian_id'];
	$_SESSION['guardian_id2'] = $injec['guardian_id_2'];
	$_SESSION['guardian_id3'] = $injec['guardian_id_3'];

	}
	else 
	{
		echo 0;
	}
}
else if (mysqli_num_rows($stmt_guard)>0) {
	$injec_g = mysqli_fetch_assoc($stmt_guard);
	
	if ($injec_g['user_id'] === $admin_uname && $injec_g['user_pwd'] === $admin_pwd && $injec_g['access_form'] === 'SECURITY') {
		$_SESSION['id_guard'] = $injec_g['transid'];
		$_SESSION['id_sec'] = $injec_g['user_id'];
		$_SESSION['sec_name'] = $injec_g['access_form'];
		$_SESSION['sec_uname'] = $injec_g['user_id'];
		$_SESSION['sec_pwd'] = $injec_g['user_pwd'];
		echo 3;
	}
	else if (mysqli_num_rows($admin)>0) {
	$admin_r = mysqli_fetch_assoc($admin);

	if ($admin_r['user_id'] === $admin_uname && $admin_r['user_pwd'] === $admin_pwd && $admin_r['access_form'] === 'ALL') {
		$_SESSION['admin_pane'] = $admin_r['transid'];
		echo 4;
	}
	else if (mysqli_num_rows($principal)>0) {
		$a_admin_r = mysqli_fetch_assoc($principal);
		if ($a_admin_r['user_id'] === $admin_uname && $a_admin_r['user_pwd'] === $admin_pwd && $a_admin_r['access_form'] === 'PRINCIPAL') {
		$_SESSION['pr_id'] = $a_admin_r['staff_id'];
		$_SESSION['pr_fullname'] = $a_admin_r['lastname'].', '.$a_admin_r['firstname'].' '.$a_admin_r['mi'];
		$_SESSION['pr_picture'] = $a_admin_r['image'];
		$_SESSION['pr_fn'] = $a_admin_r['firstname'];
		$_SESSION['pr_mn'] = $a_admin_r['mi'];
		$_SESSION['pr_ln'] = $a_admin_r['lastname'];
		$_SESSION['pr_post'] = $a_admin_r['position'];
		echo 100;
	}
	}
	
}	
}
else if (mysqli_num_rows($teacher)>0) {
	$row_teach = mysqli_fetch_assoc($teacher);

	if ($row_teach['teacher_id'] === $uname && $row_teach['lastname'] === $pwd || $row_teach['teacher_id'] === $admin_uname && $row_teach['lastname'] === $pwd) {
		$_SESSION['teacher_transid'] = $row_teach['trans_id'];
		$_SESSION['teacher_id'] = $row_teach['teacher_id'];
		$_SESSION['teacher_fullname'] = $row_teach['lastname'].', '.$row_teach['firstname'].' '.$row_teach['mi'];
		$_SESSION['teacher_fn'] = $row_teach['firstname'];
		$_SESSION['teacher_mn'] = $row_teach['mi'];
		$_SESSION['teacher_ln'] = $row_teach['lastname'];
		$_SESSION['teacher_addr'] = $row_teach['address'];
		$_SESSION['teacher_contact'] = $row_teach['contact'];
		$_SESSION['teacher_email'] = $row_teach['email'];
		$_SESSION['teacher_age'] = $row_teach['age'];
		$_SESSION['teacher_bday'] = $row_teach['bday'];
		$_SESSION['teacher_gender'] = $row_teach['gender'];
		$_SESSION['teacher_advisory'] = $row_teach['advisory'];
		$_SESSION['teacher_grade'] = $row_teach['grade'];
		$_SESSION['teacher_pict'] = $row_teach['image'];
		echo 5;
	}
}
else if (mysqli_num_rows($staff)>0) {
	$rowf = mysqli_fetch_assoc($staff);
	if ($rowf['staff_id'] === $uname && $rowf['lastname'] === $pwd || $rowf['staff_id'] === $admin_uname && $rowf['lastname'] === $pwd) {
		$_SESSION['st_id'] = $rowf['staff_id'];
		$_SESSION['st_fullname'] = $rowf['lastname'].', '.$rowf['firstname'].' '.$rowf['mi'];
		$_SESSION['st_picture'] = $rowf['image'];
		$_SESSION['st_fn'] = $rowf['firstname'];
		$_SESSION['st_mn'] = $rowf['mi'];
		$_SESSION['st_ln'] = $rowf['lastname'];
		$_SESSION['st_addr'] = $rowf['address'];
		$_SESSION['st_contact'] = $rowf['contact'];
		$_SESSION['st_email'] = $rowf['email'];
		$_SESSION['st_age'] = $rowf['age'];
		$_SESSION['st_bday'] = $rowf['bday'];
		$_SESSION['st_gender'] = $rowf['gender'];
		$_SESSION['st_post'] = $rowf['position'];
		
		echo 6;
	}

}
else
{
	echo 2;
}

 ?>